Renshu:
=======

Renshu Trainingsverwaltung Version 2.x

Version History: 
================

15.03.2018: Version 2.5

            -added correction possibilities to exams, trainings and competitions
            -added full support for remote mysql
            -added support for configuration files
            -added support for multiple trainers/ configurations

02.02.2018: Version 2.4
    
            -layout of exams and trainings changed
            -added training by existing slot
            -pdf print layout changed

13.01.2018: Version 2.3
            
            -added compatibility to mysql
            -added printing to pdf via latex

23.07.2017: Version 2.2

            -changed judoka year of birth to date of birth
            -added judoka passport numbers
            -added right-click context menu


20.02.2017: Version 2.1

            -Windows compatible
            -Training pupillist added
            -Chooser for year of competitor overview added
            -various bugfixes
            
13.02.2017: Version 2.00

Prerequesites:
==============

- c compiler (for example gcc)
- libgtk+-3.0
- libsqlite3
- mysql (optional)
- latex and latexmk (optional)

Installation:
=============

For installation run the following commands: 

cd source

make

sudo make install

This creates an executable and configures necessary MIME types.

For configuration and further assistance see documentation.

Under Windows simply run the setup-executable located in the windows-subfolder

LICENSE:
========

Copyright (C) 2017 Wolfgang Knopki

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
