begin transaction;
create table 'judoka_temp' ('pupils_id' integer primary key, 'name' text, 'prename' text , 'date_of_birth' date, 'graduation' integer default 0, 'license' text, 'display' text, 'contact' text,'passport' integer default 0, 'notes' text);
insert into judoka_temp (pupils_id, name,prename,date_of_birth,graduation,license,display,contact,notes) select pupils_id,name,prename,year_of_birth||'-00-00',graduation,license,display,contact,notes from judoka;
drop table judoka;
alter table judoka_temp rename to judoka;
commit;