#define _GNU_SOURCE
#include <my_global.h>
#include <gtk/gtk.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "common.h"
#include "dialog_callbacks.h"
#include "read_db_callbacks.h"

#ifdef WIN32
#if 0
#include <windef.h>
#include <ansidecl.h>
#include <winbase.h>
#include <winnt.h>
#endif
#include "sqlite3.h"
#else
#include <sqlite3.h>
#include <my_global.h>
#include <mysql.h>
#endif

#define bufSize 1024

//#ifndef WIN32        
void finish_with_error(MYSQL *con)
{
    printf("error!");
    fprintf(stderr, "%s\n", mysql_error(con));
    gchar* error;
    asprintf(&error, "Cannot open database: %s\n", mysql_error(con));
    error_dialog(error);
    exit(1);        
}
//#endif

GtkWidget* find_child(GtkWidget* parent, const gchar* name)
{
    if (strcmp(gtk_widget_get_name((GtkWidget*)parent), (gchar*)name) == 0) { 
        return parent;
    }
    
    if (GTK_IS_BIN(parent)) {
        GtkWidget *child = gtk_bin_get_child(GTK_BIN(parent));
        return find_child(child, name);
    }
    
    if (GTK_IS_CONTAINER(parent)) {
        GList *children = gtk_container_get_children(GTK_CONTAINER(parent));
        while ((children = g_list_next(children)) != NULL) {
            GtkWidget* widget = find_child(GTK_WIDGET(children->data), name);
            if (widget != NULL) {
                return widget;
            }
        }
    }
    
    return NULL;
}

int read_callback_judoka(void* param, int argc, char**argv, char**column){
    GtkTreeStore* result = (GtkTreeStore*) param;
    GtkTreeIter iter;
    gtk_tree_store_append(result, &iter, NULL);
    if(argv != NULL){
        gtk_tree_store_set(result, &iter, ID_COLUMN, atoi(argv[0]), NAME_COLUMN, argv[1], PRENAME_COLUMN, argv[2], YOB_COLUMN, argv[3], GRADUATION_COLUMN, atoi(argv[4]), LICENSE_COLUMN, argv[5], CONTACT_COLUMN, argv[6], PASSPORT_COLUMN, argv[7], NOTES_COLUMN, argv[8], -1);
    }
    return 0;
}

int read_callback_trainings_combo(void* param, int argc, char**argv, char**column){
    GtkComboBox* result = (GtkComboBox*) param;
    if(argv != NULL){
        char* entry_complete;
        asprintf(&entry_complete, "%s %s", argv[0], argv[1]);
        gtk_combo_box_text_append(GTK_COMBO_BOX_TEXT(result), entry_complete, entry_complete);
    }
    return 0;
}


int read_callback_trainings_pro(void* param, int argc, char**argv, char**column){
    GtkTreeStore* result = (GtkTreeStore*) param;
    GtkTreeIter iter;
     GtkTreeIter iter_child;
    GtkTreeIter iter_parent;
    int found = 0;
    gchar* string_day;
    gint int_nr;
    gboolean valid;
    
    //check for existing entries in treestore by date
    valid = gtk_tree_model_get_iter_first(GTK_TREE_MODEL(result), &iter);
    while(found == 0 && valid){
        gtk_tree_model_get(GTK_TREE_MODEL(result),&iter,DAY_COLUMN_PRO,&string_day,NR_COLUMN_PRO, &int_nr, -1);
        if(!strcmp(string_day,argv[1]) && int_nr == atoi(argv[2])){
            found = 1;
            break; //important: leave while loop immediately or else iterator is lost
        }
        valid = gtk_tree_model_iter_next(GTK_TREE_MODEL(result),&iter);
    }
    //the found date is now returned
    if(found == 1){
        gtk_tree_store_append(result,&iter_child,&iter);
        gtk_tree_store_set(result, &iter_child, DAY_COLUMN_PRO, "", NR_COLUMN_PRO, atoi(argv[2]), DATE_COLUMN_PRO, argv[3], TRAINER0_COLUMN_PRO, argv[5],TRAINER1_COLUMN_PRO, argv[6], -1);
    }else{     
        //if not found, create new "parent" entry
        gtk_tree_store_append(result,&iter_parent,NULL);
        gtk_tree_store_set(result, &iter_parent, DAY_COLUMN_PRO, argv[1], NR_COLUMN_PRO, atoi(argv[2]), DATE_COLUMN_PRO, "", TRAINER0_COLUMN_PRO, "",TRAINER1_COLUMN_PRO, "", -1);
        gtk_tree_store_append(result,&iter_child,&iter_parent);
        gtk_tree_store_set(result, &iter_child, DAY_COLUMN_PRO, "", NR_COLUMN_PRO, atoi(argv[2]), DATE_COLUMN_PRO, argv[3],TRAINER0_COLUMN_PRO, argv[5],TRAINER1_COLUMN_PRO, argv[6], -1);
    }
    return 0;
}

int read_callback_trainings_pro_list(void* param, int argc, char**argv, char**column){
    GtkTreeStore* result = (GtkTreeStore*) param;
    GtkTreeIter iter;
    
    gtk_tree_store_append(result,&iter,NULL);
    gtk_tree_store_set(result, &iter, DAY_COLUMN_PRO, argv[1], NR_COLUMN_PRO, atoi(argv[2]), DATE_COLUMN_PRO, argv[3], -1);
    
    return 0;
}


int read_callback_trainings(void* param, int argc, char**argv, char**column){
    GtkTreeStore* result = (GtkTreeStore*) param;
    GtkTreeIter iter;
     GtkTreeIter iter_child;
    GtkTreeIter iter_parent;
    int found = 0;
    gchar* string_day;
    gint int_nr;
    gboolean valid;
    gint counter;
    
    //check for existing entries in treestore by date
    valid = gtk_tree_model_get_iter_first(GTK_TREE_MODEL(result), &iter);
    while(found == 0 && valid){
        gtk_tree_model_get(GTK_TREE_MODEL(result),&iter,DAY_COLUMN,&string_day,NR_COLUMN, &int_nr, -1);
        if(!strcmp(string_day,argv[1]) && int_nr == atoi(argv[2])){
            found = 1;
            break; //important: leave while loop immediately or else iterator is lost
        }
        valid = gtk_tree_model_iter_next(GTK_TREE_MODEL(result),&iter);
    }
    //the found date is now returned
    if(found == 1){
        gtk_tree_store_append(result,&iter_child,&iter);
        gtk_tree_store_set(result, &iter_child, DAY_COLUMN, "", NR_COLUMN, atoi(argv[2]), DATE_COLUMN, argv[3], COUNT_COLUMN, atoi(argv[4]), TRAINER0_COLUMN, argv[5],TRAINER1_COLUMN, argv[6], -1);
        //increment counter
        gtk_tree_model_get(GTK_TREE_MODEL(result),&iter,COUNT_COLUMN,&counter, -1);
        counter++;
        gtk_tree_store_set(result, &iter, COUNT_COLUMN,counter,-1); 
    }else{     
        //if not found, create new "parent" entry
        gtk_tree_store_append(result,&iter_parent,NULL);
        gtk_tree_store_set(result, &iter_parent, DAY_COLUMN, argv[1], NR_COLUMN, atoi(argv[2]), DATE_COLUMN, "", COUNT_COLUMN, 1, TRAINER0_COLUMN, "",TRAINER1_COLUMN, "", -1);
        gtk_tree_store_append(result,&iter_child,&iter_parent);
        gtk_tree_store_set(result, &iter_child, DAY_COLUMN, "", NR_COLUMN, atoi(argv[2]), DATE_COLUMN, argv[3], COUNT_COLUMN, atoi(argv[4]), TRAINER0_COLUMN, argv[5],TRAINER1_COLUMN, argv[6], -1);
    }  
    return 0;
}

int read_callback_trainings_list(void* param, int argc, char**argv, char**column){
    GtkTreeStore* result = (GtkTreeStore*) param;
    GtkTreeIter iter;
    
    gtk_tree_store_append(result,&iter,NULL);
    gtk_tree_store_set(result, &iter, DAY_COLUMN, argv[1], NR_COLUMN, atoi(argv[2]), DATE_COLUMN, argv[3], COUNT_COLUMN, atoi(argv[4]), TRAINER0_COLUMN, argv[5],TRAINER1_COLUMN, argv[6], -1);
      
    return 0;
}

int read_callback_exams(void* param, int argc, char**argv, char**column){
    GtkTreeStore* result = (GtkTreeStore*) param;
    GtkTreeIter iter;
    GtkTreeIter iter_child;
    GtkTreeIter iter_parent;
    int found = 0;
    gchar* string;
    gchar* string_examiner0;
    gchar* string_examiner1;
    gchar* string_examiner2;
    gboolean valid;
    gint counter;
    
    char* examiner0_complete;
    char* examiner1_complete;
    char* examiner2_complete;
    char* examined_complete;
    asprintf(&examiner0_complete, "%s %s", argv[2], argv[3]);
    asprintf(&examiner1_complete, "%s %s", argv[4], argv[5]);
    asprintf(&examiner2_complete, "%s %s", argv[6], argv[7]);
    asprintf(&examined_complete, "%s %s", argv[8], argv[9]);
    
    //check for existing entries in treestore by date
    valid = gtk_tree_model_get_iter_first(GTK_TREE_MODEL(result), &iter);
    while(found == 0 && valid){
        gtk_tree_model_get(GTK_TREE_MODEL(result),&iter,DATE_COLUMN_EXAM,&string,EXAMINER0_COLUMN, &string_examiner0,EXAMINER1_COLUMN, &string_examiner1,EXAMINER2_COLUMN, &string_examiner2, -1);
        if(!strcmp(string,argv[1]) && !strcmp(examiner0_complete,string_examiner0)&& !strcmp(examiner1_complete,string_examiner1)&& !strcmp(examiner2_complete,string_examiner2)){
            found = 1;
            break; //important: leave while loop immediately or else iterator is lost
        }
        valid = gtk_tree_model_iter_next(GTK_TREE_MODEL(result),&iter);
    }
    //the found date is now returned
    if(found == 1){
        gtk_tree_store_append(result,&iter_child,&iter);
        gtk_tree_store_set(result, &iter_child, EXAMINED_ID_COLUMN, argv[11], DATE_COLUMN_EXAM, "", EXAMINER0_COLUMN, "",EXAMINER1_COLUMN, "",EXAMINER2_COLUMN, "", EXAMINED_COLUMN, examined_complete, RECEIVED_COLUMN, atoi(argv[10]), -1);
        //increment counter
        gtk_tree_model_get(GTK_TREE_MODEL(result),&iter,RECEIVED_COLUMN,&counter, -1);
        counter++;
        gtk_tree_store_set(result, &iter, RECEIVED_COLUMN,counter,-1); 
    }else{     
        //if not found, create new "parent" entry
        gtk_tree_store_append(result,&iter_parent,NULL);
        gtk_tree_store_set(result, &iter_parent, EXAMINED_ID_COLUMN, "", DATE_COLUMN_EXAM, argv[1], EXAMINER0_COLUMN, examiner0_complete,EXAMINER1_COLUMN, examiner1_complete,EXAMINER2_COLUMN, examiner2_complete, EXAMINED_COLUMN, "", RECEIVED_COLUMN, 1, -1);
        gtk_tree_store_append(result,&iter_child,&iter_parent);
        gtk_tree_store_set(result, &iter_child, EXAMINED_ID_COLUMN, argv[11], DATE_COLUMN_EXAM, "", EXAMINER0_COLUMN, "",EXAMINER1_COLUMN, "",EXAMINER2_COLUMN, "", EXAMINED_COLUMN, examined_complete, RECEIVED_COLUMN, atoi(argv[10]), -1);
    }    
    free(examiner0_complete);
    free(examiner1_complete);
    free(examiner2_complete);
    free(examined_complete);
    return 0;
}

int read_callback_comp(void* param, int argc, char**argv, char**column){
    GtkTreeStore* result = (GtkTreeStore*) param;
    GtkTreeIter iter;
    GtkTreeIter iter_child;
    GtkTreeIter iter_parent;
    int found = 0;
    gchar* string;
    gboolean valid;
    gint counter;
    
    char* competitor_complete;
    asprintf(&competitor_complete, "%s %s", argv[4], argv[5]);
    
    //check for existing entries in treestore by date
    valid = gtk_tree_model_get_iter_first(GTK_TREE_MODEL(result), &iter);
    while(found == 0 && valid){
        gtk_tree_model_get(GTK_TREE_MODEL(result),&iter,DATE_COLUMN_COMP,&string, -1);
        if(!strcmp(string,argv[2])){
            found = 1;
            break; //important: leave while loop immediately or else iterator is lost
        }
        valid = gtk_tree_model_iter_next(GTK_TREE_MODEL(result),&iter);
    }
    //the found date is now returned
    if(found == 1){
        gtk_tree_store_append(result,&iter_child,&iter);
        gtk_tree_store_set(result, &iter_child, COMPETITOR_ID_COLUMN, argv[8], DATE_COLUMN_COMP, "", COMPETITOR_COLUMN, competitor_complete, LOCATION_COLUMN, "", CLASS_COLUMN, argv[6],RESULT_COLUMN, atoi(argv[7]), EVENT_COLUMN, "", -1);
        //increment counter
        gtk_tree_model_get(GTK_TREE_MODEL(result),&iter,RESULT_COLUMN,&counter, -1);
        counter++;
        gtk_tree_store_set(result, &iter, RESULT_COLUMN,counter,-1); 
    }else{     
        //if not found, create new "parent" entry
        gtk_tree_store_append(result,&iter_parent,NULL);
        gtk_tree_store_set(result, &iter_parent, COMPETITOR_ID_COLUMN, "", DATE_COLUMN_COMP, argv[2], COMPETITOR_COLUMN, "", LOCATION_COLUMN, argv[3], CLASS_COLUMN, "",RESULT_COLUMN, 1, EVENT_COLUMN, argv[1], -1);
        gtk_tree_store_append(result,&iter_child,&iter_parent);
        gtk_tree_store_set(result, &iter_child, COMPETITOR_ID_COLUMN, argv[8], DATE_COLUMN_COMP, "", COMPETITOR_COLUMN, competitor_complete, LOCATION_COLUMN, "", CLASS_COLUMN, argv[6],RESULT_COLUMN, atoi(argv[7]), EVENT_COLUMN, "", -1);
    }
    
    free(competitor_complete);
    return 0;
}


int read_callback_comp_list(void* param, int argc, char**argv, char**column){
    GtkTreeStore* result = (GtkTreeStore*) param;
    GtkTreeIter iter;
    
    char* competitor_complete;
    asprintf(&competitor_complete, "%s %s", argv[4], argv[5]);
    
    gtk_tree_store_append(result,&iter,NULL);
    gtk_tree_store_set(result, &iter, COMPETITOR_ID_COLUMN, argv[8], DATE_COLUMN_COMP, argv[2], COMPETITOR_COLUMN, competitor_complete, LOCATION_COLUMN, argv[3], CLASS_COLUMN, argv[6],RESULT_COLUMN, atoi(argv[7]), EVENT_COLUMN, argv[1], -1);

    free(competitor_complete);
    return 0;
}

int read_callback_exam_list(void* param, int argc, char**argv, char**column){
    GtkTreeStore* result = (GtkTreeStore*) param;
    GtkTreeIter iter;
    
    char* examiner0_complete;
    char* examiner1_complete;
    char* examiner2_complete;
    char* examined_complete;
    asprintf(&examiner0_complete, "%s %s", argv[2], argv[3]);
    asprintf(&examiner1_complete, "%s %s", argv[4], argv[5]);
    asprintf(&examiner2_complete, "%s %s", argv[6], argv[7]);
    asprintf(&examined_complete, "%s %s", argv[8], argv[9]);
    
    gtk_tree_store_append(result,&iter,NULL);
    gtk_tree_store_set(result, &iter, EXAMINED_ID_COLUMN, argv[11], DATE_COLUMN_EXAM, argv[1], EXAMINER0_COLUMN, examiner0_complete,EXAMINER1_COLUMN, examiner1_complete,EXAMINER2_COLUMN, examiner2_complete, EXAMINED_COLUMN, examined_complete, RECEIVED_COLUMN, atoi(argv[10]), -1);
    
    
    free(examiner0_complete);
    free(examiner1_complete);
    free(examiner2_complete);
    free(examined_complete);
    return 0;
}

void read_db(GtkTreeStore* resultbuffer,const renshu_cb_provider* cbprovider,const char* query,const int expected_cols){
    const char* dbname = cbprovider->dbname;
    const int usemysql = cbprovider->usemysql;
    gchar* error;
    char* err_msg = 0;
    
    if(!usemysql){
        sqlite3* db;
        int rc = sqlite3_open(dbname, &db);
        if(rc != SQLITE_OK){
            fprintf(stderr, "Cannot open database: %s\n", sqlite3_errmsg(db));
            asprintf(&error, "Cannot open database: %s\n", sqlite3_errmsg(db));
            error_dialog(error);
            goto end; //ugly af, but not to avoid
        }
        //execute query
        if(expected_cols == 0){
            rc = sqlite3_exec(db, query, read_callback_judoka, resultbuffer, &err_msg);
        }else if(expected_cols == 1){
            rc = sqlite3_exec(db, query, read_callback_trainings_pro, resultbuffer, &err_msg);
        }else if(expected_cols == 2){
            rc = sqlite3_exec(db, query, read_callback_trainings, resultbuffer, &err_msg);
        }else if(expected_cols == 3){
            rc = sqlite3_exec(db, query, read_callback_exams, resultbuffer, &err_msg);
        }else if(expected_cols == 4){
            rc = sqlite3_exec(db, query, read_callback_comp, resultbuffer, &err_msg);
        }else if(expected_cols == 5){
            rc = sqlite3_exec(db, query, read_callback_trainings_combo, resultbuffer, &err_msg);
        }else if(expected_cols == 6){
            rc = sqlite3_exec(db, query, read_callback_trainings_pro_list, resultbuffer, &err_msg);
        }else if(expected_cols == 7){
            rc = sqlite3_exec(db, query, read_callback_trainings_list, resultbuffer, &err_msg);
        }else if(expected_cols == 8){
            rc = sqlite3_exec(db, query, read_callback_comp_list, resultbuffer, &err_msg);
        }else if(expected_cols == 9){
            rc = sqlite3_exec(db, query, read_callback_exam_list, resultbuffer, &err_msg);
        }
        if(rc!=SQLITE_OK){
            fprintf(stderr, "Failed to select data\n");
            fprintf(stderr, "SQL error: %s\n", err_msg);
            asprintf(&error, "Failed to select data\n SQL error: %s\n", err_msg);
            error_dialog(error);
            sqlite3_free(err_msg);
            sqlite3_close(db);
        }
        end:
        sqlite3_close(db);
    }else if(usemysql){
//#ifndef WIN32        
        MYSQL* con = mysql_init(NULL);
        char** array_results = NULL;
        if (con == NULL){
            fprintf(stderr, "mysql_init() failed\n");
            error_dialog("mysql_init() dailed\n");
            exit(1);
        }  
  
        if (mysql_real_connect(con, cbprovider->dbaddr, cbprovider->dbuser, cbprovider->dbpasswd, 
          cbprovider->dbname, 0, NULL, 0) == NULL) {
            finish_with_error(con);
        }
        if (mysql_query(con, "set names 'utf8';")){  
            finish_with_error(con);
        }
        if (mysql_query(con, "set character set 'utf8';")){  
            finish_with_error(con);
        }
        if (mysql_query(con, "set sql_mode='';")){  
            finish_with_error(con);
        }
        if (mysql_query(con, query)){  
            finish_with_error(con);
        }
        MYSQL_RES *result = mysql_store_result(con);
        if (result == NULL) {
            finish_with_error(con);
        }  
        int num_fields = mysql_num_fields(result);
        MYSQL_ROW row;
        while ((row = mysql_fetch_row(result))) { 
            for(int i = 0; i < num_fields; i++) {
                array_results = realloc(array_results, (i+1) * sizeof(char*));
                asprintf(&array_results[i], "%s", row[i] ? row[i] : "NULL");
          //printf("%s  ", row[i] ? row[i] : "NULL"); 
            } 
            if(expected_cols == 0){
                read_callback_judoka(resultbuffer,num_fields, array_results,NULL);
            }else if(expected_cols == 1){
                read_callback_trainings_pro(resultbuffer,num_fields, array_results,NULL);
            }else if(expected_cols == 2){
                read_callback_trainings(resultbuffer,num_fields, array_results,NULL);
            }else if(expected_cols == 3){
                read_callback_exams(resultbuffer,num_fields, array_results,NULL);
            }else if(expected_cols == 4){
                read_callback_comp(resultbuffer,num_fields, array_results,NULL);
            }else if(expected_cols == 5){
                read_callback_trainings_combo(resultbuffer,num_fields, array_results,NULL);
            }else if(expected_cols == 6){
                read_callback_trainings_pro_list(resultbuffer,num_fields, array_results,NULL);
            }else if(expected_cols == 7){
                read_callback_trainings_list(resultbuffer,num_fields, array_results,NULL);
            }else if(expected_cols == 8){
                read_callback_comp_list(resultbuffer,num_fields, array_results,NULL);
            }else if(expected_cols == 9){
                read_callback_exam_list(resultbuffer,num_fields, array_results,NULL);
            };
        }
        mysql_free_result(result);
        mysql_close(con);
        free(array_results);
//#endif
    };
}    
/*
 * Function: show_trainings_all
 * ----------------------------
 *   callback to display all trainings
 *
 *   widget: the button that has called the callback, 
 *    
 *   data: renshu_cb_provider, provides database and output window
 *
 */
void show_trainings_all(GtkWidget *widget, gpointer data){
    renshu_cb_provider* incoming = (renshu_cb_provider*)data;
    
    GtkTreeStore* store = gtk_tree_store_new(N_TRAININGS_COLUMNS, G_TYPE_STRING, G_TYPE_INT, G_TYPE_STRING, G_TYPE_INT, G_TYPE_STRING, G_TYPE_STRING, G_TYPE_STRING);
    GtkWidget* labeltowrite = GTK_WIDGET(incoming->output);
    /*remove any already set columns/ stores*/
    gtk_tree_view_set_model(GTK_TREE_VIEW(labeltowrite), NULL);
    GList* columns = gtk_tree_view_get_columns(GTK_TREE_VIEW(labeltowrite));
    for(columns = g_list_first(columns); columns != NULL; columns = columns->next){
        gtk_tree_view_remove_column(GTK_TREE_VIEW(labeltowrite), GTK_TREE_VIEW_COLUMN(columns->data));   
    }
    //list no longer needed
    g_list_free(columns);
    GtkCellRenderer *renderer = gtk_cell_renderer_text_new();
    GtkTreeViewColumn* column; 
    column = gtk_tree_view_column_new_with_attributes("Tag", renderer, "text", DAY_COLUMN, NULL);
    gtk_tree_view_column_set_sort_column_id(column,DAY_COLUMN);
    gtk_tree_view_append_column(GTK_TREE_VIEW(labeltowrite), column);
    column = gtk_tree_view_column_new_with_attributes("Nummer", renderer, "text", NR_COLUMN, NULL);
    gtk_tree_view_column_set_sort_column_id(column,NR_COLUMN);
    gtk_tree_view_append_column(GTK_TREE_VIEW(labeltowrite), column);
    column = gtk_tree_view_column_new_with_attributes("Datum", renderer, "text", DATE_COLUMN, NULL);
    gtk_tree_view_column_set_sort_column_id(column,DATE_COLUMN);
    gtk_tree_view_append_column(GTK_TREE_VIEW(labeltowrite), column);
    column = gtk_tree_view_column_new_with_attributes("Anzahl", renderer, "text", COUNT_COLUMN, NULL);
    gtk_tree_view_column_set_sort_column_id(column,COUNT_COLUMN);
    gtk_tree_view_append_column(GTK_TREE_VIEW(labeltowrite), column);
    column = gtk_tree_view_column_new_with_attributes("Trainer 1", renderer, "text", TRAINER0_COLUMN, NULL);
    gtk_tree_view_column_set_sort_column_id(column,TRAINER0_COLUMN);
    gtk_tree_view_append_column(GTK_TREE_VIEW(labeltowrite), column);
    column = gtk_tree_view_column_new_with_attributes("Trainer 2", renderer, "text", TRAINER1_COLUMN, NULL);
    gtk_tree_view_column_set_sort_column_id(column,TRAINER1_COLUMN);
    gtk_tree_view_append_column(GTK_TREE_VIEW(labeltowrite), column);
    char* getstring;
    if(incoming->usemysql){
        asprintf(&getstring, "select training.training_id, training.weekday, training.training_nr, training.date, count(*) as anwesend, concat(judoka.prename, ' ', judoka.name) as trainer0, concat(judoka1.prename, ' ', judoka1.name) as trainer1 from training inner join judoka on training.trainer0 = judoka.pupils_id  inner join judoka as judoka1 on training.trainer1 = judoka1.pupils_id where training.date>'2013-05-01' group by training.date, training.training_nr order by training.date desc;");
    }else{
        asprintf(&getstring, "select training.training_id, training.weekday, training.training_nr, training.date, count(*) as anwesend, (judoka.prename|| ' '|| judoka.name) as trainer0, (judoka1.prename || ' ' || judoka1.name) as trainer1  from training inner join judoka on training.trainer0 = judoka.pupils_id inner join judoka as judoka1 on training.trainer1 = judoka1.pupils_id where training.date>'2013-05-01' group by training.date, training.training_nr order by training.date desc;");
    }
    read_db(store, incoming, getstring,2);
    gtk_tree_view_set_model(GTK_TREE_VIEW(labeltowrite), GTK_TREE_MODEL(store));
    free(getstring);
    g_object_unref(store); /* destroy model automatically with view */
}
/*
 * Function: show_exams_all
 * ----------------------------
 *   callback to display all exams
 *
 *   widget: the button that has called the callback, 
 *    
 *   data: renshu_cb_provider, provides database and output window
 *
 */
void show_exams_all(GtkWidget *widget, gpointer data){
    renshu_cb_provider* incoming = (renshu_cb_provider*)data;
    
    GtkTreeStore* store = gtk_tree_store_new(N_EXAM_COLUMNS, G_TYPE_STRING, G_TYPE_STRING, G_TYPE_STRING, G_TYPE_STRING, G_TYPE_STRING, G_TYPE_INT, G_TYPE_STRING);
    GtkWidget* labeltowrite = GTK_WIDGET(incoming->output);
    /*remove any already set columns/ stores*/
    gtk_tree_view_set_model(GTK_TREE_VIEW(labeltowrite), NULL);
    GList* columns = gtk_tree_view_get_columns(GTK_TREE_VIEW(labeltowrite));
    for(columns = g_list_first(columns); columns != NULL; columns = columns->next){
        gtk_tree_view_remove_column(GTK_TREE_VIEW(labeltowrite), GTK_TREE_VIEW_COLUMN(columns->data));   
    }
    //list no longer needed
    g_list_free(columns);
    GtkCellRenderer *renderer = gtk_cell_renderer_text_new();
    GtkTreeViewColumn* column; 
    column = gtk_tree_view_column_new_with_attributes("Datum", renderer, "text", DATE_COLUMN_EXAM, NULL);
    gtk_tree_view_column_set_sort_column_id(column,DATE_COLUMN_EXAM);
    gtk_tree_view_append_column(GTK_TREE_VIEW(labeltowrite), column);
    column = gtk_tree_view_column_new_with_attributes("Prüfer 1", renderer, "text", EXAMINER0_COLUMN, NULL);
    gtk_tree_view_column_set_sort_column_id(column,EXAMINER0_COLUMN);
    gtk_tree_view_append_column(GTK_TREE_VIEW(labeltowrite), column);
    column = gtk_tree_view_column_new_with_attributes("Prüfer 2", renderer, "text", EXAMINER1_COLUMN, NULL);
    gtk_tree_view_column_set_sort_column_id(column,EXAMINER1_COLUMN);
    gtk_tree_view_append_column(GTK_TREE_VIEW(labeltowrite), column);
    column = gtk_tree_view_column_new_with_attributes("Prüfer 3", renderer, "text", EXAMINER2_COLUMN, NULL);
    gtk_tree_view_column_set_sort_column_id(column,EXAMINER2_COLUMN);
    gtk_tree_view_append_column(GTK_TREE_VIEW(labeltowrite), column);
    column = gtk_tree_view_column_new_with_attributes("Prüfling", renderer, "text", EXAMINED_COLUMN, NULL);
    gtk_tree_view_column_set_sort_column_id(column,EXAMINED_COLUMN);
    gtk_tree_view_append_column(GTK_TREE_VIEW(labeltowrite), column);
    column = gtk_tree_view_column_new_with_attributes("Prüfung zu", renderer, "text", RECEIVED_COLUMN, NULL);
    gtk_tree_view_column_set_sort_column_id(column,RECEIVED_COLUMN);
    gtk_tree_view_append_column(GTK_TREE_VIEW(labeltowrite), column);
    column = gtk_tree_view_column_new_with_attributes("Pruefling_id", renderer, "text", EXAMINED_ID_COLUMN, NULL);
    gtk_tree_view_column_set_visible(column, FALSE);
    gtk_tree_view_append_column(GTK_TREE_VIEW(labeltowrite), column);
    char* getstring;
    asprintf(&getstring, "select exam_id, date,pruefer.prename, pruefer.name, pruefer1.prename, pruefer1.name,pruefer2.prename, pruefer2.name, pruefling.prename, pruefling.name, received_grade, pruefling.pupils_id from exam inner join judoka pruefling on exam.examined=pruefling.pupils_id inner join judoka pruefer on exam.examiner0=pruefer.pupils_id inner join judoka pruefer1 on exam.examiner1=pruefer1.pupils_id inner join judoka pruefer2 on exam.examiner2=pruefer2.pupils_id order by date desc;");
    read_db(store, incoming, getstring,3);
    gtk_tree_view_set_model(GTK_TREE_VIEW(labeltowrite), GTK_TREE_MODEL(store));
    free(getstring);
    g_object_unref(store); /* destroy model automatically with view */
}
/*
 * Function: show_exams_judoka
 * ----------------------------
 *   callback to display all exams of one judoka
 *
 *   widget: the button that has called the callback, 
 *    
 *   data: renshu_cb_provider, provides database and output window
 *
 */
void show_exams_judoka(GtkWidget *widget, gpointer data){
    renshu_cb_provider* incoming = (renshu_cb_provider*)data;
    
    GtkTreeStore* store = gtk_tree_store_new(N_EXAM_COLUMNS, G_TYPE_STRING, G_TYPE_STRING, G_TYPE_STRING, G_TYPE_STRING, G_TYPE_STRING, G_TYPE_INT, G_TYPE_STRING);
    GtkWidget* labeltowrite = GTK_WIDGET(incoming->output);
    /* get chosen id*/
    GtkTreeSelection* selection = gtk_tree_view_get_selection(GTK_TREE_VIEW(labeltowrite));
    GtkTreeModel* thismodel;
    GtkTreeIter thisiter;
    gchar* id;
    gint id_int;
    if(gtk_tree_selection_get_selected(selection, &thismodel, &thisiter)){
        if(gtk_tree_model_get_n_columns(thismodel) == 9){ //we are in judoka window
            gtk_tree_model_get(thismodel, &thisiter,ID_COLUMN,&id_int, -1);
            asprintf(&id,"%d",id_int);
        }else if(gtk_tree_model_get_n_columns(thismodel) == 7){;
            gtk_tree_model_get(thismodel, &thisiter,EXAMINED_ID_COLUMN,&id, -1); // if we are in exam window
       }else{
        error_dialog("function here not possible\n"); 
        return;
    };
    }else{
        error_dialog("no row selected\n"); 
        return;
    }
    g_object_unref(thismodel);
    /*remove any already set columns/ stores*/
    gtk_tree_view_set_model(GTK_TREE_VIEW(labeltowrite), NULL);
    GList* columns = gtk_tree_view_get_columns(GTK_TREE_VIEW(labeltowrite));
    for(columns = g_list_first(columns); columns != NULL; columns = columns->next){
        gtk_tree_view_remove_column(GTK_TREE_VIEW(labeltowrite), GTK_TREE_VIEW_COLUMN(columns->data));   
    }
    //list no longer needed
    g_list_free(columns);
    GtkCellRenderer *renderer = gtk_cell_renderer_text_new();
    GtkTreeViewColumn* column; 
    column = gtk_tree_view_column_new_with_attributes("Datum", renderer, "text", DATE_COLUMN_EXAM, NULL);
    gtk_tree_view_column_set_sort_column_id(column,DATE_COLUMN_EXAM);
    gtk_tree_view_append_column(GTK_TREE_VIEW(labeltowrite), column);
    column = gtk_tree_view_column_new_with_attributes("Prüfer 1", renderer, "text", EXAMINER0_COLUMN, NULL);
    gtk_tree_view_column_set_sort_column_id(column,EXAMINER0_COLUMN);
    gtk_tree_view_append_column(GTK_TREE_VIEW(labeltowrite), column);
    column = gtk_tree_view_column_new_with_attributes("Prüfer 2", renderer, "text", EXAMINER1_COLUMN, NULL);
    gtk_tree_view_column_set_sort_column_id(column,EXAMINER1_COLUMN);
    gtk_tree_view_append_column(GTK_TREE_VIEW(labeltowrite), column);
    column = gtk_tree_view_column_new_with_attributes("Prüfer 3", renderer, "text", EXAMINER2_COLUMN, NULL);
    gtk_tree_view_column_set_sort_column_id(column,EXAMINER2_COLUMN);
    gtk_tree_view_append_column(GTK_TREE_VIEW(labeltowrite), column);
    column = gtk_tree_view_column_new_with_attributes("Prüfling", renderer, "text", EXAMINED_COLUMN, NULL);
    gtk_tree_view_append_column(GTK_TREE_VIEW(labeltowrite), column);
    column = gtk_tree_view_column_new_with_attributes("Prüfung zu", renderer, "text", RECEIVED_COLUMN, NULL);
    gtk_tree_view_column_set_sort_column_id(column,RECEIVED_COLUMN);
    gtk_tree_view_append_column(GTK_TREE_VIEW(labeltowrite), column);
    column = gtk_tree_view_column_new_with_attributes("Pruefling_id", renderer, "text", EXAMINED_ID_COLUMN, NULL);
    gtk_tree_view_column_set_visible(column, FALSE);
    gtk_tree_view_append_column(GTK_TREE_VIEW(labeltowrite), column);
    char* getstring;
    asprintf(&getstring, "select exam_id, date,pruefer.prename, pruefer.name,pruefer1.prename, pruefer1.name,pruefer2.prename, pruefer2.name, pruefling.prename, pruefling.name, received_grade, pruefling.pupils_id from exam inner join judoka pruefling on exam.examined=pruefling.pupils_id inner join judoka pruefer on exam.examiner0=pruefer.pupils_id inner join judoka pruefer1 on exam.examiner1=pruefer1.pupils_id inner join judoka pruefer2 on exam.examiner2=pruefer2.pupils_id where pruefling.pupils_id='%s' order by date desc;", id);
    read_db(store, incoming, getstring,9);
    gtk_tree_view_set_model(GTK_TREE_VIEW(labeltowrite), GTK_TREE_MODEL(store));
    g_free(id);
    free(getstring);
    g_object_unref(store); /* destroy model automatically with view */
}
/*
 * Function: show_trainings_judoka
 * ----------------------------
 *   callback to display all trainings of one judoka
 *
 *   widget: the button that has called the callback, 
 *    
 *   data: renshu_cb_provider, provides database and output window
 *
 */
void show_trainings_judoka(GtkWidget *widget, gpointer data){
    renshu_cb_provider* incoming = (renshu_cb_provider*)data;
    
    GtkTreeStore* store = gtk_tree_store_new(N_TRAININGS_COLUMNS, G_TYPE_STRING, G_TYPE_INT, G_TYPE_STRING,G_TYPE_STRING, G_TYPE_STRING, G_TYPE_STRING, G_TYPE_STRING);
    GtkWidget* labeltowrite = GTK_WIDGET(incoming->output);
    /* get chosen id*/
    GtkTreeSelection* selected_row = gtk_tree_view_get_selection(GTK_TREE_VIEW(labeltowrite));
    GtkTreeModel* model;
    GtkTreeIter iter;
    gint judoka_id_int;
    gchar* judoka_id;
    if(gtk_tree_selection_get_selected(selected_row, &model, &iter)){
        gtk_tree_model_get(model, &iter, ID_COLUMN, &judoka_id_int,-1);
        asprintf(&judoka_id,"%d",judoka_id_int);
    }else{
        error_dialog("no row selected\n");  
        return;
    }
    /*remove any already set columns/ stores*/
    gtk_tree_view_set_model(GTK_TREE_VIEW(labeltowrite), NULL);
    GList* columns = gtk_tree_view_get_columns(GTK_TREE_VIEW(labeltowrite));
    for(columns = g_list_first(columns); columns != NULL; columns = columns->next){
        gtk_tree_view_remove_column(GTK_TREE_VIEW(labeltowrite), GTK_TREE_VIEW_COLUMN(columns->data));   
    }
    //list no longer needed
    g_list_free(columns);
    GtkCellRenderer *renderer = gtk_cell_renderer_text_new();
    GtkTreeViewColumn* column; 
    column = gtk_tree_view_column_new_with_attributes("Tag", renderer, "text", DAY_COLUMN_PRO, NULL);
    gtk_tree_view_column_set_sort_column_id(column,DAY_COLUMN_PRO);
    gtk_tree_view_append_column(GTK_TREE_VIEW(labeltowrite), column);
    column = gtk_tree_view_column_new_with_attributes("Nummer", renderer, "text", NR_COLUMN_PRO, NULL);
    gtk_tree_view_column_set_sort_column_id(column,NR_COLUMN_PRO);
    gtk_tree_view_append_column(GTK_TREE_VIEW(labeltowrite), column);
    column = gtk_tree_view_column_new_with_attributes("Datum", renderer, "text", DATE_COLUMN_PRO, NULL);
    gtk_tree_view_column_set_sort_column_id(column,DATE_COLUMN_PRO);
    gtk_tree_view_append_column(GTK_TREE_VIEW(labeltowrite), column);
    column = gtk_tree_view_column_new_with_attributes("Trainer 1", renderer, "text", TRAINER0_COLUMN_PRO, NULL);
    gtk_tree_view_column_set_sort_column_id(column,TRAINER0_COLUMN_PRO);
    gtk_tree_view_append_column(GTK_TREE_VIEW(labeltowrite), column);
    column = gtk_tree_view_column_new_with_attributes("Trainer 2", renderer, "text", TRAINER1_COLUMN_PRO, NULL);
    gtk_tree_view_column_set_sort_column_id(column,TRAINER1_COLUMN_PRO);
    gtk_tree_view_append_column(GTK_TREE_VIEW(labeltowrite), column);
    char* getstring;
    if(incoming->usemysql){
        asprintf(&getstring, "select training.training_id, training.weekday, training.training_nr, training.date, count(*) as anwesend, concat(judoka.prename, ' ', judoka.name) as trainer, concat(judoka1.prename, ' ', judoka1.name) as trainer1 from training inner join judoka on training.trainer0 = judoka.pupils_id inner join judoka judoka1 on training.trainer1 = judoka1.pupils_id where training.pupil='%s' group by training.date, training.training_nr order by training.date desc", judoka_id);
    }else{
        asprintf(&getstring, "select training.training_id, training.weekday, training.training_nr, training.date, count(*) as anwesend, (judoka.prename|| ' '|| judoka.name) as trainer, (judoka1.prename|| ' '|| judoka1.name) as trainer1 from training inner join judoka on training.trainer0 = judoka.pupils_id inner join judoka judoka1 on training.trainer1 = judoka1.pupils_id where training.pupil='%s' group by training.date, training.training_nr order by training.date desc", judoka_id);
    };
    read_db(store, incoming, getstring,1);
    gtk_tree_view_set_model(GTK_TREE_VIEW(labeltowrite), GTK_TREE_MODEL(store));
    g_free(judoka_id);
    free(getstring);
    g_object_unref(store); /* destroy model automatically with view */
}
/*
 * Function: show_competitions_all
 * ----------------------------
 *   callback to display all competitions
 *
 *   widget: the button that has called the callback, 
 *    
 *   data: renshu_cb_provider, provides database and output window
 *
 */
void show_competitions_all(GtkWidget *widget, gpointer data){
    renshu_cb_provider* incoming = (renshu_cb_provider*)data;
    
    GtkTreeStore* store = gtk_tree_store_new(N_COMP_COLUMNS, G_TYPE_STRING, G_TYPE_STRING, G_TYPE_STRING, G_TYPE_STRING, G_TYPE_STRING, G_TYPE_INT, G_TYPE_STRING);
    GtkWidget* labeltowrite = GTK_WIDGET(incoming->output);
    /*remove any already set columns/ stores*/
    gtk_tree_view_set_model(GTK_TREE_VIEW(labeltowrite), NULL);
    GList* columns = gtk_tree_view_get_columns(GTK_TREE_VIEW(labeltowrite));
    for(columns = g_list_first(columns); columns != NULL; columns = columns->next){
        gtk_tree_view_remove_column(GTK_TREE_VIEW(labeltowrite), GTK_TREE_VIEW_COLUMN(columns->data));   
    }
    //list no longer needed
    g_list_free(columns);
    GtkCellRenderer *renderer = gtk_cell_renderer_text_new();
    GtkTreeViewColumn* column; 
    column = gtk_tree_view_column_new_with_attributes("Veranstaltung", renderer, "text", EVENT_COLUMN, NULL);
    gtk_tree_view_append_column(GTK_TREE_VIEW(labeltowrite), column);
    column = gtk_tree_view_column_new_with_attributes("Datum", renderer, "text", DATE_COLUMN_COMP, NULL);
    gtk_tree_view_column_set_sort_column_id(column,DATE_COLUMN_COMP);
    gtk_tree_view_append_column(GTK_TREE_VIEW(labeltowrite), column);
    column = gtk_tree_view_column_new_with_attributes("Ort", renderer, "text", LOCATION_COLUMN, NULL);
    gtk_tree_view_column_set_sort_column_id(column,LOCATION_COLUMN);
    gtk_tree_view_append_column(GTK_TREE_VIEW(labeltowrite), column);
    column = gtk_tree_view_column_new_with_attributes("Kämpfer", renderer, "text", COMPETITOR_COLUMN, NULL);
    gtk_tree_view_column_set_sort_column_id(column,COMPETITOR_COLUMN);
    gtk_tree_view_append_column(GTK_TREE_VIEW(labeltowrite), column);
    column = gtk_tree_view_column_new_with_attributes("Klasse", renderer, "text", CLASS_COLUMN, NULL);
    gtk_tree_view_append_column(GTK_TREE_VIEW(labeltowrite), column);
    column = gtk_tree_view_column_new_with_attributes("Platzierung", renderer, "text", RESULT_COLUMN, NULL);
    gtk_tree_view_column_set_sort_column_id(column,RESULT_COLUMN);
    gtk_tree_view_append_column(GTK_TREE_VIEW(labeltowrite), column);
    column = gtk_tree_view_column_new_with_attributes("Kaempfer_id", renderer, "text", COMPETITOR_ID_COLUMN, NULL);
    gtk_tree_view_column_set_visible(column, FALSE);
    gtk_tree_view_append_column(GTK_TREE_VIEW(labeltowrite), column);
    char* getstring;
    asprintf(&getstring, "select competition_id, competition.name, date,location, judoka.prename, judoka.name, weight, result, judoka.pupils_id from competition inner join judoka on competition.competitor=judoka.pupils_id order by date desc;");
    read_db(store, incoming, getstring,4);
    gtk_tree_view_set_model(GTK_TREE_VIEW(labeltowrite), GTK_TREE_MODEL(store));
    free(getstring);
    g_object_unref(store); /* destroy model automatically with view */
}
/*
 * Function: show_competitions_competitor
 * ----------------------------
 *   callback to display all competitions of a particular competitor
 *
 *   widget: the button that has called the callback, 
 *    
 *   data: renshu_cb_provider, provides database and output window
 *
 */
void show_competitions_competitor(GtkWidget *widget, gpointer data){
    renshu_cb_provider* incoming = (renshu_cb_provider*)data;
    
    GtkTreeStore* store = gtk_tree_store_new(N_COMP_COLUMNS, G_TYPE_STRING, G_TYPE_STRING, G_TYPE_STRING, G_TYPE_STRING, G_TYPE_STRING, G_TYPE_INT, G_TYPE_STRING);
    GtkWidget* labeltowrite = GTK_WIDGET(incoming->output);
    /* get chosen id*/
    GtkTreeSelection* selection = gtk_tree_view_get_selection(GTK_TREE_VIEW(labeltowrite));
    GtkTreeModel* thismodel;
    GtkTreeIter thisiter;
    gint id_int;
    gchar* id;
    if(gtk_tree_selection_get_selected(selection, &thismodel, &thisiter)){
         if(gtk_tree_model_get_n_columns(thismodel) == 9){ //we are in judoka window
            gtk_tree_model_get(thismodel, &thisiter,ID_COLUMN,&id_int, -1);    
            asprintf(&id,"%d",id_int);
        }else if(gtk_tree_model_get_n_columns(thismodel) == 7){
            gtk_tree_model_get(thismodel, &thisiter,COMPETITOR_ID_COLUMN,&id, -1); // if we are in competitions window
        }else{
        error_dialog("function not possible here\n"); 
        return;
    }
    }else{
        error_dialog("no row selected\n"); 
        return;
    }
    g_object_unref(thismodel);
    /*remove any already set columns/ stores*/
    gtk_tree_view_set_model(GTK_TREE_VIEW(labeltowrite), NULL);
    GList* columns = gtk_tree_view_get_columns(GTK_TREE_VIEW(labeltowrite));
    for(columns = g_list_first(columns); columns != NULL; columns = columns->next){
        gtk_tree_view_remove_column(GTK_TREE_VIEW(labeltowrite), GTK_TREE_VIEW_COLUMN(columns->data));   
    }
    //list no longer needed
    g_list_free(columns);
    GtkCellRenderer *renderer = gtk_cell_renderer_text_new();
    GtkTreeViewColumn* column; 
    column = gtk_tree_view_column_new_with_attributes("Veranstaltung", renderer, "text", EVENT_COLUMN, NULL);
    gtk_tree_view_append_column(GTK_TREE_VIEW(labeltowrite), column);
    column = gtk_tree_view_column_new_with_attributes("Datum", renderer, "text", DATE_COLUMN_COMP, NULL);
    gtk_tree_view_column_set_sort_column_id(column,DATE_COLUMN_COMP);
    gtk_tree_view_append_column(GTK_TREE_VIEW(labeltowrite), column);
    column = gtk_tree_view_column_new_with_attributes("Ort", renderer, "text", LOCATION_COLUMN, NULL);
    gtk_tree_view_column_set_sort_column_id(column,LOCATION_COLUMN);
    gtk_tree_view_append_column(GTK_TREE_VIEW(labeltowrite), column);
    column = gtk_tree_view_column_new_with_attributes("Kämpfer", renderer, "text", COMPETITOR_COLUMN, NULL);
    gtk_tree_view_append_column(GTK_TREE_VIEW(labeltowrite), column);
    column = gtk_tree_view_column_new_with_attributes("Klasse", renderer, "text", CLASS_COLUMN, NULL);
    gtk_tree_view_append_column(GTK_TREE_VIEW(labeltowrite), column);
    column = gtk_tree_view_column_new_with_attributes("Platzierung", renderer, "text", RESULT_COLUMN, NULL);
    gtk_tree_view_column_set_sort_column_id(column,RESULT_COLUMN);
    gtk_tree_view_append_column(GTK_TREE_VIEW(labeltowrite), column);
    column = gtk_tree_view_column_new_with_attributes("Kaempfer_id", renderer, "text", COMPETITOR_ID_COLUMN, NULL);
    gtk_tree_view_column_set_visible(column, FALSE);
    gtk_tree_view_append_column(GTK_TREE_VIEW(labeltowrite), column);
    char *getstring;
    asprintf(&getstring, "select competition_id, competition.name, date,location, judoka.prename, judoka.name, weight, result, judoka.pupils_id from competition inner join judoka on competition.competitor=judoka.pupils_id where judoka.pupils_id='%s' order by date desc;", id);
    read_db(store, incoming, getstring,8); //4
    gtk_tree_view_set_model(GTK_TREE_VIEW(labeltowrite), GTK_TREE_MODEL(store));
    g_free(id);
    free(getstring);
    g_object_unref(store); /* destroy model automatically with view */
}
/*
 * Function: show_judoka_age
 * ----------------------------
 *   callback to display all judoka in a ceertain age group depending on the button that has called
 *
 *   widget: the button that has called the callback, 
 *    
 *   data: renshu_cb_provider, provides database and output window
 *
 */
void show_judoka_age(GtkWidget *widget, gpointer data){
    renshu_cb_provider* incoming = (renshu_cb_provider*)data;
    
    GtkMenuItem* caller = (GtkMenuItem*) widget;
    GtkTreeStore* store = gtk_tree_store_new(N_COLUMNS, G_TYPE_INT, G_TYPE_STRING, G_TYPE_STRING, G_TYPE_STRING, G_TYPE_INT, G_TYPE_STRING, G_TYPE_STRING, G_TYPE_STRING, G_TYPE_STRING);
    GtkWidget* labeltowrite = GTK_WIDGET(incoming->output);
    /*remove any already set columns/ stores*/
    gtk_tree_view_set_model(GTK_TREE_VIEW(labeltowrite), NULL);
    GList* columns = gtk_tree_view_get_columns(GTK_TREE_VIEW(labeltowrite));
    for(columns = g_list_first(columns); columns != NULL; columns = columns->next){
        gtk_tree_view_remove_column(GTK_TREE_VIEW(labeltowrite), GTK_TREE_VIEW_COLUMN(columns->data));   
    }
    //list no longer needed
    g_list_free(columns);
    GtkCellRenderer *renderer = gtk_cell_renderer_text_new();
    GtkTreeViewColumn* column; 
    column = gtk_tree_view_column_new_with_attributes("ID", renderer, "text", ID_COLUMN, NULL);
    gtk_tree_view_column_set_sort_column_id(column,ID_COLUMN);
    gtk_tree_view_append_column(GTK_TREE_VIEW(labeltowrite), column);
    column = gtk_tree_view_column_new_with_attributes("Name", renderer, "text", NAME_COLUMN, NULL);
    gtk_tree_view_column_set_sort_column_id(column,NAME_COLUMN);
    gtk_tree_view_append_column(GTK_TREE_VIEW(labeltowrite), column);
    column = gtk_tree_view_column_new_with_attributes("Vorname", renderer, "text", PRENAME_COLUMN, NULL);
    gtk_tree_view_column_set_sort_column_id(column,PRENAME_COLUMN);
    gtk_tree_view_append_column(GTK_TREE_VIEW(labeltowrite), column);
    column = gtk_tree_view_column_new_with_attributes("Geburtsdatum", renderer, "text", YOB_COLUMN, NULL);
    gtk_tree_view_column_set_sort_column_id(column,YOB_COLUMN);
    gtk_tree_view_append_column(GTK_TREE_VIEW(labeltowrite), column);
    column = gtk_tree_view_column_new_with_attributes("Graduierung", renderer, "text", GRADUATION_COLUMN, NULL);
    gtk_tree_view_column_set_sort_column_id(column,GRADUATION_COLUMN);
    gtk_tree_view_append_column(GTK_TREE_VIEW(labeltowrite), column);
    column = gtk_tree_view_column_new_with_attributes("Lizenz", renderer, "text", LICENSE_COLUMN, NULL);
    gtk_tree_view_column_set_sort_column_id(column,LICENSE_COLUMN);
    gtk_tree_view_append_column(GTK_TREE_VIEW(labeltowrite), column);
    column = gtk_tree_view_column_new_with_attributes("Kontakt", renderer, "text", CONTACT_COLUMN, NULL);
    gtk_tree_view_column_set_sort_column_id(column,CONTACT_COLUMN);
    gtk_tree_view_append_column(GTK_TREE_VIEW(labeltowrite), column);
    column = gtk_tree_view_column_new_with_attributes("Passnummer", renderer, "text", PASSPORT_COLUMN, NULL);
    gtk_tree_view_column_set_sort_column_id(column,PASSPORT_COLUMN);
    gtk_tree_view_append_column(GTK_TREE_VIEW(labeltowrite), column);
    column = gtk_tree_view_column_new_with_attributes("Bemerkungen", renderer, "text", NOTES_COLUMN, NULL);
    gtk_tree_view_column_set_sort_column_id(column,NOTES_COLUMN);
    gtk_tree_view_append_column(GTK_TREE_VIEW(labeltowrite), column);
    char* bufferstring = "";
    if(!(GTK_IS_MENU_ITEM(caller))){
        asprintf(&bufferstring, "SELECT pupils_id,name, prename, date_of_birth,graduation,rates.identifier,contact,passport,notes from judoka inner join rates on judoka.license = rates.rateid where display='1' order by name asc;");
    }else{
        if(incoming->usemysql){
            if(strcmp(gtk_menu_item_get_label(caller), "u10") == 0){
                asprintf(&bufferstring, "SELECT pupils_id,name, prename, date_of_birth,graduation, rates.identifier,contact,passport,notes from judoka inner join rates on judoka.license = rates.rateid where date_of_birth > (select date_sub((select last_day(date_add(now(),interval 12-month(now()) month))), interval 10 year)) and display='1' order by name asc;");
            }else if(strcmp(gtk_menu_item_get_label(caller), "u12") == 0){
                asprintf(&bufferstring, "SELECT pupils_id,name, prename, date_of_birth,graduation, rates.identifier,contact,passport,notes from judoka inner join rates on judoka.license = rates.rateid where date_of_birth > (select date_sub((select last_day(date_add(now(),interval 12-month(now()) month))), interval 12 year)) and date_of_birth < (select date_sub((select last_day(date_add(now(),interval 12-month(now()) month))), interval 10 year)) and display='1' order by name asc;");
            }else if(strcmp(gtk_menu_item_get_label(caller), "u15") == 0){
                asprintf(&bufferstring, "SELECT pupils_id,name, prename, date_of_birth,graduation, rates.identifier,contact,passport,notes from judoka inner join rates on judoka.license = rates.rateid where date_of_birth > (select date_sub((select last_day(date_add(now(),interval 12-month(now()) month))),interval 15 year)) and date_of_birth < (select date_sub((select last_day(date_add(now(),interval 12-month(now()) month))), interval 12 year)) and display='1' order by name asc;");
            }else if(strcmp(gtk_menu_item_get_label(caller), "u18") == 0){
                asprintf(&bufferstring, "SELECT pupils_id,name, prename, date_of_birth,graduation, rates.identifier,contact,passport,notes from judoka inner join rates on judoka.license = rates.rateid where date_of_birth > (select date_sub((select last_day(date_add(now(),interval 12-month(now()) month))), interval 18 year)) and date_of_birth < (select date_sub((select last_day(date_add(now(),interval 12-month(now()) month))), interval 15 year)) and display='1' order by name asc;");
            }else if(strcmp(gtk_menu_item_get_label(caller), "u21") == 0){
                asprintf(&bufferstring, "SELECT pupils_id,name, prename, date_of_birth,graduation, rates.identifier,contact,passport,notes from judoka inner join rates on judoka.license = rates.rateid where date_of_birth >(select date_sub((select last_day(date_add(now(),interval 12-month(now()) month))), interval 21 year)) and date_of_birth <(select date_sub((select last_day(date_add(now(),interval 12-month(now()) month))), interval 18 year)) and display='1' order by name asc;");
            }else if(strcmp(gtk_menu_item_get_label(caller), "Aktive") == 0){
                asprintf(&bufferstring, "SELECT pupils_id,name, prename, date_of_birth,graduation, rates.identifier,contact,passport,notes from judoka inner join rates on judoka.license = rates.rateid where date_of_birth <(select date_sub((select last_day(date_add(now(),interval 12-month(now()) month))), interval 21 year)) and display='1' and date_of_birth > '0000-01-02' order by name asc;");
            }else{
                asprintf(&bufferstring, "SELECT pupils_id,name, prename, date_of_birth,graduation, rates.identifier,contact,passport,notes from judoka inner join rates on judoka.license = rates.rateid where display='1' order by name asc;");
            }
        }else{
            if(strcmp(gtk_menu_item_get_label(caller), "u10") == 0){
                asprintf(&bufferstring, "SELECT pupils_id,name, prename, date_of_birth,graduation, rates.identifier,contact,passport,notes from judoka inner join rates on judoka.license = rates.rateid where date_of_birth > (select strftime('%%Y-12-31', 'now', '-10 years')) and display='1' order by name asc;");
            }else if(strcmp(gtk_menu_item_get_label(caller), "u12") == 0){
                asprintf(&bufferstring, "SELECT pupils_id,name, prename, date_of_birth,graduation, rates.identifier,contact,passport,notes from judoka inner join rates on judoka.license = rates.rateid where date_of_birth > (select strftime('%%Y-12-31', 'now','-12 years')) and date_of_birth < (select strftime('%%Y-12-31', 'now', '-10 years')) and display='1' order by name asc;");
            }else if(strcmp(gtk_menu_item_get_label(caller), "u15") == 0){
                asprintf(&bufferstring, "SELECT pupils_id,name, prename, date_of_birth,graduation, rates.identifier,contact,passport,notes from judoka inner join rates on judoka.license = rates.rateid where date_of_birth > (select strftime('%%Y-12-31', 'now', '-15 years')) and date_of_birth < (select strftime('%%Y-12-31', 'now', '-12 years')) and display='1' order by name asc;");
            }else if(strcmp(gtk_menu_item_get_label(caller), "u18") == 0){
                asprintf(&bufferstring, "SELECT pupils_id,name, prename, date_of_birth,graduation, rates.identifier,contact,passport,notes from judoka inner join rates on judoka.license = rates.rateid where date_of_birth > (select strftime('%%Y-12-31', 'now', '-18 years')) and date_of_birth < (select strftime('%%Y-12-31', 'now', '-15 years')) and display='1' order by name asc;");
            }else if(strcmp(gtk_menu_item_get_label(caller), "u21") == 0){
                asprintf(&bufferstring, "SELECT pupils_id,name, prename, date_of_birth,graduation, rates.identifier,contact,passport,notes from judoka inner join rates on judoka.license = rates.rateid where date_of_birth >(select strftime('%%Y-12-31', 'now', '-21 years')) and date_of_birth <(select strftime('%%Y-12-31', 'now', '-18 years')) and display='1' order by name asc;");
            }else if(strcmp(gtk_menu_item_get_label(caller), "Aktive") == 0){
                asprintf(&bufferstring, "SELECT pupils_id,name, prename, date_of_birth,graduation, rates.identifier,contact,passport,notes from judoka inner join rates on judoka.license = rates.rateid where date_of_birth <(select strftime('%%Y-12-31', 'now', '-21 years')) and display='1' and date_of_birth > '0000-01-02' order by name asc;");
            }else{
                asprintf(&bufferstring, "SELECT pupils_id,name, prename, date_of_birth,graduation, rates.identifier,contact,passport,notes from judoka inner join rates on judoka.license = rates.rateid where display='1' order by name asc;");
            }
        }
    }
    
    read_db(store, incoming, bufferstring,0);
    //populate tree with model
    gtk_tree_view_set_model(GTK_TREE_VIEW(labeltowrite), GTK_TREE_MODEL(store));
    free(bufferstring);
    g_object_unref(store); /* destroy model automatically with view */
};

/*
 * Function: show_judoka_training
 * ----------------------------
 *   callback to display all judokas that attended a training
 *
 *   widget: the button that has called the callback, 
 *    
 *   data: renshu_cb_provider, provides database and output window
 *
 */
void show_judoka_training(GtkWidget *widget, gpointer data){
    renshu_cb_provider* incoming = (renshu_cb_provider*)data;
    
    GtkTreeStore* store = gtk_tree_store_new(N_COLUMNS, G_TYPE_INT, G_TYPE_STRING, G_TYPE_STRING, G_TYPE_STRING, G_TYPE_INT, G_TYPE_STRING, G_TYPE_STRING, G_TYPE_STRING, G_TYPE_STRING);
    GtkWidget* labeltowrite = GTK_WIDGET(incoming->output);
    GtkTreeSelection* selection = gtk_tree_view_get_selection(GTK_TREE_VIEW(labeltowrite));
    GtkTreeModel* thismodel;
    GtkTreeIter thisiter;
    GtkTreeIter thatiter;
    gchar* training_day;
    gint training_nr_int;
    gchar* training_nr;
    if(gtk_tree_selection_get_selected(selection, &thismodel, &thisiter)){
        if(gtk_tree_model_get_n_columns(thismodel) == 6 || gtk_tree_model_get_n_columns(thismodel) == 5 ){ //we are in training window
            thatiter = thisiter;   
            gtk_tree_model_get(thismodel, &thisiter,DATE_COLUMN,&training_day, -1);
            gtk_tree_model_get(thismodel, &thatiter, NR_COLUMN, &training_nr_int,-1);
            asprintf(&training_nr,"%d",training_nr_int);
        } else{
            error_dialog("function not possible\n"); 
            return;
        };
    }else{
        error_dialog("no row selected\n"); 
        return;
    }
    g_object_unref(thismodel);
    /*remove any already set columns/ stores*/
    gtk_tree_view_set_model(GTK_TREE_VIEW(labeltowrite), NULL);
    GList* columns = gtk_tree_view_get_columns(GTK_TREE_VIEW(labeltowrite));
    for(columns = g_list_first(columns); columns != NULL; columns = columns->next){
        gtk_tree_view_remove_column(GTK_TREE_VIEW(labeltowrite), GTK_TREE_VIEW_COLUMN(columns->data));   
    }
    //list no longer needed
    g_list_free(columns);
    GtkCellRenderer *renderer = gtk_cell_renderer_text_new();
    GtkTreeViewColumn* column;
    column = gtk_tree_view_column_new_with_attributes("ID", renderer, "text", ID_COLUMN, NULL);
    gtk_tree_view_column_set_sort_column_id(column,ID_COLUMN);
    gtk_tree_view_append_column(GTK_TREE_VIEW(labeltowrite), column);
    column = gtk_tree_view_column_new_with_attributes("Name", renderer, "text", NAME_COLUMN, NULL);
    gtk_tree_view_column_set_sort_column_id(column,NAME_COLUMN);
    gtk_tree_view_append_column(GTK_TREE_VIEW(labeltowrite), column);
    column = gtk_tree_view_column_new_with_attributes("Vorname", renderer, "text", PRENAME_COLUMN, NULL);
    gtk_tree_view_column_set_sort_column_id(column,PRENAME_COLUMN);
    gtk_tree_view_append_column(GTK_TREE_VIEW(labeltowrite), column);
    column = gtk_tree_view_column_new_with_attributes("Geburtsdatum", renderer, "text", YOB_COLUMN, NULL);
    gtk_tree_view_column_set_sort_column_id(column,YOB_COLUMN);
    gtk_tree_view_append_column(GTK_TREE_VIEW(labeltowrite), column);
    column = gtk_tree_view_column_new_with_attributes("Graduierung", renderer, "text", GRADUATION_COLUMN, NULL);
    gtk_tree_view_column_set_sort_column_id(column,GRADUATION_COLUMN);
    gtk_tree_view_append_column(GTK_TREE_VIEW(labeltowrite), column);
    column = gtk_tree_view_column_new_with_attributes("Lizenz", renderer, "text", LICENSE_COLUMN, NULL);
    gtk_tree_view_column_set_sort_column_id(column,LICENSE_COLUMN);
    gtk_tree_view_append_column(GTK_TREE_VIEW(labeltowrite), column);
    column = gtk_tree_view_column_new_with_attributes("Kontakt", renderer, "text", CONTACT_COLUMN, NULL);
    gtk_tree_view_column_set_sort_column_id(column,CONTACT_COLUMN);
    gtk_tree_view_append_column(GTK_TREE_VIEW(labeltowrite), column);
    column = gtk_tree_view_column_new_with_attributes("Passnummer", renderer, "text", PASSPORT_COLUMN, NULL);
    gtk_tree_view_column_set_sort_column_id(column,PASSPORT_COLUMN);
    gtk_tree_view_append_column(GTK_TREE_VIEW(labeltowrite), column);
    column = gtk_tree_view_column_new_with_attributes("Bemerkungen", renderer, "text", NOTES_COLUMN, NULL);
    gtk_tree_view_column_set_sort_column_id(column,NOTES_COLUMN);
    gtk_tree_view_append_column(GTK_TREE_VIEW(labeltowrite), column);
    char* bufferstring;
    asprintf(&bufferstring, "select judoka.pupils_id,judoka.name, judoka.prename, judoka.date_of_birth, judoka.graduation, judoka.license, judoka.contact,judoka.passport, judoka.notes from training inner join judoka on training.pupil=judoka.pupils_id where training.date='%s' and training.training_nr='%s';", training_day, training_nr);
    read_db(store, incoming, bufferstring,0);
    //populate tree with model
    gtk_tree_view_set_model(GTK_TREE_VIEW(labeltowrite), GTK_TREE_MODEL(store));
    g_free(training_day);
    g_free(training_nr);
    free(bufferstring);
    g_object_unref(store); /* destroy model automatically with view */
};

/*
 * Function: search_judoka
 * ----------------------------
 *   callback to search judoka
 *
 *   widget: the dialog that has called the callback containing data
 *    
 *   data: renshu_cb_provider, provides database and output window
 *
 */
void search_judoka(GtkWidget *widget, gpointer data){
     renshu_cb_provider* incoming = (renshu_cb_provider*)data;
    
    GtkWidget* caller = widget;
    GtkTreeStore* store = gtk_tree_store_new(N_COLUMNS, G_TYPE_INT, G_TYPE_STRING, G_TYPE_STRING, G_TYPE_STRING, G_TYPE_INT, G_TYPE_STRING, G_TYPE_STRING, G_TYPE_STRING, G_TYPE_STRING);
    GtkWidget* labeltowrite = GTK_WIDGET(incoming->output);
    /*remove any already set columns/ stores*/
    gtk_tree_view_set_model(GTK_TREE_VIEW(labeltowrite), NULL);
    GList* columns = gtk_tree_view_get_columns(GTK_TREE_VIEW(labeltowrite));
    for(columns = g_list_first(columns); columns != NULL; columns = columns->next){
        gtk_tree_view_remove_column(GTK_TREE_VIEW(labeltowrite), GTK_TREE_VIEW_COLUMN(columns->data));   
    }
    //list no longer needed
    g_list_free(columns);
    GtkCellRenderer *renderer = gtk_cell_renderer_text_new();
    GtkTreeViewColumn* column;
    column = gtk_tree_view_column_new_with_attributes("ID", renderer, "text", ID_COLUMN, NULL);
    gtk_tree_view_column_set_sort_column_id(column,ID_COLUMN);
    gtk_tree_view_append_column(GTK_TREE_VIEW(labeltowrite), column);
    column = gtk_tree_view_column_new_with_attributes("Name", renderer, "text", NAME_COLUMN, NULL);
    gtk_tree_view_column_set_sort_column_id(column,NAME_COLUMN);
    gtk_tree_view_append_column(GTK_TREE_VIEW(labeltowrite), column);
    column = gtk_tree_view_column_new_with_attributes("Vorname", renderer, "text", PRENAME_COLUMN, NULL);
    gtk_tree_view_column_set_sort_column_id(column,PRENAME_COLUMN);
    gtk_tree_view_append_column(GTK_TREE_VIEW(labeltowrite), column);
    column = gtk_tree_view_column_new_with_attributes("Geburtsdatum", renderer, "text", YOB_COLUMN, NULL);
    gtk_tree_view_column_set_sort_column_id(column,YOB_COLUMN);
    gtk_tree_view_append_column(GTK_TREE_VIEW(labeltowrite), column);
    column = gtk_tree_view_column_new_with_attributes("Graduierung", renderer, "text", GRADUATION_COLUMN, NULL);
    gtk_tree_view_column_set_sort_column_id(column,GRADUATION_COLUMN);
    gtk_tree_view_append_column(GTK_TREE_VIEW(labeltowrite), column);
    column = gtk_tree_view_column_new_with_attributes("Lizenz", renderer, "text", LICENSE_COLUMN, NULL);
    gtk_tree_view_column_set_sort_column_id(column,LICENSE_COLUMN);
    gtk_tree_view_append_column(GTK_TREE_VIEW(labeltowrite), column);
    column = gtk_tree_view_column_new_with_attributes("Kontakt", renderer, "text", CONTACT_COLUMN, NULL);
    gtk_tree_view_column_set_sort_column_id(column,CONTACT_COLUMN);
    gtk_tree_view_append_column(GTK_TREE_VIEW(labeltowrite), column);
    column = gtk_tree_view_column_new_with_attributes("Passnummer", renderer, "text", PASSPORT_COLUMN, NULL);
    gtk_tree_view_column_set_sort_column_id(column,PASSPORT_COLUMN);
    gtk_tree_view_append_column(GTK_TREE_VIEW(labeltowrite), column);
    column = gtk_tree_view_column_new_with_attributes("Bemerkungen", renderer, "text", NOTES_COLUMN, NULL);
    gtk_tree_view_column_set_sort_column_id(column,NOTES_COLUMN);
    gtk_tree_view_append_column(GTK_TREE_VIEW(labeltowrite), column);
    char* bufferstring = "";
    char arguments[1][100];
    int i = 0;
    
    GtkWidget* content = gtk_dialog_get_content_area(GTK_DIALOG(caller));
    GList* children = gtk_container_get_children(GTK_CONTAINER(content)); 
    for(children = g_list_first(children); children != NULL; children = children->next){
        if(GTK_IS_ENTRY(children->data)){
            const gchar* text = gtk_entry_get_text(GTK_ENTRY(children->data));
            sprintf(arguments[i],"%s", text);
            i++;
        }
    }
    
    asprintf(&bufferstring, "SELECT pupils_id,name, prename, date_of_birth,graduation, rates.identifier,contact,passport,notes from judoka  inner join rates on judoka.license = rates.rateid where  (name like '%%%s%%' or prename like '%%%s%%' or rates.identifier like '%%%s%%' or notes like '%%%s%%' or passport like '%%%s%%')", arguments[0], arguments[0],arguments[0],arguments[0],arguments[0]);
    read_db(store, incoming, bufferstring,0);
    //populate tree with model
    gtk_tree_view_set_model(GTK_TREE_VIEW(labeltowrite), GTK_TREE_MODEL(store));
    free(bufferstring);
    g_object_unref(store); /* destroy model automatically with view */
};

int read_config_file(const char* filename, char result[9][200]){
    int i = 0;
    char buf[bufSize];
    
    FILE*f;
    
    f = fopen(filename, "r");
    if (f == NULL){
        error_dialog("error opening configfile");
        return 1;
    }
    
     while (fgets(buf, sizeof(buf), f) != NULL) {
         buf[strlen(buf) - 1] = '\0'; // eat the newline fgets() stores
        sprintf(result[i],"%s", buf);
        i++;
    }

    fclose(f);
    return 0;
};