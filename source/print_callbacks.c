#define _GNU_SOURCE
#include <gtk/gtk.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <time.h>
#include "common.h"
#include <math.h>
#include "dialog_callbacks.h"
#include "read_db_callbacks.h"

#ifdef WIN32
#if 0
#include <windef.h>
#include <ansidecl.h>
#include <winbase.h>
#include <winnt.h>
#endif
#include "sqlite3.h"
#else
#include <sqlite3.h>
#endif

//Safe asprintf macro to avoid leaking memory by appending stuff
#define SASPRINTF(target, args...) ({char* pointer = target; asprintf(&(target), args); free(pointer);})


typedef struct Pupil{
    char* name;
    char* prename;
    char* yob;
    char* grad;
    char** dates;
    int count_dates;
} Pupil;

typedef struct Training{
    char* weekday;
    char* nr;
    char** dates;
    char** attended;
    Pupil pupils[100];
    int count_pupils;
    int count_dates;
} Training;


/*
 * Function: found
 * ----------------------------
 *   checks, whether a string is contained in an array of strings 
 *   within the range range
 *
 *   array: array of strings to be checked, 
 *   target: string to search for 
 *   range: max range to search 
 * 
 *   returns: if string is found
 *
 */
gboolean found(char* array[25], char* target, int range){
    for(int i = 0; i<range; i++){
        if(strcmp(array[i], target) == 0){
            return TRUE;
        }
    }
    return FALSE;
}

/*
 * Function: print_trainings
 * ----------------------------
 *   prints a list of all trainings with a range of the last 6 months
 *
 *   widget: the button that has called the callback, 
 *   passed to the save dialog 
 *   data: renshu_cb_provider, provides database and output window
 *
 */
void print_trainings(GtkWidget *widget, gpointer data){
    renshu_cb_provider* incoming = (renshu_cb_provider*) data;
    GtkWidget* dialog = widget;
    GtkWidget* content = gtk_dialog_get_content_area(GTK_DIALOG(dialog));
    GList* children = gtk_container_get_children(GTK_CONTAINER(content)); 
    char* arguments[5];
    int i = 0;
    for(children = g_list_first(children); children != NULL; children = children->next){
        if(GTK_IS_COMBO_BOX_TEXT(children->data)){
            const gchar* text = gtk_combo_box_get_active_id(GTK_COMBO_BOX(children->data));
            asprintf(&arguments[i],"%s", text);
            i++;
        }
    }
    
    //parse slot
    
    char* training_text = strtok(arguments[4], " \n");
    char* initial_weekday;
    char* initial_training_nr;
    int k = 0;
    while(training_text != NULL){
        if(k%2==0){
            asprintf(&initial_weekday, "%s", training_text);
        }else{
            asprintf(&initial_training_nr, "%s", training_text);
        }
        training_text = strtok(NULL, " \n");
        k++;
    }
    
    //Begin with initial file setup: 
    char* output = NULL;
    asprintf(&output, "<style>\nth.rotate {\nwidth:30px;\nwhite-space: nowrap;\n}\ntd.rotate {\n  width:30px;\n  white-space: nowrap;\n}\nth.rotate > div {\n  transform: \ntranslate(0px,-7px)\nrotate(270deg);\nwidth: 30px;\n}\ntd.rotate > div {\ntransform: \ntranslate(0px,-7px)\nrotate(270deg);\n  width: 30px;\n}\ntd.date{\nwidth: 30px;\ntext-align:center;\nborder: 1px solid #ccc;\n}\ntd.firstrow{\nwidth: 100px;\nborder: 1px solid #ccc;\n}\nth.firstrow{\nwidth: 100px;\n}\ntable{\nmargin-top:100px;\nborder-collapse:collapse;\npage-break-after: always;\n}\n</style>\n <meta charset='UTF-8'> \n<body>\n");
    
    //Build a List of trainings:
    GtkTreeStore* store = gtk_tree_store_new(N_TRAININGS_COLUMNS, G_TYPE_STRING, G_TYPE_INT, G_TYPE_STRING, G_TYPE_INT, G_TYPE_STRING, G_TYPE_STRING, G_TYPE_INT);
    char* trainingsquery = "";
    asprintf (&trainingsquery, "select training_id, weekday, training_nr, date, count(*),trainer0,trainer1 as anwesend from training where date between '%s-%s-01' and '%s-%s-31' and weekday='%s' and training_nr='%s' group by weekday, training_nr order by date desc;", arguments[1], arguments[0], arguments[3], arguments[2], initial_weekday, initial_training_nr);
    read_db(store,incoming,trainingsquery, 7);
    GtkTreeIter iter;
    gboolean is_not_empty; 
    is_not_empty = gtk_tree_model_get_iter_first(GTK_TREE_MODEL(store), &iter);
    
    free(initial_training_nr);
    free(initial_weekday);
    
    Training trainings[15];
    char* datequery = "";
    char* pupilquery = "";
    char* datequery_pupil = "";
    int rowcount = 0;
    double percentage = 0.0;
    gchar* weekday;
    //gchar* nr;
    gint nr_int;
    while(is_not_empty){
        gtk_tree_model_get(GTK_TREE_MODEL(store), &iter, DAY_COLUMN, &weekday, NR_COLUMN, &nr_int, -1);
        asprintf(&trainings[rowcount].weekday,"%s", weekday);
        asprintf(&trainings[rowcount].nr,"%d", nr_int);
        trainings[rowcount].count_dates = 0;
        trainings[rowcount].count_pupils = 0;
        trainings[rowcount].dates = NULL;
        trainings[rowcount].attended = NULL;
        is_not_empty = gtk_tree_model_iter_next(GTK_TREE_MODEL(store), &iter);
        rowcount++;
    }
    free(weekday);
    //free(nr);
    for(int i = 0; i<rowcount; i++){
        percentage = 0.0;
        //set heading in output
        SASPRINTF(output, "%s <h2> %s %s </h2>\n <table>\n <tr><th class='firstrow'>Schüler</th><th class='rotate'><div><span>Geburtsdatum</span></div></th><th class='rotate'><div><span>Graduierung</span></div></th>", output, trainings[i].weekday, trainings[i].nr);
        //get dates for each training
        GtkTreeStore* datestore = gtk_tree_store_new(N_TRAININGS_COLUMNS, G_TYPE_STRING, G_TYPE_INT, G_TYPE_STRING, G_TYPE_INT, G_TYPE_STRING, G_TYPE_STRING, G_TYPE_INT);
        asprintf(&datequery, "select training_id, weekday, training_nr, date, count(*) as anwesend, trainer0, trainer1 from training where date between '%s-%s-01' and '%s-%s-31' and weekday ='%s' and training_nr='%s' group by date order by date asc;",arguments[1], arguments[0],arguments[3], arguments[2], trainings[i].weekday, trainings[i].nr); 
        read_db(datestore,incoming,datequery, 7);
        free(datequery);
        GtkTreeIter iter_dates;
        gboolean valid_dates_training; 
        valid_dates_training = gtk_tree_model_get_iter_first(GTK_TREE_MODEL(datestore), &iter_dates);
        int dates_count = 0;
        while(valid_dates_training){
            gchar* dateentry;
            //gchar* count;
            gint count_int;
            gtk_tree_model_get(GTK_TREE_MODEL(datestore), &iter_dates, DATE_COLUMN, &dateentry,COUNT_COLUMN, &count_int,-1);
            trainings[i].dates = realloc(trainings[i].dates, (dates_count+1) * sizeof(char*));
            trainings[i].attended = realloc(trainings[i].attended, (dates_count+1) * sizeof(char*));
            asprintf(&trainings[i].dates[dates_count],"%s", dateentry);
            asprintf(&trainings[i].attended[dates_count],"%d", count_int);
            valid_dates_training = gtk_tree_model_iter_next(GTK_TREE_MODEL(datestore), &iter_dates);
            dates_count++;
            g_free(dateentry);
           // g_free(count);
        }
        g_object_unref(datestore);
        trainings[i].count_dates = dates_count;
        //after havin acquired all the trainings dates, we can now print them to our list:
        for(int k = 0; k<dates_count; k++){
            SASPRINTF(output, "%s <td class='rotate'><div><span>%s</span></div></td>", output, trainings[i].dates[k]);    
        }
        SASPRINTF(output, "%s <td class='rotate'><div><span>%% anwesend</span></div></td> </tr>", output);

        SASPRINTF(output, "%s <tr><td></td><td></td><td></td>", output);
        for(int m = 0; m<dates_count; m++){
            SASPRINTF(output, "%s <td class='date'>%s</td>", output, trainings[i].attended[m]);    
        }
        SASPRINTF(output, "%s <td></td></tr>", output);
        
        //get pupils for each training
        GtkTreeStore* pupilstore = gtk_tree_store_new(N_COLUMNS, G_TYPE_INT, G_TYPE_STRING, G_TYPE_STRING, G_TYPE_STRING, G_TYPE_INT, G_TYPE_STRING, G_TYPE_STRING, G_TYPE_STRING, G_TYPE_INT);
        asprintf(&pupilquery, "select judoka.pupils_id, judoka.name, judoka.prename, judoka.date_of_birth, judoka.graduation, judoka.license, judoka.contact, judoka.notes,judoka.passport from judoka inner join training on judoka.pupils_id=training.pupil where date between '%s-%s-01' and '%s-%s-31' and weekday ='%s' and training_nr='%s' group by judoka.name, judoka.prename order by name asc;",arguments[1], arguments[0],arguments[3], arguments[2], trainings[i].weekday, trainings[i].nr); 
        read_db(pupilstore,incoming,pupilquery, 0);
        free(pupilquery);
        GtkTreeIter iter_pupil;
        gboolean valid_pupil; 
        valid_pupil = gtk_tree_model_get_iter_first(GTK_TREE_MODEL(pupilstore), &iter_pupil);
        int pupil_count = 0;
        while(valid_pupil){
            gchar* name_pupil;
            gchar* prename_pupil; 
          //  gchar* graduation_pupil;
            gchar* yob_pupil;
            gint graduation_pupil_int;
            gtk_tree_model_get(GTK_TREE_MODEL(pupilstore), &iter_pupil, NAME_COLUMN, &name_pupil, PRENAME_COLUMN, &prename_pupil, YOB_COLUMN, &yob_pupil, GRADUATION_COLUMN, &graduation_pupil_int,-1);
            asprintf(&trainings[i].pupils[pupil_count].name,"%s", name_pupil);
            asprintf(&trainings[i].pupils[pupil_count].prename,"%s", prename_pupil);
            asprintf(&trainings[i].pupils[pupil_count].yob,"%s", yob_pupil);
            asprintf(&trainings[i].pupils[pupil_count].grad,"%d", graduation_pupil_int);
            trainings[i].pupils[pupil_count].dates = NULL;
            valid_pupil = gtk_tree_model_iter_next(GTK_TREE_MODEL(pupilstore), &iter_pupil);
            pupil_count++;
            g_free(name_pupil);
            g_free(prename_pupil);
            //g_free(graduation_pupil);
            g_free(yob_pupil);
        }
        g_object_unref(pupilstore);
        trainings[i].count_pupils = pupil_count;
        //get dates for each pupil
        for(int j = 0; j<pupil_count; j++){
            GtkTreeStore* datestore_pupil = gtk_tree_store_new(N_TRAININGS_COLUMNS_PRO, G_TYPE_STRING, G_TYPE_INT, G_TYPE_STRING, G_TYPE_STRING, G_TYPE_STRING);
            asprintf(&datequery_pupil, "select training.training_id, training.weekday, training.training_nr, training.date, training.trainer0, training.trainer1  from judoka inner join training on judoka.pupils_id=training.pupil where date between '%s-%s-01' and '%s-%s-31' and judoka.name ='%s' and judoka.prename='%s' and training.weekday='%s' and training.training_nr='%s' group by training.date;",arguments[1], arguments[0],arguments[3], arguments[2], trainings[i].pupils[j].name, trainings[i].pupils[j].prename, trainings[i].weekday, trainings[i].nr); 
            read_db(datestore_pupil, incoming, datequery_pupil, 6);
            free(datequery_pupil);
            GtkTreeIter iter_date_pupil;
            gboolean valid_date_pupil; 
            valid_date_pupil = gtk_tree_model_get_iter_first(GTK_TREE_MODEL(datestore_pupil), &iter_date_pupil);
            int datecount_pupil = 0;
            while(valid_date_pupil){
                gchar* dateentry_pupil;
                gtk_tree_model_get(GTK_TREE_MODEL(datestore_pupil), &iter_date_pupil, DATE_COLUMN, &dateentry_pupil,-1);
                trainings[i].pupils[j].dates = realloc(trainings[i].pupils[j].dates, (datecount_pupil+1) * sizeof(char*));
                asprintf(&trainings[i].pupils[j].dates[datecount_pupil],"%s", dateentry_pupil);
                valid_date_pupil = gtk_tree_model_iter_next(GTK_TREE_MODEL(datestore_pupil), &iter_date_pupil);
                datecount_pupil++;
                g_free(dateentry_pupil);
            }
            g_object_unref(datestore_pupil);
            trainings[i].pupils[j].count_dates = datecount_pupil;
        }
        //build output: check for each training date, whether it is contained in the pupils dates and then print either x or -
        //one row for each pupil
        for(int l = 0; l<trainings[i].count_pupils; l++){
            if(atoi(trainings[i].pupils[l].grad)<0){ //select, whether pupil has a dan or kyu
                int dan = atoi(trainings[i].pupils[l].grad);
                dan = abs(dan);
                sprintf(trainings[i].pupils[l].grad, "%i", dan);
                SASPRINTF(output, "%s <tr><td class='firstrow'>%s %s</td><td class='date'>%s</td><td class='date'>%s.Dan</td>", output, trainings[i].pupils[l].prename, trainings[i].pupils[l].name, trainings[i].pupils[l].yob, trainings[i].pupils[l].grad);
            }else{
                SASPRINTF(output, "%s <tr><td class='firstrow'>%s %s</td><td class='date'>%s</td><td class='date'>%s.Kyu</td>", output, trainings[i].pupils[l].prename, trainings[i].pupils[l].name, trainings[i].pupils[l].yob, trainings[i].pupils[l].grad);
            }
            //now check for dates of our training and whether it is contained in the datevec of our pupil
            for(int m=0; m<trainings[i].count_dates; m++){
                if(found(trainings[i].pupils[l].dates, trainings[i].dates[m], trainings[i].pupils[l].count_dates)){
                    SASPRINTF(output, "%s <td class='date'>x</td>", output);
                        }else{
                    SASPRINTF(output, "%s <td class='date'>-</td>", output);
                }
            }
            for(int k=0; k<trainings[i].pupils[l].count_dates; k++){
            free(trainings[i].pupils[l].dates[k]);
            }
            percentage = 100.00*((double)(trainings[i].pupils[l].count_dates) / (double)(trainings[i].count_dates));
            free(trainings[i].pupils[l].name);
            free(trainings[i].pupils[l].dates);
            free(trainings[i].pupils[l].prename);
            free(trainings[i].pupils[l].grad);
            free(trainings[i].pupils[l].yob);
            //end of row, finalize
            SASPRINTF(output, "%s <td class='date'>%.2f</td> </tr>\n", output, percentage);
            
        }
        for(int k=0; k<trainings[i].count_dates; k++){
            free(trainings[i].dates[k]);
            free(trainings[i].attended[k]);
            
        }
        free(trainings[i].dates);
        free(trainings[i].attended);
        free(trainings[i].weekday);
        free(trainings[i].nr);
        SASPRINTF(output, "%s </table>\n", output);
        
    }
    SASPRINTF(output, "%s </body>\n", output);
   
    save_dialog(widget, output,0);
    //don't need it anymore, return to heap
    free(output);
    g_object_unref(store);
};

/*
 * Function: print_trainings_pdf
 * ----------------------------
 *   prints a list of all trainings with a range of the last 6 months
 *
 *   widget: the button that has called the callback, 
 *   passed to the save dialog 
 *   data: renshu_cb_provider, provides database and output window
 *
 */
void print_trainings_pdf(GtkWidget *widget, gpointer data){
    renshu_cb_provider* incoming = (renshu_cb_provider*) data;
    GtkWidget* dialog = widget;
    GtkWidget* content = gtk_dialog_get_content_area(GTK_DIALOG(dialog));
    GList* children = gtk_container_get_children(GTK_CONTAINER(content)); 
    char* arguments[5];
    int i = 0;
    for(children = g_list_first(children); children != NULL; children = children->next){
        if(GTK_IS_COMBO_BOX_TEXT(children->data)){
            const gchar* text = gtk_combo_box_get_active_id(GTK_COMBO_BOX(children->data));
            asprintf(&arguments[i],"%s", text);
            i++;
        }
    }
    //parse slot
    
    char* training_text = strtok(arguments[4], " \n");
    char* initial_weekday;
    char* initial_training_nr;
    int k = 0;
    while(training_text != NULL){
        if(k%2==0){
            asprintf(&initial_weekday, "%s", training_text);
        }else{
            asprintf(&initial_training_nr, "%s", training_text);
        }
        training_text = strtok(NULL, " \n");
        k++;
    }
    
    
    //Begin with initial file setup: 
    char* output = NULL;
    asprintf(&output, "\\documentclass[8pt]{scrartcl} \n \\usepackage[utf8]{inputenc} \n \\usepackage[a4paper, left=1cm, right=1cm, top=1cm, bottom=1cm]{geometry}\n \\usepackage{rotating}\n \\usepackage{longtable}\n \\usepackage[ngerman]{babel} \\usepackage{pdflscape} \n \\begin{document} \\begin{landscape}\n");
    
    //Build a List of trainings:
    GtkTreeStore* store = gtk_tree_store_new(N_TRAININGS_COLUMNS, G_TYPE_STRING, G_TYPE_INT, G_TYPE_STRING, G_TYPE_INT, G_TYPE_STRING, G_TYPE_STRING, G_TYPE_INT);
    char* trainingsquery = "";
    asprintf (&trainingsquery, "select training_id, weekday, training_nr, date, count(*) as anwesend, trainer0, trainer1 from training where date between '%s-%s-01' and '%s-%s-31' and weekday='%s' and training_nr='%s' group by weekday, training_nr order by date desc;", arguments[1], arguments[0], arguments[3], arguments[2], initial_weekday, initial_training_nr);
    read_db(store,incoming,trainingsquery, 7);
    GtkTreeIter iter;
    gboolean is_not_empty; 
    is_not_empty = gtk_tree_model_get_iter_first(GTK_TREE_MODEL(store), &iter);
    free(initial_training_nr);
    free(initial_weekday);
    Training trainings[15];
    char* datequery = "";
    char* pupilquery = "";
    char* datequery_pupil = "";
    int rowcount = 0;
    double percentage = 0.0;
    gchar* weekday;
    //gchar* nr;
    gint nr_int;
    while(is_not_empty){
        gtk_tree_model_get(GTK_TREE_MODEL(store), &iter, DAY_COLUMN, &weekday, NR_COLUMN, &nr_int, -1);
        asprintf(&trainings[rowcount].weekday,"%s", weekday);
        asprintf(&trainings[rowcount].nr,"%d", nr_int);
        trainings[rowcount].count_dates = 0;
        trainings[rowcount].count_pupils = 0;
        trainings[rowcount].dates = NULL;
        trainings[rowcount].attended = NULL;
        is_not_empty = gtk_tree_model_iter_next(GTK_TREE_MODEL(store), &iter);
        rowcount++;
    }
    free(weekday);
    //free(nr);
    for(int i = 0; i<rowcount; i++){
        percentage = 0.0;
        //set heading in output
        SASPRINTF(output, "%s \\section*{ %s %s }\n \\begin{longtable}{lll|", output, trainings[i].weekday, trainings[i].nr);
        //get dates for each training
        GtkTreeStore* datestore = gtk_tree_store_new(N_TRAININGS_COLUMNS, G_TYPE_STRING, G_TYPE_INT, G_TYPE_STRING, G_TYPE_INT, G_TYPE_STRING, G_TYPE_STRING);
        asprintf(&datequery, "select training_id, weekday, training_nr, date, count(*) as anwesend, trainer0, trainer1 from training where date between '%s-%s-01' and '%s-%s-31' and weekday ='%s' and training_nr='%s' group by date order by date asc;",arguments[1], arguments[0],arguments[3], arguments[2], trainings[i].weekday, trainings[i].nr); 
        read_db(datestore,incoming,datequery, 7);
        free(datequery);
        GtkTreeIter iter_dates;
        gboolean valid_dates_training; 
        valid_dates_training = gtk_tree_model_get_iter_first(GTK_TREE_MODEL(datestore), &iter_dates);
        int dates_count = 0;
        while(valid_dates_training){
            gchar* dateentry;
            //gchar* count;
            gint count_int;
            gtk_tree_model_get(GTK_TREE_MODEL(datestore), &iter_dates, DATE_COLUMN, &dateentry,COUNT_COLUMN, &count_int,-1);
            trainings[i].dates = realloc(trainings[i].dates, (dates_count+1) * sizeof(char*));
            trainings[i].attended = realloc(trainings[i].attended, (dates_count+1) * sizeof(char*));
            asprintf(&trainings[i].dates[dates_count],"%s", dateentry);
            asprintf(&trainings[i].attended[dates_count],"%d", count_int);
            valid_dates_training = gtk_tree_model_iter_next(GTK_TREE_MODEL(datestore), &iter_dates);
            dates_count++;
            g_free(dateentry);
           // g_free(count);
        }
        g_object_unref(datestore);
        trainings[i].count_dates = dates_count;
        //first make the right amount of columns:
        for(int j = 0; j<dates_count; j++){
            SASPRINTF(output,"%s l", output);
        }
        //then new table 
        SASPRINTF(output,"%s l} \n Schüler & Geb.datum & Grad", output);
        //after havin acquired all the trainings dates, we can now print them to our list:
        for(int k = 0; k<dates_count; k++){
            SASPRINTF(output, "%s & \\begin{sideways} %s \\end{sideways}", output, trainings[i].dates[k]);    
        }
        SASPRINTF(output, "%s & \\begin{sideways} $\\%%$ anwesend \\end{sideways} \\\\\n ", output);

        SASPRINTF(output, "%s & &", output);
        for(int m = 0; m<dates_count; m++){
            SASPRINTF(output, "%s  & %s", output, trainings[i].attended[m]);    
        }
        SASPRINTF(output, "%s & \\\\\n", output);
        
        //get pupils for each training
        GtkTreeStore* pupilstore = gtk_tree_store_new(N_COLUMNS, G_TYPE_INT, G_TYPE_STRING, G_TYPE_STRING, G_TYPE_STRING, G_TYPE_INT, G_TYPE_STRING, G_TYPE_STRING, G_TYPE_STRING, G_TYPE_INT);
        asprintf(&pupilquery, "select judoka.pupils_id, judoka.name, judoka.prename, judoka.date_of_birth, judoka.graduation, judoka.license, judoka.contact, judoka.notes, judoka.passport from judoka inner join training on judoka.pupils_id=training.pupil where date between '%s-%s-01' and '%s-%s-31' and weekday ='%s' and training_nr='%s' group by judoka.name, judoka.prename order by name asc;",arguments[1], arguments[0],arguments[3], arguments[2], trainings[i].weekday, trainings[i].nr); 
        read_db(pupilstore,incoming,pupilquery, 0);
        free(pupilquery);
        GtkTreeIter iter_pupil;
        gboolean valid_pupil; 
        valid_pupil = gtk_tree_model_get_iter_first(GTK_TREE_MODEL(pupilstore), &iter_pupil);
        int pupil_count = 0;
        while(valid_pupil){
            gchar* name_pupil;
            gchar* prename_pupil; 
          //  gchar* graduation_pupil;
            gchar* yob_pupil;
            gint graduation_pupil_int;
            gtk_tree_model_get(GTK_TREE_MODEL(pupilstore), &iter_pupil, NAME_COLUMN, &name_pupil, PRENAME_COLUMN, &prename_pupil, YOB_COLUMN, &yob_pupil, GRADUATION_COLUMN, &graduation_pupil_int,-1);
            asprintf(&trainings[i].pupils[pupil_count].name,"%s", name_pupil);
            asprintf(&trainings[i].pupils[pupil_count].prename,"%s", prename_pupil);
            asprintf(&trainings[i].pupils[pupil_count].yob,"%s", yob_pupil);
            asprintf(&trainings[i].pupils[pupil_count].grad,"%d", graduation_pupil_int);
            trainings[i].pupils[pupil_count].dates = NULL;
            valid_pupil = gtk_tree_model_iter_next(GTK_TREE_MODEL(pupilstore), &iter_pupil);
            pupil_count++;
            g_free(name_pupil);
            g_free(prename_pupil);
            //g_free(graduation_pupil);
            g_free(yob_pupil);
        }
        g_object_unref(pupilstore);
        trainings[i].count_pupils = pupil_count;
        //get dates for each pupil
        for(int j = 0; j<pupil_count; j++){
            GtkTreeStore* datestore_pupil = gtk_tree_store_new(N_TRAININGS_COLUMNS_PRO, G_TYPE_STRING, G_TYPE_INT, G_TYPE_STRING, G_TYPE_STRING, G_TYPE_STRING);
            asprintf(&datequery_pupil, "select training.training_id, training.weekday, training.training_nr, training.date , training.trainer0, training.trainer1 from judoka inner join training on judoka.pupils_id=training.pupil where date between '%s-%s-01' and '%s-%s-31' and judoka.name ='%s' and judoka.prename='%s' and training.weekday='%s' and training.training_nr='%s' group by training.date;",arguments[1], arguments[0],arguments[3], arguments[2], trainings[i].pupils[j].name, trainings[i].pupils[j].prename, trainings[i].weekday, trainings[i].nr); 
            read_db(datestore_pupil, incoming, datequery_pupil, 6);
            free(datequery_pupil);
            GtkTreeIter iter_date_pupil;
            gboolean valid_date_pupil; 
            valid_date_pupil = gtk_tree_model_get_iter_first(GTK_TREE_MODEL(datestore_pupil), &iter_date_pupil);
            int datecount_pupil = 0;
            while(valid_date_pupil){
                gchar* dateentry_pupil;
                gtk_tree_model_get(GTK_TREE_MODEL(datestore_pupil), &iter_date_pupil, DATE_COLUMN, &dateentry_pupil,-1);
                trainings[i].pupils[j].dates = realloc(trainings[i].pupils[j].dates, (datecount_pupil+1) * sizeof(char*));
                asprintf(&trainings[i].pupils[j].dates[datecount_pupil],"%s", dateentry_pupil);
                valid_date_pupil = gtk_tree_model_iter_next(GTK_TREE_MODEL(datestore_pupil), &iter_date_pupil);
                datecount_pupil++;
                g_free(dateentry_pupil);
            }
            g_object_unref(datestore_pupil);
            trainings[i].pupils[j].count_dates = datecount_pupil;
        }
        //build output: check for each training date, whether it is contained in the pupils dates and then print either x or -
        //one row for each pupil
        for(int l = 0; l<trainings[i].count_pupils; l++){
            if(atoi(trainings[i].pupils[l].grad)<0){ //select, whether pupil has a dan or kyu
                int dan = atoi(trainings[i].pupils[l].grad);
                dan = abs(dan);
                sprintf(trainings[i].pupils[l].grad, "%i", dan);
                SASPRINTF(output, "%s  %s %s & %s & %s.Dan ", output, trainings[i].pupils[l].prename, trainings[i].pupils[l].name, trainings[i].pupils[l].yob, trainings[i].pupils[l].grad);
            }else{
                SASPRINTF(output, "%s  %s %s & %s& %s.Kyu", output, trainings[i].pupils[l].prename, trainings[i].pupils[l].name, trainings[i].pupils[l].yob, trainings[i].pupils[l].grad);
            }
            //now check for dates of our training and whether it is contained in the datevec of our pupil
            for(int m=0; m<trainings[i].count_dates; m++){
                if(found(trainings[i].pupils[l].dates, trainings[i].dates[m], trainings[i].pupils[l].count_dates)){
                    SASPRINTF(output, "%s & x", output);
                        }else{
                    SASPRINTF(output, "%s & -", output);
                }
            }
            for(int k=0; k<trainings[i].pupils[l].count_dates; k++){
            free(trainings[i].pupils[l].dates[k]);
            }
            percentage = 100.00*((double)(trainings[i].pupils[l].count_dates) / (double)(trainings[i].count_dates));
            free(trainings[i].pupils[l].name);
            free(trainings[i].pupils[l].dates);
            free(trainings[i].pupils[l].prename);
            free(trainings[i].pupils[l].grad);
            free(trainings[i].pupils[l].yob);
            //end of row, finalize
            SASPRINTF(output, "%s & %.2f \\\\\n", output, percentage);
            
        }
        for(int k=0; k<trainings[i].count_dates; k++){
            free(trainings[i].dates[k]);
            free(trainings[i].attended[k]);
            
        }
        free(trainings[i].dates);
        free(trainings[i].attended);
        free(trainings[i].weekday);
        free(trainings[i].nr);
        SASPRINTF(output, "%s \\end{longtable}\n \\pagebreak\n", output);
        
    }
    SASPRINTF(output, "%s \\end{landscape}\n \\end{document}\n", output);
   
    save_dialog(widget, output,1);
    //don't need it anymore, return to heap
    free(output);
    g_object_unref(store);
};

/*
 * Function: print_salary
 * ----------------------------
 *   prints a salary sheet for specified month and year
 *
 *   widget: the dialog that has called the callback, 
 *   contains month and year 
 *   data: renshu_cb_provider, provides database and output window
 *
 */
void print_salary_pdf(GtkWidget *widget, gpointer data){
    renshu_cb_provider * incoming = (renshu_cb_provider*) data;
    GtkWidget* dialog = widget;
    GtkWidget* content = gtk_dialog_get_content_area(GTK_DIALOG(dialog));
    GList* children = gtk_container_get_children(GTK_CONTAINER(content)); 
    char* arguments[4];
    char variables[9][200];
    int i = 0;
    for(children = g_list_first(children); children != NULL; children = children->next){
        if(GTK_IS_COMBO_BOX_TEXT(children->data)){
            const gchar* text = gtk_combo_box_get_active_id(GTK_COMBO_BOX(children->data));
            asprintf(&arguments[i],"%s", text);
            if(i == 0){
                text = gtk_combo_box_text_get_active_text(GTK_COMBO_BOX_TEXT(children->data));    
                asprintf(&arguments[i+1],"%s", text);
                i+=2;
            }else{
                i++;
            }
        }
    }
    //arguments:
    //0 month nr
    //1 month text
    //2 year text
    //3 trainer text
    
    //disassemble trainertext
    
    char* trainer_text = strtok(arguments[3], " \n");
    char* trainer_name;
    char* trainer_prename;
    int k = 0;
    while(trainer_text != NULL){
        if(k%2==0){
            asprintf(&trainer_prename, "%s", trainer_text);
        }else{
            asprintf(&trainer_name, "%s", trainer_text);
        }
        trainer_text = strtok(NULL, " \n");
        k++;
    }
    
    //get config file and deassemble into array
    
    char* configquery;
    asprintf(&configquery,"select configpath from settings where name='%s' and prename='%s'",trainer_name, trainer_prename);
    char* headerquery;
    asprintf(&headerquery,"select headerpath from settings where name='%s' and prename='%s'",trainer_name, trainer_prename);
    
    get_single_entry(variables[5],configquery,incoming);
    get_single_entry(variables[6],headerquery,incoming);
    
    free(configquery);
    free(headerquery);
    
    read_config_file(variables[5],variables);
    
    char payout_string[50];
    char payout_name[50];
    char* payout_query;
    char* payout_query1;
    asprintf(&payout_query,"select rates.rate from judoka inner join rates on judoka.license = rates.rateid where judoka.name='%s' and judoka.prename='%s';",trainer_name,trainer_prename);
    get_single_entry(payout_string,payout_query,incoming);
    asprintf(&payout_query1,"select rates.identifier from judoka inner join rates on judoka.license = rates.rateid where judoka.name='%s' and judoka.prename='%s';",trainer_name,trainer_prename);
    get_single_entry(payout_name,payout_query1,incoming);
    free(payout_query);
    free(payout_query1);
    
    //LEFT OF: get trainer_id
    char trainer_string[50];
    char* trainer_query;
    asprintf(&trainer_query,"select pupils_id from judoka where name='%s' and prename='%s';",trainer_name,trainer_prename);
    get_single_entry(trainer_string,trainer_query,incoming);
    free(trainer_query);
    
    double global_time = 0.0;
    double global_time_hours = 0.0;
    double local_time_hours = 0.0;
    int local_time = 0;
    gboolean first_training = FALSE;
    gboolean second_training = FALSE;
    gboolean no_training = TRUE;
    double payout_factor = 0.0;
    double payout_local = 0.0;
    double payout_global = 0.0;
    gchar pupil_count[50];
    int monthday = 1;
    int trainingnr = 1;
    gchar weekday[50];
    sprintf(weekday, " ");
    sscanf(payout_string, "%lf", &payout_factor);
    char* query = "";
    //get output: 
    char* output = "";
    asprintf(&output, "\\documentclass[9pt]{scrartcl} \n \\usepackage[utf8]{inputenc}\n \\usepackage[ngerman]{babel}\n \\usepackage{multirow}\n \\usepackage{graphicx}\n \\usepackage{eurosym} \n \\usepackage{fancyhdr}\n \\usepackage[a4paper]{geometry}\n \\geometry{left=1.0cm, top=2.5cm, right=1.0cm, bottom=1cm} \n \\begin{document} \n \\setlength{\\parindent}{0pt} \n \\lhead{\\includegraphics[width=\\columnwidth]{%s}}\n \\lfoot{\\empty}\n \\rfoot{\\empty} \n \\cfoot{\\empty} \n \\renewcommand{\\headrulewidth}{0pt}\n \\setlength{\\headsep}{70pt}\n \\pagestyle{fancy} \n \\begin{tabular}{|p{0.15\\columnwidth}p{0.85\\columnwidth}|}\\hline", variables[6]);
    
    //header image
    SASPRINTF(output, "%s  \\textbf{Name:}& %s %s \\\\ \n \\textbf{Strasse:}& %s \\\\ \n \\textbf{PLZ, Ort:}& %s\\\\\\hline \n \\end{tabular} \n \\begin{tabular}{|p{0.15\\columnwidth}p{0.15\\columnwidth}p{0.67\\columnwidth}|}\\hline \n \\textbf{Bank}&Institut:& %s \\\\ \n &IBAN:& %s \\\\ \n &BIC& %s \\\\\\hline \n \\end{tabular}\n \\newline\\newline \\textbf{Monat:} %s \\hspace{0.7\\columnwidth} Jahr: %s \\newline\\newline \n \\begin{tabular}{|c|c|c|c|c|c|c|c|c|c|c|c|c|c|}\\hline \n \\multirow{3}{*}{Tag} & \\multirow{3}{*}{Tätigkeit} & \\multirow{3}{*}{Ausbildung} & \\multicolumn{3}{|c|}{\\textbf{1.Training}} & \\multicolumn{3}{|c|}{\\textbf{2.Training}} & \\multicolumn{3}{|c|}{\\textbf{3.Training}} & \\multirow{3}{*}{Std.} & \\multirow{3}{*}{Betr.}\\\\\\cline{4-12} \n & & & Beg. & Ende & TN & Beg. & Ende & TN & Beg. & Ende & TN & & \\\\\\cline{4-12} \n  & & & hh:mm & hh:mm & & hh:mm & hh:mm & & hh:mm & hh:mm & & & \\\\\\hline \n\n", output, trainer_prename, trainer_name, variables[4], variables[3], variables[0], variables[1], variables[2], arguments[1], arguments[2]);
    
    //start with table: header:
    while (monthday<32){
        //first entry: Day of month: 
        SASPRINTF(output, "%s %d", output, monthday);
        //reset any local variables specific for that row: 
        local_time = 0;
        local_time_hours = 0.0;
        no_training = TRUE; 
        first_training = FALSE;
        second_training = FALSE; 
        sprintf(weekday, " ");
        trainingnr = 0;
        while(trainingnr < 4){
            //reset pupilcount beause it is training-specific
            sprintf(pupil_count, " ");
            sprintf(weekday, " ");
            //get weekday
             asprintf(&query, "select weekday from training where date='%s-%s-%02d' and training_nr='%d' and (trainer0='%s' or trainer1='%s') group by weekday;", arguments[2], arguments[0], monthday, trainingnr, trainer_string, trainer_string);
            get_single_entry(weekday, query, incoming);
            //get pupil count for that specific training: 
            sprintf(query, "select count(*) from training where date='%s-%s-%02d' and training_nr='%d' and (trainer0='%s' or trainer1='%s');", arguments[2], arguments[0], monthday, trainingnr,trainer_string, trainer_string);
            get_single_entry(pupil_count, query, incoming);
            //if exists training on said day (with the id of course): 
            if(strcmp(weekday, " ")!=0){
                //enter "Training" and qualification
                if(no_training){
                    SASPRINTF(output, "%s & Training & %s", output, payout_name);
                    no_training = FALSE;
                }
                //if there was already a training set the training:
                if(first_training){
                    second_training = TRUE;
                    first_training = FALSE;
                }else if(second_training){
                    first_training = FALSE;
                    second_training = FALSE;
                }else{
                    first_training = TRUE;
                    second_training = FALSE;
                }
                char begin_string[50];
                char end_string[50];
                char* begin_query;
                char* end_query;
                asprintf(&begin_query, "select begin from slots where day='%s' and nr='%d';", weekday, trainingnr);
                get_single_entry(begin_string, begin_query, incoming);
                asprintf(&end_query, "select end from slots where day='%s' and nr='%d';", weekday, trainingnr);
                get_single_entry(end_string, end_query, incoming);
                free(end_query);
                free(begin_query);
                
                int begin_int = atoi(begin_string);
                int end_int = atoi(end_string);
                
                //ought to work... int always floors the result...doesn't it?
                int diff = end_int - begin_int - ((floor(end_int/100)-floor(begin_int/100))*40);
                SASPRINTF(output, "%s&%s&%s&%s",output,begin_string,end_string,pupil_count);
                local_time += diff;
            }
            trainingnr++;
        }
        //compute payout:
        global_time += local_time;
        local_time_hours = (double)local_time / 60.0;
        payout_local = (double)(local_time_hours)*payout_factor;
        payout_global += payout_local;
        if(no_training){ //there was no training emptystring needed or else layout is fucked up
            SASPRINTF(output, "%s & & & & & & & & & & & & & \\\\\n ", output);  
        }else if(first_training){ //there was only one training
            SASPRINTF(output, "%s & & & & & & & %.2fh & %.2f \\euro\\\\\n",output, local_time_hours, payout_local);
        }else if(second_training){ //there were two trainings
            SASPRINTF(output, "%s & & & & %.2fh & %.2f \\euro\\\\\n",output, local_time_hours, payout_local);
        }else{ //there were 3 trainings
            SASPRINTF(output, "%s & %.2fh & %.2f\\euro\\\\\n",output, local_time_hours, payout_local);
        }
        //now step to the next day and reloop: 
        monthday++;
    }
    //after 31 days comes the final row:
    global_time_hours = (double)(global_time) / 60.0;
    SASPRINTF(output, "%s\\hline& & & & & & & & & & & & %.2fh & %.2f\\euro\\\\\\hline\n \\end{tabular}\n \\newline\\newline\n\\textbf{Ich bestätige meine Angaben:}\\\\\n\\newline\\newline\n\\textbf{Gesehen Abteilungsleitung:}\n\\end{document}",output, global_time_hours, payout_global);
    save_dialog(widget, output,1);
    
    for(int j=0; j<4; j++){
     free(arguments[j]);   
    }
    free(output);
    free(query);
};

/*
 * Function: print_salary
 * ----------------------------
 *   prints a salary sheet for specified month and year
 *
 *   widget: the dialog that has called the callback, 
 *   contains month and year 
 *   data: renshu_cb_provider, provides database and output window
 *
 */
void print_salary(GtkWidget *widget, gpointer data){
    renshu_cb_provider * incoming = (renshu_cb_provider*) data;
    GtkWidget* dialog = widget;
    GtkWidget* content = gtk_dialog_get_content_area(GTK_DIALOG(dialog));
    GList* children = gtk_container_get_children(GTK_CONTAINER(content)); 
    char* arguments[4];
    char variables[9][200];
    int i = 0;
    for(children = g_list_first(children); children != NULL; children = children->next){
        if(GTK_IS_COMBO_BOX_TEXT(children->data)){
            const gchar* text = gtk_combo_box_get_active_id(GTK_COMBO_BOX(children->data));
            asprintf(&arguments[i],"%s", text);
            if(i == 0){
                text = gtk_combo_box_text_get_active_text(GTK_COMBO_BOX_TEXT(children->data));    
                asprintf(&arguments[i+1],"%s", text);
                i+=2;
            }else{
                i++;
            }
        }
    }
    //arguments:
    //0 month nr
    //1 month text
    //2 year text
    //3 trainer text
    
    //disassemble trainertext
    
    char* trainer_text = strtok(arguments[3], " \n");
    char* trainer_name;
    char* trainer_prename;
    int k = 0;
    while(trainer_text != NULL){
        if(k%2==0){
            asprintf(&trainer_prename, "%s", trainer_text);
        }else{
            asprintf(&trainer_name, "%s", trainer_text);
        }
        trainer_text = strtok(NULL, " \n");
        k++;
    }
    
    //get config file and deassemble into array
    
    char* configquery;
    asprintf(&configquery,"select configpath from settings where name='%s' and prename='%s'",trainer_name, trainer_prename);
    char* headerquery;
    asprintf(&headerquery,"select headerpath from settings where name='%s' and prename='%s'",trainer_name, trainer_prename);
    
    get_single_entry(variables[5],configquery,incoming);
    get_single_entry(variables[6],headerquery,incoming);
    
    free(configquery);
    free(headerquery);
    
    read_config_file(variables[5],variables);
    
    char payout_string[50];
    char payout_name[50];
    char* payout_query;
    char* payout_query1;
    asprintf(&payout_query,"select rates.rate from judoka inner join rates on judoka.license = rates.rateid where judoka.name='%s' and judoka.prename='%s';",trainer_name,trainer_prename);
    get_single_entry(payout_string,payout_query,incoming);
    asprintf(&payout_query1,"select rates.identifier from judoka inner join rates on judoka.license = rates.rateid where judoka.name='%s' and judoka.prename='%s';",trainer_name,trainer_prename);
    get_single_entry(payout_name,payout_query1,incoming);
    free(payout_query);
    free(payout_query1);
    
    //LEFT OF: get trainer_id
    char trainer_string[50];
    char* trainer_query;
    asprintf(&trainer_query,"select pupils_id from judoka where name='%s' and prename='%s';",trainer_name,trainer_prename);
    get_single_entry(trainer_string,trainer_query,incoming);
    free(trainer_query);
    
    double global_time = 0.0;
    double global_time_hours = 0.0;
    double local_time_hours = 0.0;
    int local_time = 0;
    gboolean first_training = FALSE;
    gboolean second_training = FALSE;
    gboolean no_training = TRUE;
    double payout_factor = 0.0;
    double payout_local = 0.0;
    double payout_global = 0.0;
    gchar pupil_count[50];
    int monthday = 1;
    int trainingnr = 1;
    gchar weekday[50];
    sprintf(weekday, " ");
    sscanf(payout_string, "%lf", &payout_factor);
    char* query = "";
    //get output: 
    char* output = "";
    asprintf(&output, "<style>\ntable{\nmargin-left:80px;\nborder-collapse:collapse;\n}\ntable.salary{\nmargin-left:00px;\nborder-collapse:collapse;\n}\nth{\ntext-align:left;\n}\n</style>\n <meta charset='UTF-8'> \n<body>\n");
    
    //header image
    SASPRINTF(output, "%s <img src='file://%s' alt='Smiley face' height='66' width='700'>\n", output, variables[6]);
    SASPRINTF(output, "%s <table frame='box'>\n<tr><th>Name:</th><td>%s %s</td></tr>\n<tr><th>Straße:</th><td>%s</td></tr>\n<tr><th>PLZ, Ort:</th><td>%s</td></tr>\n</table>\n<table frame='box'> \n<tr><th>Bank:</th><td>Institut:</td><td>%s</td></tr>\n<tr><th></th><td>IBAN:</td><td>%s</td></tr>\n<tr><th></th><td>BIC:</td><td>%s</td></tr>\n</table>\n<h3>%s %s</h3>\n", output, trainer_prename, trainer_name, variables[4], variables[3], variables[0], variables[1], variables[2], arguments[1], arguments[2]);
    
    //start with table: header:
    SASPRINTF(output, "%s <table class='salary' border='1'>\n<tr><th>Tag</th><th>Tätigkeit</th><th>Ausbildung</th><th colspan='3'>1. Training</th><th colspan='3'>2. Training</th><th colspan='3'>3. Training</th><th>Std.</th><th>Betrag</th></tr>\n<tr><td></td><td></td><td></td><td>Start</td><td>Ende</td><td>TN</td><td>Start</td><td>Ende</td><td>TN</td><td>Start</td><td>Ende</td><td>TN</td><td></td><td></td></tr>\n", output);
    
    while (monthday<32){
        //first entry: Day of month: 
        SASPRINTF(output, "%s <tr><td>%d</td>", output, monthday);
        //reset any local variables specific for that row: 
        local_time = 0;
        local_time_hours = 0.0;
        no_training = TRUE; 
        first_training = FALSE;
        second_training = FALSE; 
        sprintf(weekday, " ");
        trainingnr = 0;
        while(trainingnr < 4){
            //reset pupilcount beause it is training-specific
            sprintf(pupil_count, " ");
            sprintf(weekday, " ");
            //get weekday
            asprintf(&query, "select weekday from training where date='%s-%s-%02d' and training_nr='%d' and (trainer0='%s' or trainer1='%s') group by weekday;", arguments[2], arguments[0], monthday, trainingnr, trainer_string, trainer_string);
            get_single_entry(weekday, query, incoming);
            //get pupil count for that specific training: 
            sprintf(query, "select count(*) from training where date='%s-%s-%02d' and training_nr='%d' and (trainer0='%s' or trainer1='%s');", arguments[2], arguments[0], monthday, trainingnr,trainer_string, trainer_string);
            get_single_entry(pupil_count, query, incoming);
            //if exists training on said day (with the id of course): 
            if(strcmp(weekday, " ")!=0){
                //enter "Training" and qualification
                if(no_training){
                    SASPRINTF(output, "%s<td> Training </td> <td>%s</td>", output, payout_name);
                    no_training = FALSE;
                }
                //if there was already a training set the training:
                if(first_training){
                    second_training = TRUE;
                    first_training = FALSE;
                }else if(second_training){
                    first_training = FALSE;
                    second_training = FALSE;
                }else{
                    first_training = TRUE;
                    second_training = FALSE;
                }
                
                char begin_string[50];
                char end_string[50];
                char* begin_query;
                char* end_query;
                asprintf(&begin_query, "select begin from slots where day='%s' and nr='%d';", weekday, trainingnr);
                get_single_entry(begin_string, begin_query, incoming);
                asprintf(&end_query, "select end from slots where day='%s' and nr='%d';", weekday, trainingnr);
                get_single_entry(end_string, end_query, incoming);
                free(end_query);
                free(begin_query);
                
                int begin_int = atoi(begin_string);
                int end_int = atoi(end_string);
                
                //ought to work... int always floors the result...doesn't it?
                int diff = end_int - begin_int - ((floor(end_int/100)-floor(begin_int/100))*40);
                SASPRINTF(output, "%s<td>%s</td><td>%s</td><td>%s</td>",output,begin_string,end_string,pupil_count);
                local_time += diff;
            }
            trainingnr++;
        }
        //compute payout:
        global_time += local_time;
        local_time_hours = (double)local_time / 60.0;
        payout_local = (double)(local_time_hours)*payout_factor;
        payout_global += payout_local;
        if(no_training){ //there was no training emptystring needed or else layout is fucked up
            SASPRINTF(output, "%s<td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td></tr>\n", output);  
        }else if(first_training){ //there was only one training
            SASPRINTF(output, "%s<td></td><td></td><td></td><td></td><td></td><td></td><td>%.2fh</td><td>%.2f€</td></tr>\n",output, local_time_hours, payout_local);
        }else if(second_training){ //there were two trainings
            SASPRINTF(output, "%s<td></td><td></td><td></td><td>%.2fh</td><td>%.2f€</td></tr>\n",output, local_time_hours, payout_local);
        }else{ //there were 3 trainings
            SASPRINTF(output, "%s<td>%.2fh</td><td>%.2f€</td></tr>\n",output, local_time_hours, payout_local);
        }
        //now step to the next day and reloop: 
        monthday++;
    }
    //after 31 days comes the final row:
    global_time_hours = (double)(global_time) / 60.0;
    SASPRINTF(output, "%s<tr><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td>%.2fh</td><td>%.2f€</td></tr>\n</table>\n<h4>Ich bestätige meine Angaben:</h4>\n<h4>Gesehen Abteilungsleitung:</h4>\n</body>",output, global_time_hours, payout_global);
    save_dialog(widget, output,0);
    
    for(int j=0; j<4; j++){
     free(arguments[j]);   
    }
    free(output);
    free(query);
};


/*
 * Function: print_competitors
 * ----------------------------
 *   prints a list of all competitors in the current year
 *
 *   widget: the button that has called the callback, 
 *   passed to the save dialog 
 *   data: renshu_cb_provider, provides database and output window
 *
 */
void print_competitors(GtkWidget *widget, gpointer data){
    renshu_cb_provider * incoming = (renshu_cb_provider*) data;
    GtkWidget* dialog = widget;
    GtkWidget* content = gtk_dialog_get_content_area(GTK_DIALOG(dialog));
    GList* children = gtk_container_get_children(GTK_CONTAINER(content)); 
    char* arguments[2];
    int j = 0;
    for(children = g_list_first(children); children != NULL; children = children->next){
        if(GTK_IS_COMBO_BOX_TEXT(children->data)){
            const gchar* text = gtk_combo_box_get_active_id(GTK_COMBO_BOX(children->data));
            asprintf(&arguments[j],"%s", text);
            if(j!=2){
                text = gtk_combo_box_text_get_active_text(GTK_COMBO_BOX_TEXT(children->data));    
                asprintf(&arguments[j+1],"%s", text);
                j+=2;
            }else{
                j++;
            }
        }
    }
    char* output = "";
    GtkTreeStore* store = gtk_tree_store_new(N_COMP_COLUMNS, G_TYPE_STRING, G_TYPE_STRING, G_TYPE_STRING, G_TYPE_STRING, G_TYPE_STRING, G_TYPE_INT, G_TYPE_STRING);
    GtkTreeIter iter;
    gchar* yob;
    gchar* event;
    gchar* date;
    gchar* location;
    gchar* name;
    gchar* class;
    gchar* result;
    gint result_int;
    gboolean switched_class = FALSE;
    time_t now = time(0);
    struct tm *ltm = localtime(&now);
    int offset=0;
    int year_current = (1900+ltm->tm_year+offset);
    year_current = atoi(arguments[1]);
    int previous_year = 0;
    int year=0;
    char *previous_name = "";
    int class_counter = 0;
    char* classes[] = {"U10:", "U12:","U15:","U18:","U21:","Aktive:" };
    int switchyears[]={(year_current-5), (year_current-10), (year_current-12),(year_current-15), (year_current-18), (year_current-21), (year_current-90)};
    char *query="";
    asprintf(&query, "select judoka.pupils_id, competition.name, date,location, judoka.prename, judoka.name, weight, result, judoka.date_of_birth from competition inner join judoka on competition.competitor=judoka.pupils_id where date between '%d-01-01' and '%d-12-31' order by judoka.date_of_birth desc, judoka.pupils_id asc;",year_current, year_current); 
    
    read_db(store, incoming,query,8);
    free(query);
    //ID-Column is year of birth
    gboolean is_not_empty; 
    is_not_empty = gtk_tree_model_get_iter_first(GTK_TREE_MODEL(store), &iter);
    asprintf(&output, " <meta charset='UTF-8'> \n<body>\n<h1>Übersicht über die Wettkämpfer nach Altersklassen im Jahr %d</h1>\n", year_current); 
    while(is_not_empty){
        gtk_tree_model_get(GTK_TREE_MODEL(store), &iter, COMPETITOR_ID_COLUMN, &yob, DATE_COLUMN_COMP, &date, COMPETITOR_COLUMN, &name, LOCATION_COLUMN, &location, CLASS_COLUMN, &class,RESULT_COLUMN, &result_int, EVENT_COLUMN, &event, -1);
        sscanf(yob, "%d", &year);
        for(int i = class_counter; i<6; i++){
            if(switchyears[i] >= year && year > switchyears[i+1] && !switched_class){
                class_counter = i; 
            }
        }
        if(switchyears[class_counter] >= year && year > switchyears[class_counter+1]){
            if(!switched_class){
                SASPRINTF(output, "%s\n <h2>%s</h2>\n",output, classes[class_counter]);
                class_counter++;
                switched_class = TRUE;
            }
        }
        if(year != previous_year){//neues Jahr
            switched_class = FALSE;
        }
        if(strcmp(name, previous_name)!=0){//neuer Name
            SASPRINTF(output,"%s \n <h3>%s</h3>\n", output, name);
        }
        // date, event, location, result
        asprintf(&result, "%d", result_int);
        SASPRINTF(output,"%s %s %s \t %s \t", output, date, event, location);
        if(strcmp(result, "-1") == 0){
            SASPRINTF(output,"%s teilg.<br>\n", output);
        }else{
            SASPRINTF(output,"%s %s.Platz<br>\n", output, result);
        }
        asprintf(&previous_name, "%s", name);
        previous_year = year;
        is_not_empty = gtk_tree_model_iter_next(GTK_TREE_MODEL(store), &iter);
    }
    SASPRINTF(output,"%s </body>", output);
    save_dialog(widget, output,0);
};

/*
 * Function: print_competitors_pdf
 * -------------------------------
 *   prints a list of all competitors in the current year
 *
 *   widget: the button that has called the callback, 
 *   passed to the save dialog 
 *   data: renshu_cb_provider, provides database and output window
 *
 */
void print_competitors_pdf(GtkWidget *widget, gpointer data){
    renshu_cb_provider * incoming = (renshu_cb_provider*) data;
    GtkWidget* dialog = widget;
    GtkWidget* content = gtk_dialog_get_content_area(GTK_DIALOG(dialog));
    GList* children = gtk_container_get_children(GTK_CONTAINER(content)); 
    char* arguments[2];
    int j = 0;
    for(children = g_list_first(children); children != NULL; children = children->next){
        if(GTK_IS_COMBO_BOX_TEXT(children->data)){
            const gchar* text = gtk_combo_box_get_active_id(GTK_COMBO_BOX(children->data));
            asprintf(&arguments[j],"%s", text);
            if(j!=2){
                text = gtk_combo_box_text_get_active_text(GTK_COMBO_BOX_TEXT(children->data));    
                asprintf(&arguments[j+1],"%s", text);
                j+=2;
            }else{
                j++;
            }
        }
    }
    char* output = "";
    GtkTreeStore* store = gtk_tree_store_new(N_COMP_COLUMNS, G_TYPE_STRING, G_TYPE_STRING, G_TYPE_STRING, G_TYPE_STRING, G_TYPE_STRING, G_TYPE_INT, G_TYPE_STRING);
    GtkTreeIter iter;
    gchar* yob;
    gchar* event;
    gchar* date;
    gchar* location;
    gchar* name;
    gchar* class;
    gchar* result;
    gint result_int;
    gboolean switched_class = FALSE;
    time_t now = time(0);
    struct tm *ltm = localtime(&now);
    int offset=0;
    int year_current = (1900+ltm->tm_year+offset);
    year_current = atoi(arguments[1]);
    int previous_year = 0;
    int year=0;
    char *previous_name = "";
    int class_counter = 0;
    char* classes[] = {"U10:", "U12:","U15:","U18:","U21:","Aktive:" };
    int switchyears[]={(year_current-5), (year_current-10), (year_current-12),(year_current-15), (year_current-18), (year_current-21), (year_current-90)};
    char *query="";
    asprintf(&query, "select judoka.pupils_id, competition.name, date,location, judoka.prename, judoka.name, weight, result, judoka.date_of_birth from competition inner join judoka on competition.competitor=judoka.pupils_id where date between '%d-01-01' and '%d-12-31' order by judoka.date_of_birth desc, judoka.pupils_id asc;",year_current, year_current); 
    
    read_db(store, incoming,query, 8);
    free(query);
    //ID-Column is year of birth
    gboolean is_not_empty; 
    is_not_empty = gtk_tree_model_get_iter_first(GTK_TREE_MODEL(store), &iter);
    asprintf(&output, "\\documentclass[11pt]{scrartcl} \n \\usepackage[utf8]{inputenc}\n \\usepackage[ngerman]{babel}\n \\usepackage{graphicx}\n \\usepackage[a4paper]{geometry}\n \\geometry{left=2cm, top=2.5cm, right=2cm, bottom=1cm} \n \\begin{document} \n \\setlength{\\parindent}{0pt} \n \\section*{Übersicht über die Wettkämpfer nach Altersklassen im Jahr %d}\n", year_current); 
    while(is_not_empty){
        gtk_tree_model_get(GTK_TREE_MODEL(store), &iter, COMPETITOR_ID_COLUMN, &yob, DATE_COLUMN_COMP, &date, COMPETITOR_COLUMN, &name, LOCATION_COLUMN, &location, CLASS_COLUMN, &class,RESULT_COLUMN, &result_int, EVENT_COLUMN, &event, -1);
        sscanf(yob, "%d", &year);
        for(int i = class_counter; i<6; i++){
            if(switchyears[i] >= year && year > switchyears[i+1] && !switched_class){
                class_counter = i; 
            }
        }
        if(switchyears[class_counter] >= year && year > switchyears[class_counter+1]){
            if(!switched_class){
                SASPRINTF(output, "%s\n \\subsection*{%s}\n",output, classes[class_counter]);
                class_counter++;
                switched_class = TRUE;
            }
        }
        if(year != previous_year){//neues Jahr
            switched_class = FALSE;
        }
        if(strcmp(name, previous_name)!=0){//neuer Name
            SASPRINTF(output,"%s \n \\subsubsection*{%s}\n", output, name);
        }
        // date, event, location, result
        asprintf(&result, "%d", result_int);
        SASPRINTF(output,"%s %s %s , %s :", output, date, event, location);
        if(strcmp(result, "-1") == 0){
            SASPRINTF(output,"%s teilg.\\\\\n", output);
        }else{
            SASPRINTF(output,"%s %s.Platz\\\\\n", output, result);
        }
        asprintf(&previous_name, "%s", name);
        previous_year = year;
        is_not_empty = gtk_tree_model_iter_next(GTK_TREE_MODEL(store), &iter);
    }
    SASPRINTF(output,"%s \\end{document}", output);
    save_dialog(widget, output,1);
};