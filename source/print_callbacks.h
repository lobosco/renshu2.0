#ifndef PRINT_CALLBACKS_H 
#define PRINT_CALLBACKS_H 

//list descriptive part of function--usually just the prototype(s)

void print_trainings(GtkWidget *widget, gpointer data);
void print_salary(GtkWidget *widget, gpointer data);
void print_competitors(GtkWidget *widget, gpointer data);
void print_trainings_pdf(GtkWidget *widget, gpointer data);
void print_salary_pdf(GtkWidget *widget, gpointer data);
void print_competitors_pdf(GtkWidget *widget, gpointer data);

#endif