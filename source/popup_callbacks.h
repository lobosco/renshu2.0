#ifndef POPUP_CALLBACKS_H 
#define POPUP_CALLBACKS_H 

//list descriptive part of function--usually just the prototype(s)

gboolean viewPopupMenu (GtkWidget *treeview, gpointer userdata);
void view_popup_menu (GtkWidget *treeview, GdkEventButton *event, gpointer userdata);
gboolean viewRightClick (GtkWidget *treeview, GdkEventButton *event, gpointer userdata);

#endif