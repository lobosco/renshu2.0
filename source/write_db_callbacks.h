#ifndef WRITE_DB_CALLBACKS_H 
#define WRITE_DB_CALLBACKS_H 

//list descriptive part of function--usually just the prototype(s)

void write_db(const char* query, const renshu_cb_provider* cb_provider);
void write_judoka(GtkWidget *widget, gpointer data);
void update_judoka(GtkWidget *widget, gpointer data);
void write_training_exist(GtkWidget *widget, gpointer data);
void write_competition(GtkWidget *widget, gpointer data);
void write_exam(GtkWidget *widget, gpointer data);
void write_initial(GtkWidget *widget, gpointer data);
void update_settings(GtkWidget *widget, gpointer data);
void add_new_settings(GtkWidget *widget, gpointer data);
void update_exam(GtkWidget *widget, gpointer data);
void update_training(GtkWidget *widget, gpointer data);
void delete_judoka(GtkWidget *widget, gpointer data);
#endif