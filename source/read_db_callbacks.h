
#ifndef READ_DB_CALLBACKS_H 
#define READ_DB_CALLBACKS_H 

#include <mysql.h>


//list descriptive part of function--usually just the prototype(s)
void finish_with_error(MYSQL* con);
GtkWidget* find_child(GtkWidget* parent, const gchar* name);
void read_db(GtkTreeStore* resultbuffer, const renshu_cb_provider* cbprovider, const char* query,const int expected_cols);
int read_callback(void* param, int argc, char**argv, char**column);
void show_trainings_judoka(GtkWidget *widget, gpointer data);
void show_trainings_all(GtkWidget *widget, gpointer data);
void show_trainings_slot(GtkWidget *widget, gpointer data);
void show_exams_all(GtkWidget *widget, gpointer data);
void show_exams_judoka(GtkWidget *widget, gpointer data);
void show_competitions_all(GtkWidget *widget, gpointer data);
void show_competitions_competitor(GtkWidget *widget, gpointer data);
void show_judoka_age(GtkWidget *widget, gpointer data);
void search_judoka(GtkWidget *widget, gpointer data);
void show_judoka_training(GtkWidget *widget, gpointer data);
int read_config_file(const char* filename, char result[9][200]);
#endif