#define _GNU_SOURCE
#include <gtk/gtk.h>
#include "common.h"
#include "read_db_callbacks.h"
#include "dialog_callbacks.h"
#include "print_callbacks.h"
#include "popup_callbacks.h"
#include <string.h>
#include <stdio.h>
#include <stdlib.h>

#ifdef WIN32

#if 0
#include <windef.h>
#include <ansidecl.h>
#include <winbase.h>
#include <winnt.h>
#endif

#endif

gboolean viewPopupMenu (GtkWidget *treeview, gpointer userdata)
  {
    view_popup_menu(treeview, NULL, userdata);

    return TRUE; /* we handled this */
  };


  void view_popup_menu (GtkWidget *treeview, GdkEventButton *event, gpointer userdata)
  {
    GtkWidget *menu, *item_judoka_update, *item_judoka_trainings,*item_judoka_competitions, *item_judoka_exams, *item_trainings_pupils, *item_trainings_update, *item_competition_update, *item_exam_update;

    menu = gtk_menu_new();

    item_judoka_update = gtk_menu_item_new_with_label("Judoka aktualisieren");
    item_judoka_trainings = gtk_menu_item_new_with_label("Trainingsbesuche des Judoka");
    item_judoka_competitions = gtk_menu_item_new_with_label("Wettkämpfe des Judoka");
    item_judoka_exams = gtk_menu_item_new_with_label("Prüfungen des Judoka");
    item_trainings_pupils = gtk_menu_item_new_with_label("Schüler dieses Trainings");
    item_trainings_update = gtk_menu_item_new_with_label("Training aktualisieren");
    item_competition_update = gtk_menu_item_new_with_label("Wettkampf aktualisieren");
    item_exam_update = gtk_menu_item_new_with_label("Prüfung aktualisieren");
    
    g_signal_connect(item_judoka_update, "activate",(GCallback) update_judoka_dialog , userdata);
    g_signal_connect(item_judoka_trainings, "activate",(GCallback) show_trainings_judoka , userdata);
    g_signal_connect(item_judoka_competitions, "activate",(GCallback) show_competitions_competitor , userdata);
    g_signal_connect(item_judoka_exams, "activate",(GCallback) show_exams_judoka , userdata);
    g_signal_connect(item_trainings_pupils, "activate",(GCallback) show_judoka_training , userdata);
    g_signal_connect(item_trainings_update, "activate",(GCallback) update_training_dialog , userdata);
    g_signal_connect(item_competition_update, "activate",(GCallback) update_competition_dialog , userdata);
    g_signal_connect(item_exam_update, "activate",(GCallback) update_exam_dialog , userdata);

    gtk_menu_shell_append(GTK_MENU_SHELL(menu), item_judoka_update);
    gtk_menu_shell_append(GTK_MENU_SHELL(menu), item_judoka_trainings);
    gtk_menu_shell_append(GTK_MENU_SHELL(menu), item_judoka_competitions);
    gtk_menu_shell_append(GTK_MENU_SHELL(menu), item_judoka_exams);
    gtk_menu_shell_append(GTK_MENU_SHELL(menu), item_trainings_pupils);
    gtk_menu_shell_append(GTK_MENU_SHELL(menu), item_trainings_update);
    gtk_menu_shell_append(GTK_MENU_SHELL(menu), item_competition_update);
    gtk_menu_shell_append(GTK_MENU_SHELL(menu), item_exam_update);
    
    gtk_widget_show_all(menu);

    /* Note: event can be NULL here when called from view_onPopupMenu;
     *  gdk_event_get_time() accepts a NULL argument */
    gtk_menu_popup(GTK_MENU(menu), NULL, NULL, NULL, NULL,
                   (event != NULL) ? event->button : 0,
                   gdk_event_get_time((GdkEvent*)event));
  };


  gboolean viewRightClick (GtkWidget *treeview, GdkEventButton *event, gpointer userdata)
  {
    /* single click with the right mouse button? */
    if (event->type == GDK_BUTTON_PRESS  &&  event->button == 3)
    {
      g_print ("Single right click on the tree view.\n");

      /* optional: select row if no row is selected or only
       *  one other row is selected (will only do something
       *  if you set a tree selection mode as described later
       *  in the tutorial) */
      if (1)
      {
        GtkTreeSelection *selection;

        selection = gtk_tree_view_get_selection(GTK_TREE_VIEW(treeview));

        /* Note: gtk_tree_selection_count_selected_rows() does not
         *   exist in gtk+-2.0, only in gtk+ >= v2.2 ! */
        if (gtk_tree_selection_count_selected_rows(selection)  <= 1)
        {
           GtkTreePath *path;

           /* Get tree path for row that was clicked */
           if (gtk_tree_view_get_path_at_pos(GTK_TREE_VIEW(treeview),
                                             (gint) event->x, 
                                             (gint) event->y,
                                             &path, NULL, NULL, NULL))
           {
             gtk_tree_selection_unselect_all(selection);
             gtk_tree_selection_select_path(selection, path);
             gtk_tree_path_free(path);
           }
        }
      } /* end of optional bit */

      view_popup_menu(treeview, event, userdata);

      return TRUE; /* we handled this */
    }

    return FALSE; /* we did not handle this */
  };