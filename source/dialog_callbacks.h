#ifndef DIALOG_CALLBACKS_H 
#define DIALOG_CALLBACKS_H 

//list descriptive part of function--usually just the prototype(s)

void error_dialog(gchar* message);
int single_entry_callback(void* param, int argc, char**argv, char**column);
void get_single_entry(char* result, const char* query, const renshu_cb_provider* dbprovider);
void new_training_dialog(GtkWidget *widget, gpointer data);
void new_training_exist_dialog(GtkWidget *widget, gpointer data);
void new_judoka_dialog(GtkWidget *widget, gpointer data);
void new_exam_dialog(GtkWidget *widget, gpointer data);
void new_comp_dialog(GtkWidget *widget, gpointer data);
void update_settings_dialog(GtkWidget *widget, gpointer data);
void update_judoka_dialog(GtkWidget *widget, gpointer data);
void search_judoka_dialog(GtkWidget *widget, gpointer data);
void about_dialog(GtkWidget* widget, gpointer data);
void file_chooser_dialog(GtkWidget* widget, gpointer data);
void salary_chooser_dialog(GtkWidget* widget, gpointer data);
void save_dialog(GtkWidget* widget, gpointer data, int kind);
void update_config_dialog(GtkWidget* widget, gpointer data);
void new_file_dialog(GtkWidget* widget, gpointer data);
void confirm_dialog(GtkWidget* widget, gpointer data);
void competition_chooser_dialog(GtkWidget* widget, gpointer data);
void list_chooser_dialog(GtkWidget* widget, gpointer data);
void remote_db_dialog(GtkWidget* widget, gpointer data);
void add_config_dialog(GtkWidget* widget, gpointer data);
void update_training_dialog(GtkWidget* widget, gpointer data);
void update_exam_dialog(GtkWidget* widget, gpointer data);
void update_competition_dialog(GtkWidget* widget, gpointer data);
#endif