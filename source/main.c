#define _GNU_SOURCE
#include <gtk/gtk.h>
#include "common.h"
#include "read_db_callbacks.h"
#include "dialog_callbacks.h"
#include "print_callbacks.h"
#include "popup_callbacks.h"
#include <string.h>
#include <stdio.h>
#include <stdlib.h>

#ifdef WIN32

#if 0
#include <windef.h>
#include <ansidecl.h>
#include <winbase.h>
#include <winnt.h>
#endif

#endif
GtkWidget *window;

void displayhelp(){
  printf("Programm usage:\n -m, --mysql \"adress database user passwd\" \t uses mysql Database `database` at `adress` with user `user` and password `passwd`\n -f, --file name \t uses sqlite-db contained in `file`\n -h, --help \t displays this text\n");
};

int main (int argc, char* argv[])
{
    
    gtk_init(&argc, &argv);
    /*Initialize main window:*/
    
    window = gtk_window_new(GTK_WINDOW_TOPLEVEL);
    gtk_window_set_position(GTK_WINDOW(window), GTK_WIN_POS_CENTER);
    gtk_window_set_default_size(GTK_WINDOW(window), 600,400);
    gtk_window_set_title(GTK_WINDOW(window), "Renshu - 2.X");
    
    GtkWidget* vbox = gtk_box_new(GTK_ORIENTATION_VERTICAL, 0);
    gtk_container_add(GTK_CONTAINER(window), vbox);
    
    /* initialize callback struct*/
    
    renshu_cb_provider cb_struct;
    int mysql = 0;
    int file = 0;
    
    //check for arguments: -m or --mysql set mysql datastructure; -f sets sqlite-file: 
    for(int i = 0; i<argc; i++){
        if(!strcmp(argv[i],"-m") || !strcmp(argv[i],"--mysql")){
            mysql = 1;
            cb_struct.usemysql = 1;
            char* argument_text = strtok(argv[i+1], " ");
            int k = 0;
            while(argument_text != NULL){
                if(k%4==0){
                    asprintf(&cb_struct.dbaddr, "%s", argument_text);
                }else if(k%4==1){
                    asprintf(&cb_struct.dbname, "%s", argument_text);
                }else if(k%4==2){
                    asprintf(&cb_struct.dbuser, "%s", argument_text);
                }else{    //list no longer needed
                    asprintf(&cb_struct.dbpasswd, "%s", argument_text);
                }
                argument_text = strtok(NULL, " ");
                k++;
            }
        }
        if(!strcmp(argv[i],"-f") || !strcmp(argv[i],"--file")){
            file = 1;
            asprintf(&cb_struct.dbname,"%s", argv[i+1]);
        }
        if(!strcmp(argv[i],"-h") || !strcmp(argv[i],"--help")){
            displayhelp();
            return 0;
        }
    }    //list no longer needed

    
    if(!mysql){
        asprintf(&cb_struct.dbaddr, " ");
        asprintf(&cb_struct.dbuser, " ");
        asprintf(&cb_struct.dbpasswd, " ");
        cb_struct.usemysql = 0;
    }
    
    if((argc >1) && (!mysql) && (!file)){
        asprintf(&cb_struct.dbname,"%s", argv[1]);
    }
    else if((!file) && (!mysql)){
        asprintf(&cb_struct.dbname, " ");
    }
    
    printf("cb_provider:\n%s %s %s %s\n", cb_struct.dbaddr, cb_struct.dbname, cb_struct.dbuser, cb_struct.dbpasswd); 
    
    
    /* initialize area to view output*/
    cb_struct.output = gtk_tree_view_new();
    gtk_tree_view_set_reorderable(GTK_TREE_VIEW(cb_struct.output), FALSE);
    GtkTreeSelection* selection = gtk_tree_view_get_selection(GTK_TREE_VIEW(cb_struct.output));
    gtk_tree_selection_set_mode(selection, GTK_SELECTION_SINGLE);
    g_signal_connect(GTK_TREE_VIEW(cb_struct.output), "button-press-event", (GCallback) viewRightClick, &cb_struct);
    g_signal_connect(GTK_TREE_VIEW(cb_struct.output), "popup-menu", (GCallback) viewPopupMenu, &cb_struct);
    
    GtkWidget* scrollwindow = gtk_scrolled_window_new(NULL, NULL);
    gtk_scrolled_window_set_min_content_height(GTK_SCROLLED_WINDOW(scrollwindow),350);
    
    GtkWidget* menubar = gtk_menu_bar_new();
    
    /*first menu*/
    
    GtkWidget* menu_renshu = gtk_menu_new();
    
    GtkWidget* item_renshu_top = gtk_menu_item_new_with_label("Renshu");
    GtkWidget* item_renshu_about = gtk_menu_item_new_with_label("About");
    GtkWidget* item_renshu_load = gtk_menu_item_new_with_label("Lade Datenbankdatei");
    GtkWidget* item_renshu_remote = gtk_menu_item_new_with_label("Verbinde mit Datenbank");
    GtkWidget* item_renshu_config = gtk_menu_item_new_with_label("Neue Datenbank einrichten");
    GtkWidget* item_renshu_new_config = gtk_menu_item_new_with_label("Neue Konfiguration hinzufügen");
    GtkWidget* item_renshu_update_config = gtk_menu_item_new_with_label("Konfiguration aktualisieren");
    GtkWidget* item_renshu_quit = gtk_menu_item_new_with_label("Quit");
    
    
    /*second menu */
    GtkWidget* menu_judoka = gtk_menu_new();
    GtkWidget* submenu_judoka_show = gtk_menu_new();
    GtkWidget* item_judoka_top = gtk_menu_item_new_with_label("Judoka");
    GtkWidget* item_judoka_show = gtk_menu_item_new_with_label("Anzeigen");
    GtkWidget* item_judoka_u10 = gtk_menu_item_new_with_label("u10");
    GtkWidget* item_judoka_u12 = gtk_menu_item_new_with_label("u12");
    GtkWidget* item_judoka_u15 = gtk_menu_item_new_with_label("u15");
    GtkWidget* item_judoka_u18 = gtk_menu_item_new_with_label("u18");
    GtkWidget* item_judoka_u21 = gtk_menu_item_new_with_label("u21");
    GtkWidget* item_judoka_active = gtk_menu_item_new_with_label("Aktive");
    GtkWidget* item_judoka_all = gtk_menu_item_new_with_label("Alle");
    GtkWidget* item_judoka_new = gtk_menu_item_new_with_label("Neu erstellen");
    GtkWidget* item_judoka_update = gtk_menu_item_new_with_label("Ausgewählten aktualisieren");
    GtkWidget* item_judoka_delete = gtk_menu_item_new_with_label("Ausgewählten entfernen");
    GtkWidget* item_judoka_trainings = gtk_menu_item_new_with_label("Zeige Trainingsbesuche des Judoka");
     GtkWidget* item_judoka_search = gtk_menu_item_new_with_label("Suche Judoka");
    
    /*second menu */
    GtkWidget* menu_training = gtk_menu_new();
    GtkWidget* item_training_top = gtk_menu_item_new_with_label("Training");
    GtkWidget* item_training_show = gtk_menu_item_new_with_label("Anzeigen");
    GtkWidget* item_training_new = gtk_menu_item_new_with_label("Neu Eintragen");
    GtkWidget* item_training_new_exist = gtk_menu_item_new_with_label("Bestehendes Neu Eintragen");
    GtkWidget* item_training_update = gtk_menu_item_new_with_label("Training aktualisieren");
    GtkWidget* item_training_pupils = gtk_menu_item_new_with_label("Alle Schüler dieses Termins");
    
    /*second menu */
    GtkWidget* menu_exam = gtk_menu_new();
    GtkWidget* item_exam_top = gtk_menu_item_new_with_label("Prüfungen");
    GtkWidget* item_exam_show = gtk_menu_item_new_with_label("Anzeigen");
    GtkWidget* item_exam_new = gtk_menu_item_new_with_label("Neu Eintragen");
    GtkWidget* item_exam_update = gtk_menu_item_new_with_label("Prüfung aktualisieren");
    GtkWidget* item_exam_date = gtk_menu_item_new_with_label("zeige Prüfungen des Judoka");
    
    
    
    /*second menu */
    GtkWidget* menu_competition = gtk_menu_new();
    GtkWidget* item_competition_top = gtk_menu_item_new_with_label("Wettkampf");
    GtkWidget* item_competition_show = gtk_menu_item_new_with_label("Anzeigen");
    GtkWidget* item_competition_new = gtk_menu_item_new_with_label("Neu eintragen");
    GtkWidget* item_competition_update = gtk_menu_item_new_with_label("Aktualisieren");
    GtkWidget* item_competition_date = gtk_menu_item_new_with_label("Kämpfe des Athleten");
    
    
    /*second menu */
    GtkWidget* menu_print = gtk_menu_new();
    GtkWidget* item_print_top = gtk_menu_item_new_with_label("Drucken");
    GtkWidget* item_print_trainings = gtk_menu_item_new_with_label("Trainingsliste");
    GtkWidget* item_print_competitors = gtk_menu_item_new_with_label("Wettkämpfer Jahresübersicht");
    GtkWidget* item_print_salary = gtk_menu_item_new_with_label("Abrechnung");
    
    
    
    /* assemble menubar */
    gtk_menu_item_set_submenu(GTK_MENU_ITEM(item_renshu_top), menu_renshu);
    gtk_menu_item_set_submenu(GTK_MENU_ITEM(item_judoka_top), menu_judoka);
    gtk_menu_item_set_submenu(GTK_MENU_ITEM(item_judoka_show), submenu_judoka_show);
    gtk_menu_item_set_submenu(GTK_MENU_ITEM(item_training_top), menu_training);
    gtk_menu_item_set_submenu(GTK_MENU_ITEM(item_exam_top), menu_exam);
    gtk_menu_item_set_submenu(GTK_MENU_ITEM(item_competition_top), menu_competition);
    gtk_menu_item_set_submenu(GTK_MENU_ITEM(item_print_top), menu_print);
    
    gtk_menu_shell_append(GTK_MENU_SHELL(menubar), item_renshu_top);
    gtk_menu_shell_append(GTK_MENU_SHELL(menubar), item_judoka_top);
    gtk_menu_shell_append(GTK_MENU_SHELL(menubar), item_training_top);
    gtk_menu_shell_append(GTK_MENU_SHELL(menubar), item_exam_top);
    gtk_menu_shell_append(GTK_MENU_SHELL(menubar), item_competition_top);
    gtk_menu_shell_append(GTK_MENU_SHELL(menubar), item_print_top);
    
    gtk_menu_shell_append(GTK_MENU_SHELL(menu_renshu), item_renshu_load);
    gtk_menu_shell_append(GTK_MENU_SHELL(menu_renshu), item_renshu_remote);
    gtk_menu_shell_append(GTK_MENU_SHELL(menu_renshu), item_renshu_config);
    gtk_menu_shell_append(GTK_MENU_SHELL(menu_renshu), item_renshu_new_config);
    gtk_menu_shell_append(GTK_MENU_SHELL(menu_renshu), item_renshu_update_config);
    gtk_menu_shell_append(GTK_MENU_SHELL(menu_renshu), item_renshu_about);
    gtk_menu_shell_append(GTK_MENU_SHELL(menu_renshu), item_renshu_quit);
    
    gtk_menu_shell_append(GTK_MENU_SHELL(submenu_judoka_show), item_judoka_u10);
    gtk_menu_shell_append(GTK_MENU_SHELL(submenu_judoka_show), item_judoka_u12);
    gtk_menu_shell_append(GTK_MENU_SHELL(submenu_judoka_show), item_judoka_u15);
    gtk_menu_shell_append(GTK_MENU_SHELL(submenu_judoka_show), item_judoka_u18);
    gtk_menu_shell_append(GTK_MENU_SHELL(submenu_judoka_show), item_judoka_u21);
    gtk_menu_shell_append(GTK_MENU_SHELL(submenu_judoka_show), item_judoka_active);
    gtk_menu_shell_append(GTK_MENU_SHELL(submenu_judoka_show), item_judoka_all);
    gtk_menu_shell_append(GTK_MENU_SHELL(menu_judoka), item_judoka_show);
    gtk_menu_shell_append(GTK_MENU_SHELL(menu_judoka), item_judoka_new);
    gtk_menu_shell_append(GTK_MENU_SHELL(menu_judoka), item_judoka_update);
    gtk_menu_shell_append(GTK_MENU_SHELL(menu_judoka), item_judoka_delete);
    gtk_menu_shell_append(GTK_MENU_SHELL(menu_judoka), item_judoka_trainings);
    gtk_menu_shell_append(GTK_MENU_SHELL(menu_judoka), item_judoka_search);
    
    gtk_menu_shell_append(GTK_MENU_SHELL(menu_training), item_training_show);
    gtk_menu_shell_append(GTK_MENU_SHELL(menu_training), item_training_new);
    gtk_menu_shell_append(GTK_MENU_SHELL(menu_training), item_training_new_exist);
    gtk_menu_shell_append(GTK_MENU_SHELL(menu_training), item_training_update);
    gtk_menu_shell_append(GTK_MENU_SHELL(menu_training), item_training_pupils);
    
    gtk_menu_shell_append(GTK_MENU_SHELL(menu_exam), item_exam_show);
    gtk_menu_shell_append(GTK_MENU_SHELL(menu_exam), item_exam_new);
    gtk_menu_shell_append(GTK_MENU_SHELL(menu_exam), item_exam_update);
    gtk_menu_shell_append(GTK_MENU_SHELL(menu_exam), item_exam_date);
    
    gtk_menu_shell_append(GTK_MENU_SHELL(menu_competition), item_competition_show);
    gtk_menu_shell_append(GTK_MENU_SHELL(menu_competition), item_competition_new);;
    gtk_menu_shell_append(GTK_MENU_SHELL(menu_competition), item_competition_update);
    gtk_menu_shell_append(GTK_MENU_SHELL(menu_competition), item_competition_date);
    
    gtk_menu_shell_append(GTK_MENU_SHELL(menu_print), item_print_trainings);
    gtk_menu_shell_append(GTK_MENU_SHELL(menu_print), item_print_competitors);
    gtk_menu_shell_append(GTK_MENU_SHELL(menu_print), item_print_salary);
    
    gtk_container_add(GTK_CONTAINER(scrollwindow), cb_struct.output);
    gtk_box_pack_start(GTK_BOX(vbox), menubar, FALSE, FALSE, 1);
    gtk_box_pack_start(GTK_BOX(vbox), scrollwindow, TRUE, TRUE, 1);
    
    g_signal_connect(G_OBJECT(window), "destroy",
                     G_CALLBACK(gtk_main_quit), NULL);
    
    g_signal_connect(G_OBJECT(item_renshu_quit), "activate",
                     G_CALLBACK(gtk_main_quit), NULL);
    
    g_signal_connect(G_OBJECT(item_judoka_u10), "activate",
                     G_CALLBACK(show_judoka_age), &cb_struct);
    g_signal_connect(G_OBJECT(item_judoka_u12), "activate",
                     G_CALLBACK(show_judoka_age), &cb_struct);
    g_signal_connect(G_OBJECT(item_judoka_u15), "activate",
                     G_CALLBACK(show_judoka_age), &cb_struct);
    g_signal_connect(G_OBJECT(item_judoka_u18), "activate",
                     G_CALLBACK(show_judoka_age), &cb_struct);
    g_signal_connect(G_OBJECT(item_judoka_u21), "activate",
                     G_CALLBACK(show_judoka_age), &cb_struct);
    g_signal_connect(G_OBJECT(item_judoka_all), "activate",
                     G_CALLBACK(show_judoka_age), &cb_struct);
    g_signal_connect(G_OBJECT(item_judoka_active), "activate",
                     G_CALLBACK(show_judoka_age), &cb_struct);
    g_signal_connect(G_OBJECT(item_judoka_trainings), "activate",
                     G_CALLBACK(show_trainings_judoka), &cb_struct);
    g_signal_connect(G_OBJECT(item_training_show), "activate",
                     G_CALLBACK(show_trainings_all), &cb_struct); 
    g_signal_connect(G_OBJECT(item_exam_show), "activate",
                     G_CALLBACK(show_exams_all), &cb_struct);
    g_signal_connect(G_OBJECT(item_exam_date), "activate",
                     G_CALLBACK(show_exams_judoka), &cb_struct);
    g_signal_connect(G_OBJECT(item_competition_show), "activate",
                     G_CALLBACK(show_competitions_all), &cb_struct);
    g_signal_connect(G_OBJECT(item_competition_date), "activate",
                     G_CALLBACK(show_competitions_competitor), &cb_struct);
    g_signal_connect(G_OBJECT(item_renshu_about), "activate",
                     G_CALLBACK(about_dialog), &cb_struct);
    g_signal_connect(G_OBJECT(item_judoka_new), "activate",
                     G_CALLBACK(new_judoka_dialog), &cb_struct);
    g_signal_connect(G_OBJECT(item_training_new), "activate",
                     G_CALLBACK(new_training_dialog), &cb_struct);
    g_signal_connect(G_OBJECT(item_training_update), "activate",
                     G_CALLBACK(update_training_dialog), &cb_struct);
    g_signal_connect(G_OBJECT(item_training_new_exist), "activate",
                     G_CALLBACK(new_training_exist_dialog), &cb_struct);
    g_signal_connect(G_OBJECT(item_exam_new), "activate",
                     G_CALLBACK(new_exam_dialog), &cb_struct);
    g_signal_connect(G_OBJECT(item_exam_update), "activate",
                     G_CALLBACK(update_exam_dialog), &cb_struct);
    g_signal_connect(G_OBJECT(item_competition_new), "activate",
                     G_CALLBACK(new_comp_dialog), &cb_struct);
    g_signal_connect(G_OBJECT(item_competition_update), "activate",
                     G_CALLBACK(update_competition_dialog), &cb_struct);
    g_signal_connect(G_OBJECT(item_renshu_config), "activate",
                     G_CALLBACK(update_settings_dialog), &cb_struct);
    g_signal_connect(G_OBJECT(item_judoka_update), "activate",
                     G_CALLBACK(update_judoka_dialog), &cb_struct);
    g_signal_connect(G_OBJECT(item_renshu_load), "activate",
                     G_CALLBACK(file_chooser_dialog), &cb_struct);
    g_signal_connect(G_OBJECT(item_renshu_remote), "activate",
                     G_CALLBACK(remote_db_dialog), &cb_struct);
    g_signal_connect(G_OBJECT(item_renshu_new_config), "activate",
                     G_CALLBACK(add_config_dialog), &cb_struct);
    g_signal_connect(G_OBJECT(item_print_salary), "activate",
                     G_CALLBACK(salary_chooser_dialog), &cb_struct);
    g_signal_connect(G_OBJECT(item_renshu_update_config), "activate",
                     G_CALLBACK(update_config_dialog), &cb_struct);
    g_signal_connect(G_OBJECT(item_print_trainings), "activate",
                     G_CALLBACK(list_chooser_dialog), &cb_struct);
    g_signal_connect(G_OBJECT(item_print_competitors), "activate",
                     G_CALLBACK(competition_chooser_dialog), &cb_struct);
    g_signal_connect(G_OBJECT(item_judoka_delete), "activate",
                     G_CALLBACK(confirm_dialog), &cb_struct);
     g_signal_connect(G_OBJECT(item_judoka_search), "activate",
                     G_CALLBACK(search_judoka_dialog), &cb_struct);
    g_signal_connect(G_OBJECT(item_training_pupils), "activate",
                     G_CALLBACK(show_judoka_training), &cb_struct);
    gtk_widget_show_all(window);
    
    gtk_main();
    free(cb_struct.dbname);
    return 0;
}