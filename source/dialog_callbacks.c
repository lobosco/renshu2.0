#define _GNU_SOURCE
#include <gtk/gtk.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "common.h"
#include "write_db_callbacks.h"
#include "print_callbacks.h"
#include "read_db_callbacks.h"


#ifdef WIN32
#if 0
#include <windef.h>
#include <ansidecl.h>
#include <winbase.h>
#include <winnt.h>
#endif
#include "sqlite3.h"
#else
#include <sqlite3.h>
#endif

int single_entry_callback(void* param, int argc, char**argv, char**column){
    gchar* result = (gchar*) param;
    sprintf(result, "%s" ,argv[0]);
    return 0;
};


int slot_changed_callback(GtkWidget *widget, gpointer data){
    GtkComboBox* caller = GTK_COMBO_BOX(widget);
    renshu_cb_provider* incoming = (renshu_cb_provider*) data;
    
    GtkTreeStore* store = gtk_tree_store_new(N_COLUMNS, G_TYPE_INT, G_TYPE_STRING, G_TYPE_STRING, G_TYPE_STRING, G_TYPE_INT, G_TYPE_STRING, G_TYPE_STRING, G_TYPE_STRING, G_TYPE_STRING);
    GtkWidget* labeltowrite = GTK_WIDGET(incoming->output);
    /*remove any already set columns/ stores*/
    gtk_tree_view_set_model(GTK_TREE_VIEW(labeltowrite), NULL);
    GList* columns = gtk_tree_view_get_columns(GTK_TREE_VIEW(labeltowrite));
    for(columns = g_list_first(columns); columns != NULL; columns = columns->next){
        gtk_tree_view_remove_column(GTK_TREE_VIEW(labeltowrite), GTK_TREE_VIEW_COLUMN(columns->data));   
    }
    //list no longer needed
    g_list_free(columns);
    GtkCellRenderer *renderer = gtk_cell_renderer_text_new();
    GtkTreeViewColumn* column; 
    column = gtk_tree_view_column_new_with_attributes("ID", renderer, "text", ID_COLUMN, NULL);
    gtk_tree_view_column_set_sort_column_id(column,ID_COLUMN);
    gtk_tree_view_append_column(GTK_TREE_VIEW(labeltowrite), column);
    column = gtk_tree_view_column_new_with_attributes("Name", renderer, "text", NAME_COLUMN, NULL);
    gtk_tree_view_column_set_sort_column_id(column,NAME_COLUMN);
    gtk_tree_view_append_column(GTK_TREE_VIEW(labeltowrite), column);
    column = gtk_tree_view_column_new_with_attributes("Vorname", renderer, "text", PRENAME_COLUMN, NULL);
    gtk_tree_view_column_set_sort_column_id(column,PRENAME_COLUMN);
    gtk_tree_view_append_column(GTK_TREE_VIEW(labeltowrite), column);
    column = gtk_tree_view_column_new_with_attributes("Geburtsdatum", renderer, "text", YOB_COLUMN, NULL);
    gtk_tree_view_column_set_sort_column_id(column,YOB_COLUMN);
    gtk_tree_view_append_column(GTK_TREE_VIEW(labeltowrite), column);
    column = gtk_tree_view_column_new_with_attributes("Graduierung", renderer, "text", GRADUATION_COLUMN, NULL);
    gtk_tree_view_column_set_sort_column_id(column,GRADUATION_COLUMN);
    gtk_tree_view_append_column(GTK_TREE_VIEW(labeltowrite), column);
    column = gtk_tree_view_column_new_with_attributes("Lizenz", renderer, "text", LICENSE_COLUMN, NULL);
    gtk_tree_view_column_set_sort_column_id(column,LICENSE_COLUMN);
    gtk_tree_view_append_column(GTK_TREE_VIEW(labeltowrite), column);
    column = gtk_tree_view_column_new_with_attributes("Kontakt", renderer, "text", CONTACT_COLUMN, NULL);
    gtk_tree_view_column_set_sort_column_id(column,CONTACT_COLUMN);
    gtk_tree_view_append_column(GTK_TREE_VIEW(labeltowrite), column);
    column = gtk_tree_view_column_new_with_attributes("Passnummer", renderer, "text", PASSPORT_COLUMN, NULL);
    gtk_tree_view_column_set_sort_column_id(column,PASSPORT_COLUMN);
    gtk_tree_view_append_column(GTK_TREE_VIEW(labeltowrite), column);
    column = gtk_tree_view_column_new_with_attributes("Bemerkungen", renderer, "text", NOTES_COLUMN, NULL);
    gtk_tree_view_column_set_sort_column_id(column,NOTES_COLUMN);
    gtk_tree_view_append_column(GTK_TREE_VIEW(labeltowrite), column);
    
    
    //deassemble caller input
    gchar* text = gtk_combo_box_text_get_active_text(GTK_COMBO_BOX_TEXT(caller));
    //deassemble training slot:
    char* training_text = strtok(text, " \n");
    char* training_weekday;
    char* training_nr;
    int k = 0;
    while(training_text != NULL){
        if(k%2==0){
            asprintf(&training_weekday, "%s", training_text);
        }else{
            asprintf(&training_nr, "%s", training_text);
        }
        training_text = strtok(NULL, " \n");
        k++;
    }

    char* query;
    if(incoming->usemysql){
        asprintf(&query, "SELECT judoka.pupils_id,judoka.name, judoka.prename, judoka.date_of_birth,judoka.graduation,judoka.license,judoka.contact,judoka.passport,judoka.notes from judoka inner join training on training.pupil=judoka.pupils_id where judoka.display='1' and training.weekday='%s' and training.training_nr='%s' and training.date> (SELECT DATE_SUB(CURDATE(), INTERVAL 12 MONTH)) group by judoka.pupils_id;", training_weekday, training_nr);
    }else{
        asprintf(&query, "SELECT judoka.pupils_id,judoka.name, judoka.prename, judoka.date_of_birth,judoka.graduation,judoka.license,judoka.contact,judoka.passport,judoka.notes from judoka inner join training on training.pupil=judoka.pupils_id where judoka.display='1' and training.weekday='%s' and training.training_nr='%s' and training.date>(select strftime('%%Y-%%m-%%d', 'now', '-1 years')) group by judoka.pupils_id;", training_weekday, training_nr);
    }
    read_db(store, incoming,query,0);
    gtk_tree_view_set_model(GTK_TREE_VIEW(labeltowrite), GTK_TREE_MODEL(store));
    free(query);
    free(text);
    free(training_text);
    free(training_weekday);
    free(training_nr);
    g_object_unref(store); /* destroy model automatically with view */
    return 0;
}

void error_dialog(gchar* message){
    GtkDialogFlags flags = GTK_DIALOG_MODAL;
    GtkWidget* dialog = gtk_message_dialog_new (NULL,flags,GTK_MESSAGE_ERROR,GTK_BUTTONS_OK, "%s" ,message);
    gtk_dialog_run (GTK_DIALOG (dialog));
    gtk_widget_destroy (dialog);
};

void get_single_entry(gchar* result ,const char* query, renshu_cb_provider* dbprovider){
    const char* dbname = dbprovider->dbname;
    int usemysql = dbprovider->usemysql;
    gchar* error = "";
    char* err_msg = 0;
    
    if(!usemysql){
        sqlite3* db;
        int rc = sqlite3_open(dbname, &db);
        if(rc != SQLITE_OK){
            fprintf(stderr, "Cannot open database: %s\n", sqlite3_errmsg(db));
            asprintf(&error, "Cannot open database: %s\n", sqlite3_errmsg(db));
            error_dialog(error);
            g_free(error);
            goto end;
        }
        //execute query
        rc = sqlite3_exec(db, query, single_entry_callback, result, &err_msg);
        if(rc!=SQLITE_OK){
            fprintf(stderr, "Failed to select data\n");
            fprintf(stderr, "SQL error: %s\n", err_msg);
            asprintf(&error, "Failed to select data\n SQL error: %s\n", err_msg);
            error_dialog(error);
            g_free(error);
            sqlite3_free(err_msg);
            sqlite3_close(db);
        }
        end:
        sqlite3_close(db);
    }else if(usemysql){
//#ifndef WIN32        
        
        MYSQL* con = mysql_init(NULL);
        char** array_results = NULL;
        if (con == NULL){
            fprintf(stderr, "mysql_init() failed\n");
            error_dialog("mysql_init() failed\n");
            exit(1);
        }  
  
        if (mysql_real_connect(con, dbprovider->dbaddr, dbprovider->dbuser, dbprovider->dbpasswd, 
          dbprovider->dbname, 0, NULL, 0) == NULL) {
            finish_with_error(con);
        }
        if (mysql_query(con, "set names 'utf8';")){  
            finish_with_error(con);
        }
        if (mysql_query(con, "set character set 'utf8';")){  
            finish_with_error(con);
        }
        if (mysql_query(con, "set sql_mode='';")){  
            finish_with_error(con);
        }
        if (mysql_query(con, query)){  
            finish_with_error(con);
        }
        MYSQL_RES *result_mysql = mysql_store_result(con);
        if (result == NULL) {
            finish_with_error(con);
        }  
        int num_fields = mysql_num_fields(result_mysql);
        MYSQL_ROW row;
        while ((row = mysql_fetch_row(result_mysql))) { 
            for(int i = 0; i < num_fields; i++) {
                array_results = realloc(array_results, (i+1) * sizeof(char*));
                asprintf(&array_results[i], "%s", row[i] ? row[i] : "NULL");
            } 
            single_entry_callback(result,num_fields, array_results,NULL);
        }
        mysql_free_result(result_mysql);
        mysql_close(con);
//#endif
        
    };
};    

int settings_changed_callback(GtkWidget *widget, gpointer data){
    GtkComboBox* caller = GTK_COMBO_BOX(widget);
    renshu_cb_provider* incoming = (renshu_cb_provider*) data;
     //deassemble caller input
    gchar* text = gtk_combo_box_text_get_active_text(GTK_COMBO_BOX_TEXT(caller));
    //deassemble training slot:
    char* trainer_text = strtok(text, " \n");
    char* trainer_name;
    char* trainer_prename;
    int k = 0;
    while(trainer_text != NULL){
        if(k%2==0){
            asprintf(&trainer_prename, "%s", trainer_text);
        }else{
            asprintf(&trainer_name, "%s", trainer_text);
        }
        trainer_text = strtok(NULL, " \n");
        k++;
    }
    
    //get config file and deassemble into array
    
    char texts[7][200];
    
    char* configquery;
    asprintf(&configquery,"select configpath from settings where name='%s' and prename='%s'",trainer_name, trainer_prename);
    char* headerquery;
    asprintf(&headerquery,"select headerpath from settings where name='%s' and prename='%s'",trainer_name, trainer_prename);
    
    get_single_entry(texts[5],configquery,incoming);
    get_single_entry(texts[6],headerquery,incoming);
    
    read_config_file(texts[5],texts);
    
    //now write to widgets: 
    // get parent of caller
    GtkWidget* contentbox = gtk_widget_get_ancestor(GTK_WIDGET(caller), GTK_TYPE_BOX);
    GList* children = gtk_container_get_children(GTK_CONTAINER(contentbox));
    int i = 0;
    int j = 0;
    for(children = g_list_first(children); children != NULL; children = children->next){
        if(GTK_IS_ENTRY(children->data)){
            if(i == 0){
                gtk_entry_set_text(GTK_ENTRY(children->data),trainer_name);
            }
            if(i == 1){
                gtk_entry_set_text(GTK_ENTRY(children->data),trainer_prename);
            }
            if(i>1){
                gtk_entry_set_text(GTK_ENTRY(children->data),texts[i-2]);
            }
            i++;
        }else if(GTK_IS_FILE_CHOOSER(children->data)){
            if(j == 0){ //configchooser
                gtk_file_chooser_set_filename(GTK_FILE_CHOOSER(children->data),texts[5]);
            }
            if(j == 1){ //configchooser
                gtk_file_chooser_set_filename(GTK_FILE_CHOOSER(children->data),texts[6]);
            }
            j++;
        }
    }
   return 0;
};



void confirm_dialog(GtkWidget* widget, gpointer data){
    GtkDialogFlags flags = GTK_DIALOG_DESTROY_WITH_PARENT;
    GtkWidget* dialog = gtk_message_dialog_new (NULL,flags,GTK_MESSAGE_QUESTION,GTK_BUTTONS_NONE,"Sicher, dass Sie den Eintrag entfernen wollen? Die Daten gehen unwiderbringlich verloren!");
    gtk_window_set_modal(GTK_WINDOW(dialog), TRUE);
    gtk_dialog_add_buttons(GTK_DIALOG(dialog),"Abbruch", GTK_RESPONSE_CANCEL, "OK", GTK_RESPONSE_OK, NULL);
    gint res = gtk_dialog_run(GTK_DIALOG (dialog));
    if(res==GTK_RESPONSE_OK){
        delete_judoka(widget, data);
    }
    gtk_widget_destroy (dialog);
};
/*
 * Function: new_file_dialog
 * ----------------------------
 *   provides a dialog to update the configuration of the current database
 *
 *   widget: the button that has called the callback, 
 *    
 *   data: pointer to the file_chooser_button widget that was previously meant to store the file
 *
 */
void new_file_dialog(GtkWidget* widget, gpointer data){
    printf("enter new file\n");
    GtkWidget* output = GTK_WIDGET(data);
    GtkFileFilter* filter_ren = gtk_file_filter_new();
    gtk_file_filter_add_pattern(filter_ren, "*.ren");
    GtkWidget* dialog = gtk_file_chooser_dialog_new("Speichere Datei", NULL, GTK_FILE_CHOOSER_ACTION_SAVE, "Abbruch", GTK_RESPONSE_CANCEL, "OK", GTK_RESPONSE_OK, NULL);
    gtk_window_set_modal(GTK_WINDOW(dialog), TRUE);
    gtk_file_chooser_add_filter(GTK_FILE_CHOOSER(dialog), filter_ren);
    gtk_file_chooser_set_do_overwrite_confirmation(GTK_FILE_CHOOSER(dialog), TRUE);
    gtk_file_chooser_set_current_name (GTK_FILE_CHOOSER(dialog), "untitled.ren");
    gint res = gtk_dialog_run(GTK_DIALOG(dialog));
    if(res == GTK_RESPONSE_OK){
        char* filename = gtk_file_chooser_get_filename(GTK_FILE_CHOOSER(dialog));
        gtk_button_set_label(GTK_BUTTON(output), filename);
    }
    gtk_widget_destroy(dialog);
};
/*
 * Function: new_training_dialog
 * ----------------------------
 *   provides a dialog to put in datas of new training
 *
 *   widget: the button that has called the callback, 
 *    
 *   data: renshu_cb_provider, provides database and output window
 *
 */
void new_training_dialog(GtkWidget *widget, gpointer data){
    GtkWidget* dialog = gtk_dialog_new_with_buttons("Neues Training", NULL, GTK_DIALOG_MODAL,"Abbruch", GTK_RESPONSE_CANCEL, "OK", GTK_RESPONSE_OK, NULL);
    renshu_cb_provider* incoming = (renshu_cb_provider*)data;
     
    // create entry widgets
    GtkWidget* entry_date = gtk_entry_new();
    
    GtkWidget* label = gtk_label_new("Hier bitte neues Training eintragen:\n");
    GtkWidget* label_date = gtk_label_new("Datum:");
    GtkWidget* label_slot = gtk_label_new("Slot:");
    GtkWidget* button_slot = gtk_combo_box_text_new();
    read_db(GTK_TREE_STORE(button_slot), incoming, "select day, nr from slots;", 5);
    GtkWidget* label_trainer = gtk_label_new("Trainer:");
    GtkWidget* button_trainer0 = gtk_combo_box_text_new();
    read_db(GTK_TREE_STORE(button_trainer0), incoming, "select prename, name from judoka where license > 1 or pupils_id=0;", 5);
    GtkWidget* button_trainer1 = gtk_combo_box_text_new();
    read_db(GTK_TREE_STORE(button_trainer1), incoming, "select prename, name from judoka where license > 1 or pupils_id=0;", 5);
    
    GtkWidget* label_pupils = gtk_label_new("Schüler:");
    
    GtkWidget* treeview_pupils = gtk_tree_view_new();
    gtk_tree_view_set_reorderable(GTK_TREE_VIEW(treeview_pupils), FALSE);
    GtkTreeSelection* selection = gtk_tree_view_get_selection(GTK_TREE_VIEW(treeview_pupils));
    gtk_tree_selection_set_mode(selection, GTK_SELECTION_MULTIPLE);
    GtkWidget* scrollpupils = gtk_scrolled_window_new(NULL, NULL);
    gtk_scrolled_window_set_min_content_height(GTK_SCROLLED_WINDOW(scrollpupils),250);
    
    gtk_container_add(GTK_CONTAINER(scrollpupils), treeview_pupils);
    
    //build the treeview
    GtkTreeStore* store = gtk_tree_store_new(N_COLUMNS, G_TYPE_INT, G_TYPE_STRING, G_TYPE_STRING, G_TYPE_STRING, G_TYPE_INT, G_TYPE_STRING, G_TYPE_STRING, G_TYPE_STRING, G_TYPE_STRING);
    /*remove any already set columns/ stores*/
    gtk_tree_view_set_model(GTK_TREE_VIEW(treeview_pupils), NULL);
    GList* columns = gtk_tree_view_get_columns(GTK_TREE_VIEW(treeview_pupils));
    for(columns = g_list_first(columns); columns != NULL; columns = columns->next){
        gtk_tree_view_remove_column(GTK_TREE_VIEW(treeview_pupils), GTK_TREE_VIEW_COLUMN(columns->data));   
    }
    //list no longer needed
    g_list_free(columns);
    GtkCellRenderer *renderer = gtk_cell_renderer_text_new();
    GtkTreeViewColumn* column; 
    column = gtk_tree_view_column_new_with_attributes("ID", renderer, "text", ID_COLUMN, NULL);
    gtk_tree_view_column_set_sort_column_id(column,ID_COLUMN);
    gtk_tree_view_append_column(GTK_TREE_VIEW(treeview_pupils), column);
    column = gtk_tree_view_column_new_with_attributes("Name", renderer, "text", NAME_COLUMN, NULL);
    gtk_tree_view_column_set_sort_column_id(column,NAME_COLUMN);
    gtk_tree_view_append_column(GTK_TREE_VIEW(treeview_pupils), column);
    column = gtk_tree_view_column_new_with_attributes("Vorname", renderer, "text", PRENAME_COLUMN, NULL);
    gtk_tree_view_column_set_sort_column_id(column,PRENAME_COLUMN);
    gtk_tree_view_append_column(GTK_TREE_VIEW(treeview_pupils), column);
    column = gtk_tree_view_column_new_with_attributes("Geburtsdatum", renderer, "text", YOB_COLUMN, NULL);
    gtk_tree_view_column_set_sort_column_id(column,YOB_COLUMN);
    gtk_tree_view_append_column(GTK_TREE_VIEW(treeview_pupils), column);
    column = gtk_tree_view_column_new_with_attributes("Graduierung", renderer, "text", GRADUATION_COLUMN, NULL);
    gtk_tree_view_column_set_sort_column_id(column,GRADUATION_COLUMN);
    gtk_tree_view_append_column(GTK_TREE_VIEW(treeview_pupils), column);
    column = gtk_tree_view_column_new_with_attributes("Lizenz", renderer, "text", LICENSE_COLUMN, NULL);
    gtk_tree_view_column_set_sort_column_id(column,LICENSE_COLUMN);
    gtk_tree_view_append_column(GTK_TREE_VIEW(treeview_pupils), column);
    column = gtk_tree_view_column_new_with_attributes("Kontakt", renderer, "text", CONTACT_COLUMN, NULL);
    gtk_tree_view_column_set_sort_column_id(column,CONTACT_COLUMN);
    gtk_tree_view_append_column(GTK_TREE_VIEW(treeview_pupils), column);
    column = gtk_tree_view_column_new_with_attributes("Passnummer", renderer, "text", PASSPORT_COLUMN, NULL);
    gtk_tree_view_column_set_sort_column_id(column,PASSPORT_COLUMN);
    gtk_tree_view_append_column(GTK_TREE_VIEW(treeview_pupils), column);
    column = gtk_tree_view_column_new_with_attributes("Bemerkungen", renderer, "text", NOTES_COLUMN, NULL);
    gtk_tree_view_column_set_sort_column_id(column,NOTES_COLUMN);
    gtk_tree_view_append_column(GTK_TREE_VIEW(treeview_pupils), column);
    
    
    char* query;
    asprintf(&query, "SELECT judoka.pupils_id,judoka.name, judoka.prename, judoka.date_of_birth,judoka.graduation,judoka.license,judoka.contact,judoka.passport,judoka.notes from judoka where judoka.display='1';");
    read_db(store, incoming,query,0);
    gtk_tree_view_set_model(GTK_TREE_VIEW(treeview_pupils), GTK_TREE_MODEL(store));
    free(query);
    g_object_unref(store); /* destroy model automatically with view */
    
    gtk_entry_set_placeholder_text(GTK_ENTRY(entry_date),"yyyy-mm-dd");
    
    
    //assemble widgets
    GtkWidget* contentbox = gtk_dialog_get_content_area(GTK_DIALOG(dialog));
    gtk_box_pack_start(GTK_BOX(contentbox), label, TRUE, TRUE, 0);
    gtk_box_pack_start(GTK_BOX(contentbox), label_date, TRUE, TRUE, 0);
    gtk_box_pack_start(GTK_BOX(contentbox), entry_date, TRUE, TRUE, 0);
    gtk_box_pack_start(GTK_BOX(contentbox), label_trainer, TRUE, TRUE, 0);
    gtk_box_pack_start(GTK_BOX(contentbox), button_trainer0, TRUE, TRUE, 0);
    gtk_box_pack_start(GTK_BOX(contentbox), button_trainer1, TRUE, TRUE, 0);
    gtk_box_pack_start(GTK_BOX(contentbox), label_slot, TRUE, TRUE, 0);
    gtk_box_pack_start(GTK_BOX(contentbox), button_slot, TRUE, TRUE, 0);
    gtk_box_pack_start(GTK_BOX(contentbox), label_pupils, TRUE, TRUE, 0);
    gtk_box_pack_start(GTK_BOX(contentbox), scrollpupils, TRUE, TRUE, 0);
    
    gtk_widget_show_all(dialog); //important: needed for reasons
    gint result = gtk_dialog_run(GTK_DIALOG(dialog));
    
    switch (result){
        case GTK_RESPONSE_OK:
            write_training_exist(dialog, data); //DONE: changed training code
            break;
        default:
            break;
    }
    gtk_widget_destroy(dialog); 
};


/*
 * Function: new_training_exist_dialog
 * ----------------------------
 *   provides a dialog to enter month, year and qualification for current salary print
 *
 *   widget: the button that has called the callback, 
 *    
 *   data: renshu_cb_provider, provides database and output window
 *
 */
void new_training_exist_dialog(GtkWidget* widget, gpointer data){
    renshu_cb_provider* incoming = (renshu_cb_provider*)data;
    renshu_cb_provider local;
    local = *incoming;
    GtkWidget* dialog = gtk_dialog_new_with_buttons("Neues Training", NULL, GTK_DIALOG_MODAL,"Abbruch", GTK_RESPONSE_CANCEL, "OK", GTK_RESPONSE_OK, NULL);
    // create entry widgets
    GtkWidget* button_slot = gtk_combo_box_text_new();
    read_db(GTK_TREE_STORE(button_slot), incoming, "select day, nr from slots;", 5);
    
    GtkWidget* label = gtk_label_new("Hier bitte neues Training eintragen:\n");
    GtkWidget* label_date = gtk_label_new("Datum:");
    GtkWidget* label_trainer = gtk_label_new("Trainer:");
    GtkWidget* label_pupils = gtk_label_new("Schüler:");
    GtkWidget* label_slot = gtk_label_new("Slot auswählen:");
    GtkWidget* entry_date = gtk_entry_new();
    GtkWidget* button_trainer0 = gtk_combo_box_text_new();
    read_db(GTK_TREE_STORE(button_trainer0), incoming, "select prename, name from judoka where license > 1 or pupils_id=0;", 5);
    GtkWidget* button_trainer1 = gtk_combo_box_text_new();
    read_db(GTK_TREE_STORE(button_trainer1), incoming, "select prename, name from judoka where license > 1 or pupils_id=0;", 5);
    
    GtkWidget* treeview_pupils = gtk_tree_view_new();
    gtk_tree_view_set_reorderable(GTK_TREE_VIEW(treeview_pupils), FALSE);
    GtkTreeSelection* selection = gtk_tree_view_get_selection(GTK_TREE_VIEW(treeview_pupils));
    gtk_tree_selection_set_mode(selection, GTK_SELECTION_MULTIPLE);
    GtkWidget* scrollpupils = gtk_scrolled_window_new(NULL, NULL);
    gtk_scrolled_window_set_min_content_height(GTK_SCROLLED_WINDOW(scrollpupils),250);
    
    gtk_container_add(GTK_CONTAINER(scrollpupils), treeview_pupils);
    
    local.output = treeview_pupils;
    
    g_signal_connect( G_OBJECT( button_slot ), "changed",
                      G_CALLBACK(slot_changed_callback ), &local);
    
    //assemble widgets
    GtkWidget* contentbox = gtk_dialog_get_content_area(GTK_DIALOG(dialog));
    gtk_box_pack_start(GTK_BOX(contentbox), label, TRUE, TRUE, 0);
    gtk_box_pack_start(GTK_BOX(contentbox), label_date, TRUE, TRUE, 0);
    gtk_box_pack_start(GTK_BOX(contentbox), entry_date, TRUE, TRUE, 0);
    gtk_box_pack_start(GTK_BOX(contentbox), label_trainer, TRUE, TRUE, 0);
    gtk_box_pack_start(GTK_BOX(contentbox), button_trainer0, TRUE, TRUE, 0);
    gtk_box_pack_start(GTK_BOX(contentbox), button_trainer1, TRUE, TRUE, 0);
    gtk_box_pack_start(GTK_BOX(contentbox), label_slot, TRUE, TRUE, 0);
    gtk_box_pack_start(GTK_BOX(contentbox), button_slot, TRUE, TRUE, 0);
    gtk_box_pack_start(GTK_BOX(contentbox), label_pupils, TRUE, TRUE, 0);
    gtk_box_pack_start(GTK_BOX(contentbox), scrollpupils, TRUE, TRUE, 0);
    
    
    gtk_widget_show_all(dialog); //important: needed for reasons
    gint result = gtk_dialog_run(GTK_DIALOG(dialog));
    
    switch (result){
        case GTK_RESPONSE_OK:
            write_training_exist(dialog, data);
            break;
        default:
            break;
    }
    gtk_widget_destroy(dialog);
    
};

/*
 * Function: new_judoka_dialog
 * ----------------------------
 *   provides a dialog to put in datas of new judoka
 *
 *   widget: the button that has called the callback, 
 *    
 *   data: renshu_cb_provider, provides database and output window
 *
 */
void new_judoka_dialog(GtkWidget *widget, gpointer data){
    renshu_cb_provider* incoming = (renshu_cb_provider*)data;
    GtkWidget* dialog = gtk_dialog_new_with_buttons("Neuer Judoka", NULL, GTK_DIALOG_MODAL,"Abbruch", GTK_RESPONSE_CANCEL, "OK", GTK_RESPONSE_OK, NULL);
    // create entry widgets
    GtkWidget* entry_name = gtk_entry_new();
    GtkWidget* entry_prename = gtk_entry_new();
    GtkWidget* entry_yob = gtk_entry_new();
    GtkWidget* entry_graduation = gtk_entry_new();
    GtkWidget* entry_contact = gtk_entry_new();
    GtkWidget* entry_passport = gtk_entry_new();
    GtkWidget* entry_notes = gtk_entry_new();
    
    
    GtkWidget* label = gtk_label_new("Hier bitte neuen Judoka eintragen:\n");
    GtkWidget* label_name = gtk_label_new("Name:");
    GtkWidget* label_prename = gtk_label_new("Vorname:");
    GtkWidget* label_yob = gtk_label_new("Geburtsdatum:");
    GtkWidget* label_graduation = gtk_label_new("Graduierung:");
    GtkWidget* label_license = gtk_label_new("Lizenz:");
    GtkWidget* label_contact = gtk_label_new("Kontakt:");
    GtkWidget* label_passport = gtk_label_new("Passnummer:");
    GtkWidget* label_notes = gtk_label_new("Bemerkungen:");
    
    gtk_entry_set_placeholder_text(GTK_ENTRY(entry_name),"Name");
    gtk_entry_set_placeholder_text(GTK_ENTRY(entry_prename),"Vorname");
    gtk_entry_set_placeholder_text(GTK_ENTRY(entry_yob),"yyyy-mm-dd");
    gtk_entry_set_placeholder_text(GTK_ENTRY(entry_graduation),"Graduierung");
    gtk_entry_set_placeholder_text(GTK_ENTRY(entry_contact),"Kontakt");
    gtk_entry_set_placeholder_text(GTK_ENTRY(entry_passport),"Passnumer");
    gtk_entry_set_placeholder_text(GTK_ENTRY(entry_notes),"Sonstiges");
    
    GtkWidget* button_license = gtk_combo_box_text_new();
    read_db(GTK_TREE_STORE(button_license), incoming, "select rateid,identifier from rates;", 5);
    
    
    //assemble widgets
    GtkWidget* contentbox = gtk_dialog_get_content_area(GTK_DIALOG(dialog));
    gtk_box_pack_start(GTK_BOX(contentbox), label, TRUE, TRUE, 0);
    gtk_box_pack_start(GTK_BOX(contentbox), label_name, TRUE, TRUE, 0);
    gtk_box_pack_start(GTK_BOX(contentbox), entry_name, TRUE, TRUE, 0);
    gtk_box_pack_start(GTK_BOX(contentbox), label_prename, TRUE, TRUE, 0);
    gtk_box_pack_start(GTK_BOX(contentbox), entry_prename, TRUE, TRUE, 0);
    gtk_box_pack_start(GTK_BOX(contentbox), label_yob, TRUE, TRUE, 0);
    gtk_box_pack_start(GTK_BOX(contentbox), entry_yob, TRUE, TRUE, 0);
    gtk_box_pack_start(GTK_BOX(contentbox), label_graduation, TRUE, TRUE, 0);
    gtk_box_pack_start(GTK_BOX(contentbox), entry_graduation, TRUE, TRUE, 0);
    gtk_box_pack_start(GTK_BOX(contentbox), label_license, TRUE, TRUE, 0);
    gtk_box_pack_start(GTK_BOX(contentbox), button_license, TRUE, TRUE, 0);
    gtk_box_pack_start(GTK_BOX(contentbox), label_contact, TRUE, TRUE, 0);
    gtk_box_pack_start(GTK_BOX(contentbox), entry_contact, TRUE, TRUE, 0);
    gtk_box_pack_start(GTK_BOX(contentbox), label_passport, TRUE, TRUE, 0);
    gtk_box_pack_start(GTK_BOX(contentbox), entry_passport, TRUE, TRUE, 0);
    gtk_box_pack_start(GTK_BOX(contentbox), label_notes, TRUE, TRUE, 0);
    gtk_box_pack_start(GTK_BOX(contentbox), entry_notes, TRUE, TRUE, 0);
    
    gtk_widget_show_all(dialog); //important: needed for reasons
    gint result = gtk_dialog_run(GTK_DIALOG(dialog));
    
    switch (result){
        case GTK_RESPONSE_OK:
            write_judoka(dialog, data);
            break;
        default:
            break;
    }
    gtk_widget_destroy(dialog);
};

/*
 * Function: new_exam_dialog
 * ----------------------------
 *   provides a dialog to put in datas of new exam
 *
 *   widget: the button that has called the callback, 
 *    
 *   data: renshu_cb_provider, provides database and output window
 *
 */
void new_exam_dialog(GtkWidget *widget, gpointer data){
    renshu_cb_provider* incoming = (renshu_cb_provider*)data;
    GtkWidget* dialog = gtk_dialog_new_with_buttons("Neue Prüfung", NULL, GTK_DIALOG_MODAL,"Abbruch", GTK_RESPONSE_CANCEL, "OK", GTK_RESPONSE_OK, NULL);
    // create entry widgets
    GtkWidget* entry_date = gtk_entry_new();
    GtkWidget* entry_examined = gtk_text_view_new();
    
    GtkWidget* label = gtk_label_new("Hier bitte neue Prüfung eintragen:\n");
    GtkWidget* label_date = gtk_label_new("Datum:");
    GtkWidget* label_examiner = gtk_label_new("Prüfer:");
    GtkWidget* button_examiner0 = gtk_combo_box_text_new();
    read_db(GTK_TREE_STORE(button_examiner0), incoming, "select prename, name from judoka where graduation < 0 or pupils_id = 0;", 5);
    GtkWidget* button_examiner1 = gtk_combo_box_text_new();
    read_db(GTK_TREE_STORE(button_examiner1), incoming, "select prename, name from judoka where graduation < 0  or pupils_id = 0;", 5);
    GtkWidget* button_examiner2 = gtk_combo_box_text_new();
    read_db(GTK_TREE_STORE(button_examiner2), incoming, "select prename, name from judoka where graduation < 0  or pupils_id = 0;", 5);
    
    GtkWidget* label_examined = gtk_label_new("Prüflinge:");
    
    gtk_entry_set_placeholder_text(GTK_ENTRY(entry_date),"yyyy-mm-dd");
    
    //assemble widgets
    GtkWidget* contentbox = gtk_dialog_get_content_area(GTK_DIALOG(dialog));
    gtk_box_pack_start(GTK_BOX(contentbox), label, TRUE, TRUE, 0);
    gtk_box_pack_start(GTK_BOX(contentbox), label_date, TRUE, TRUE, 0);
    gtk_box_pack_start(GTK_BOX(contentbox), entry_date, TRUE, TRUE, 0);
    gtk_box_pack_start(GTK_BOX(contentbox), label_examiner, TRUE, TRUE, 0);
    gtk_box_pack_start(GTK_BOX(contentbox), button_examiner0, TRUE, TRUE, 0);
    gtk_box_pack_start(GTK_BOX(contentbox), button_examiner1, TRUE, TRUE, 0);
    gtk_box_pack_start(GTK_BOX(contentbox), button_examiner2, TRUE, TRUE, 0);
    gtk_box_pack_start(GTK_BOX(contentbox), label_examined, TRUE, TRUE, 0);
    gtk_box_pack_start(GTK_BOX(contentbox), entry_examined, TRUE, TRUE, 0);
    
    gtk_widget_show_all(dialog); //important: needed for reasons
    gint result = gtk_dialog_run(GTK_DIALOG(dialog));
    
    switch (result){
        case GTK_RESPONSE_OK:
            write_exam(dialog, data);
            break;
        default:
            break;
    }
    gtk_widget_destroy(dialog);
};

/*
 * Function: new_comp_dialog
 * ----------------------------
 *   provides a dialog to put in datas of new competition
 *
 *   widget: the button that has called the callback, 
 *    
 *   data: renshu_cb_provider, provides database and output window
 *
 */
void new_comp_dialog(GtkWidget *widget, gpointer data){
    GtkWidget* dialog = gtk_dialog_new_with_buttons("Neuer Wettkampf", NULL, GTK_DIALOG_MODAL,"Abbruch", GTK_RESPONSE_CANCEL, "OK", GTK_RESPONSE_OK, NULL);
    // create entry widgets
    GtkWidget* entry_date = gtk_entry_new();
    GtkWidget* entry_event = gtk_entry_new();
    GtkWidget* entry_location = gtk_entry_new();
    GtkWidget* entry_athletes = gtk_text_view_new();
    
    GtkWidget* label = gtk_label_new("Hier bitte neuen Wettkampf eintragen:\n");
    GtkWidget* label_date = gtk_label_new("Datum:");
    GtkWidget* label_event = gtk_label_new("Wettkampf:");
    GtkWidget* label_location = gtk_label_new("Ort:");
    GtkWidget* label_athletes = gtk_label_new("Platzierungen:");
    
    gtk_entry_set_placeholder_text(GTK_ENTRY(entry_date),"yyyy-mm-dd");
    gtk_entry_set_placeholder_text(GTK_ENTRY(entry_event),"Wettkampf");
    gtk_entry_set_placeholder_text(GTK_ENTRY(entry_location),"Ort");
    
    //assemble widgets
    GtkWidget* contentbox = gtk_dialog_get_content_area(GTK_DIALOG(dialog));
    gtk_box_pack_start(GTK_BOX(contentbox), label, TRUE, TRUE, 0);
    gtk_box_pack_start(GTK_BOX(contentbox), label_date, TRUE, TRUE, 0);
    gtk_box_pack_start(GTK_BOX(contentbox), entry_date, TRUE, TRUE, 0);
    gtk_box_pack_start(GTK_BOX(contentbox), label_event, TRUE, TRUE, 0);
    gtk_box_pack_start(GTK_BOX(contentbox), entry_event, TRUE, TRUE, 0);
    gtk_box_pack_start(GTK_BOX(contentbox), label_location, TRUE, TRUE, 0);
    gtk_box_pack_start(GTK_BOX(contentbox), entry_location, TRUE, TRUE, 0);
    gtk_box_pack_start(GTK_BOX(contentbox), label_athletes, TRUE, TRUE, 0);
    gtk_box_pack_start(GTK_BOX(contentbox), entry_athletes, TRUE, TRUE, 0);
    
    gtk_widget_show_all(dialog); //important: needed for reasons
    gint result = gtk_dialog_run(GTK_DIALOG(dialog));
    
    switch (result){
        case GTK_RESPONSE_OK:
            write_competition(dialog, data);
            break;
        default:
            break;
    }
    gtk_widget_destroy(dialog);
};

/*
 * Function: update_settings_dialog
 * ----------------------------
 *   provides a dialog to save new settings in database file
 *
 *   widget: the button that has called the callback, 
 *    
 *   data: renshu_cb_provider, provides database and output window
 *
 */
void update_settings_dialog(GtkWidget *widget, gpointer data){
    renshu_cb_provider* incoming = (renshu_cb_provider*) data;
    GtkWidget* dialog = gtk_dialog_new_with_buttons("Neue Datenbank", NULL, GTK_DIALOG_MODAL,"Abbruch", GTK_RESPONSE_CANCEL, "OK", GTK_RESPONSE_OK, NULL);
    // create entry widgets
    GtkFileFilter* filter_image = gtk_file_filter_new();
    gtk_file_filter_add_pixbuf_formats(filter_image);
        
    GtkWidget* button_filechooser = gtk_button_new_with_label("Datenbankdatei");
    g_signal_connect(G_OBJECT(button_filechooser), "clicked",
                     G_CALLBACK(new_file_dialog), button_filechooser);
    GtkWidget* entry_db = gtk_entry_new();
    GtkWidget* entry_name = gtk_entry_new();
    GtkWidget* entry_prename = gtk_entry_new();
    GtkWidget* entry_bank = gtk_entry_new();
    GtkWidget* entry_iban = gtk_entry_new();
    GtkWidget* entry_bic = gtk_entry_new();
    GtkWidget* entry_postal = gtk_entry_new();
    GtkWidget* entry_adress = gtk_entry_new();
    GtkWidget* button_header = gtk_file_chooser_button_new("Wähle Abrechnungsheader aus", GTK_FILE_CHOOSER_ACTION_OPEN);
    GtkWidget* button_config = gtk_file_chooser_button_new("Wähle Konfigurationsdatei aus", GTK_FILE_CHOOSER_ACTION_OPEN);
    gtk_file_chooser_add_filter(GTK_FILE_CHOOSER(button_header), filter_image);
    
    GtkWidget* entry_slots = gtk_text_view_new();
    GtkWidget* entry_rates = gtk_text_view_new();
    
    GtkWidget* label = gtk_label_new("Hier bitte neue Datenbank anlegen:\n");
    GtkWidget* label_file = gtk_label_new("Datenbank(datei):");
    GtkWidget* label_name = gtk_label_new("Name:");
    GtkWidget* label_prename = gtk_label_new("Vorname:");
    GtkWidget* label_bank = gtk_label_new("Kreditinstitut:");
    GtkWidget* label_iban = gtk_label_new("IBAN:");
    GtkWidget* label_bic = gtk_label_new("BIC:");
    GtkWidget* label_postal = gtk_label_new("PLZ/Ort:");
    GtkWidget* label_adress = gtk_label_new("Straße, Hausnummer:");
    GtkWidget* label_config = gtk_label_new("Konfigurationsdatei:");
    GtkWidget* label_header = gtk_label_new("Headerabbildung für Abrechnung:");
    GtkWidget* label_slots = gtk_label_new("Mögliche Trainings:");
    GtkWidget* label_rates = gtk_label_new("Vergütungsstufen:");
    gtk_entry_set_placeholder_text(GTK_ENTRY(entry_name),"Name");
    gtk_entry_set_text(GTK_ENTRY(entry_db),incoming->dbname);
    
    gtk_entry_set_placeholder_text(GTK_ENTRY(entry_prename),"Vorname");
    gtk_entry_set_placeholder_text(GTK_ENTRY(entry_iban),"IBAN");
    gtk_entry_set_placeholder_text(GTK_ENTRY(entry_bic),"BIC");
    gtk_entry_set_placeholder_text(GTK_ENTRY(entry_bank),"Bank");
    gtk_entry_set_placeholder_text(GTK_ENTRY(entry_postal),"PLZ, Ort");
    gtk_entry_set_placeholder_text(GTK_ENTRY(entry_adress),"Straße, Hausnummer");
    
    
    
    //assemble widgets
    GtkWidget* contentbox = gtk_dialog_get_content_area(GTK_DIALOG(dialog));
    gtk_box_pack_start(GTK_BOX(contentbox), label, TRUE, TRUE, 0);
    gtk_box_pack_start(GTK_BOX(contentbox), label_file, TRUE, TRUE, 0);
    if(!incoming->usemysql){
        gtk_box_pack_start(GTK_BOX(contentbox), button_filechooser, TRUE, TRUE, 0);
    }else{
        gtk_box_pack_start(GTK_BOX(contentbox), entry_db, TRUE, TRUE, 0);
    }
    gtk_box_pack_start(GTK_BOX(contentbox), label_name, TRUE, TRUE, 0);
    gtk_box_pack_start(GTK_BOX(contentbox), entry_name, TRUE, TRUE, 0);
    gtk_box_pack_start(GTK_BOX(contentbox), label_prename, TRUE, TRUE, 0);
    gtk_box_pack_start(GTK_BOX(contentbox), entry_prename, TRUE, TRUE, 0);
     gtk_box_pack_start(GTK_BOX(contentbox), label_config, TRUE, TRUE, 0);
    gtk_box_pack_start(GTK_BOX(contentbox), button_config, TRUE, TRUE, 0);
    gtk_box_pack_start(GTK_BOX(contentbox), label_bank, TRUE, TRUE, 0);
    gtk_box_pack_start(GTK_BOX(contentbox), entry_bank, TRUE, TRUE, 0);
    gtk_box_pack_start(GTK_BOX(contentbox), label_iban, TRUE, TRUE, 0);
    gtk_box_pack_start(GTK_BOX(contentbox), entry_iban, TRUE, TRUE, 0);
    gtk_box_pack_start(GTK_BOX(contentbox), label_bic, TRUE, TRUE, 0);
    gtk_box_pack_start(GTK_BOX(contentbox), entry_bic, TRUE, TRUE, 0);
    gtk_box_pack_start(GTK_BOX(contentbox), label_postal, TRUE, TRUE, 0);
    gtk_box_pack_start(GTK_BOX(contentbox), entry_postal, TRUE, TRUE, 0);
    gtk_box_pack_start(GTK_BOX(contentbox), label_adress, TRUE, TRUE, 0);
    gtk_box_pack_start(GTK_BOX(contentbox), entry_adress, TRUE, TRUE, 0);
    gtk_box_pack_start(GTK_BOX(contentbox), label_header, TRUE, TRUE, 0);
    gtk_box_pack_start(GTK_BOX(contentbox), button_header, TRUE, TRUE, 0);
    gtk_box_pack_start(GTK_BOX(contentbox), label_slots, TRUE, TRUE, 0);
    gtk_box_pack_start(GTK_BOX(contentbox), entry_slots, TRUE, TRUE, 0);
    gtk_box_pack_start(GTK_BOX(contentbox), label_rates, TRUE, TRUE, 0);
    gtk_box_pack_start(GTK_BOX(contentbox), entry_rates, TRUE, TRUE, 0);
    
    gtk_widget_show_all(dialog); //important: needed for reasons
    gint result = gtk_dialog_run(GTK_DIALOG(dialog));
    switch (result){
        case GTK_RESPONSE_OK:
            write_initial(dialog, data);
            break;
        default:
            break;
    }
    gtk_widget_destroy(dialog);
};

/*
 * Function: update_judoka_dialog
 * ----------------------------
 *   provides a dialog to update data of selected judoka
 *
 *   widget: the button that has called the callback, 
 *    
 *   data: renshu_cb_provider, provides database and output window
 *
 */
void update_judoka_dialog(GtkWidget *widget, gpointer data){
    
    renshu_cb_provider* incoming = (renshu_cb_provider*)data;
    GtkWidget* judoka_view = incoming->output;
    GtkTreeSelection* selection = gtk_tree_view_get_selection(GTK_TREE_VIEW(judoka_view));
    GtkTreeModel* thismodel;
    GtkTreeIter iter;
    gint id_int = 0;
    gchar* id;
    if(gtk_tree_selection_get_selected(selection, &thismodel, &iter)){
        gtk_tree_model_get(thismodel, &iter, ID_COLUMN, &id_int,-1);
        asprintf(&id, "%d", id_int);
    }else{
        error_dialog("no row selected\n"); 
        return;
    }
    
    GtkWidget* dialog = gtk_dialog_new_with_buttons("Judoka aktualisieren", NULL, GTK_DIALOG_MODAL,"Abbruch", GTK_RESPONSE_CANCEL, "OK", GTK_RESPONSE_OK, NULL);
    // create entry widgets
    GtkWidget* entry_name = gtk_entry_new();
    GtkWidget* entry_prename = gtk_entry_new();
    GtkWidget* entry_yob = gtk_entry_new();
    GtkWidget* entry_graduation = gtk_entry_new();
    GtkWidget* entry_contact = gtk_entry_new();
    GtkWidget* entry_passport = gtk_entry_new();
    GtkWidget* entry_notes = gtk_entry_new();
    
    GtkWidget* label = gtk_label_new("Hier bitte neue Daten des Judoka eintragen:\n");
    GtkWidget* label_name = gtk_label_new("Name:");
    GtkWidget* label_prename = gtk_label_new("Vorname:");
    GtkWidget* label_yob = gtk_label_new("Geburtsdatum:");
    GtkWidget* label_graduation = gtk_label_new("Graduierung:");
    GtkWidget* label_license = gtk_label_new("Lizenz:");
    GtkWidget* label_contact = gtk_label_new("Kontakt:");
    GtkWidget* label_passport = gtk_label_new("Passnummer:");
    GtkWidget* label_notes = gtk_label_new("Bemerkungen:");
    
    GtkWidget* button_license = gtk_combo_box_text_new();
    read_db(GTK_TREE_STORE(button_license), incoming, "select rateid,identifier from rates;", 5);
    
    
    char* namequery = "";
    asprintf(&namequery, "select name from judoka where pupils_id='%s';", id); 
    char* prenamequery = "";
    asprintf(&prenamequery, "select prename from judoka where pupils_id='%s';", id);
    char* yobquery = "";
    asprintf(&yobquery, "select date_of_birth from judoka where pupils_id='%s';", id); 
    char* gradquery = "";
    asprintf(&gradquery, "select graduation from judoka where pupils_id='%s';", id);
    char* licquery = "";
    if(incoming->usemysql){
        asprintf(&licquery, "select concat(rates.rateid,' ',rates.identifier) from judoka inner join rates on judoka.license=rates.rateid where pupils_id='%s';", id); 
    }else{
        asprintf(&licquery, "select (rates.rateid||' '||rates.identifier) from judoka inner join rates on judoka.license=rates.rateid where pupils_id='%s';", id); 
    }
    char* contactquery = "";
    asprintf(&contactquery, "select contact from judoka where pupils_id='%s';", id); 
    char* passportquery = "";
    asprintf(&passportquery, "select passport from judoka where pupils_id='%s';", id); 
    char* notesquery = "";
    asprintf(&notesquery, "select notes from judoka where pupils_id='%s';", id); 
    
    
    char text_name[50];
    char text_prename[50];
    char text_yob[50];
    char text_graduation[50];
    char text_license[50];
    char text_passport[50];
    char text_contact[100];
    char text_notes[200];
    
    get_single_entry(text_name, namequery, incoming);
    get_single_entry(text_prename, prenamequery, incoming);
    get_single_entry(text_yob, yobquery, incoming);
    get_single_entry(text_graduation, gradquery, incoming);
    get_single_entry(text_license, licquery, incoming);
    get_single_entry(text_contact, contactquery, incoming);
    get_single_entry(text_passport, passportquery, incoming);
    get_single_entry(text_notes, notesquery, incoming);
    
    printf("textentry: %s\n",text_license);
    
    free(namequery);
    free(prenamequery);
    free(yobquery);
    free(gradquery);
    free(licquery);
    free(contactquery);
    free(passportquery);
    free(notesquery);
    g_free(id);
    
    gtk_entry_set_text(GTK_ENTRY(entry_name), text_name);
    gtk_entry_set_text(GTK_ENTRY(entry_prename), text_prename);
    gtk_entry_set_text(GTK_ENTRY(entry_yob), text_yob);
    gtk_entry_set_text(GTK_ENTRY(entry_graduation), text_graduation);
    gtk_combo_box_set_active_id(GTK_COMBO_BOX(button_license), text_license);
    gtk_entry_set_text(GTK_ENTRY(entry_contact), text_contact);
    gtk_entry_set_text(GTK_ENTRY(entry_passport), text_passport);
    gtk_entry_set_text(GTK_ENTRY(entry_notes), text_notes);
    
    //assemble widgets
    GtkWidget* contentbox = gtk_dialog_get_content_area(GTK_DIALOG(dialog));
    gtk_box_pack_start(GTK_BOX(contentbox), label, TRUE, TRUE, 0);
    gtk_box_pack_start(GTK_BOX(contentbox), label_name, TRUE, TRUE, 0);
    gtk_box_pack_start(GTK_BOX(contentbox), entry_name, TRUE, TRUE, 0);
    gtk_box_pack_start(GTK_BOX(contentbox), label_prename, TRUE, TRUE, 0);
    gtk_box_pack_start(GTK_BOX(contentbox), entry_prename, TRUE, TRUE, 0);
    gtk_box_pack_start(GTK_BOX(contentbox), label_yob, TRUE, TRUE, 0);
    gtk_box_pack_start(GTK_BOX(contentbox), entry_yob, TRUE, TRUE, 0);
    gtk_box_pack_start(GTK_BOX(contentbox), label_graduation, TRUE, TRUE, 0);
    gtk_box_pack_start(GTK_BOX(contentbox), entry_graduation, TRUE, TRUE, 0);
    gtk_box_pack_start(GTK_BOX(contentbox), label_license, TRUE, TRUE, 0);
    gtk_box_pack_start(GTK_BOX(contentbox), button_license, TRUE, TRUE, 0);
    gtk_box_pack_start(GTK_BOX(contentbox), label_contact, TRUE, TRUE, 0);
    gtk_box_pack_start(GTK_BOX(contentbox), entry_contact, TRUE, TRUE, 0);
    gtk_box_pack_start(GTK_BOX(contentbox), label_passport, TRUE, TRUE, 0);
    gtk_box_pack_start(GTK_BOX(contentbox), entry_passport, TRUE, TRUE, 0);
    gtk_box_pack_start(GTK_BOX(contentbox), label_notes, TRUE, TRUE, 0);
    gtk_box_pack_start(GTK_BOX(contentbox), entry_notes, TRUE, TRUE, 0);
    
    gtk_widget_show_all(dialog); //important: needed for reasons
    gint result = gtk_dialog_run(GTK_DIALOG(dialog));
    
    switch (result){
        case GTK_RESPONSE_OK:
            update_judoka(dialog, data);
            break;
        default:
            break;
    }
    gtk_widget_destroy(dialog);
};

/*
 * Function: search_judoka_dialog
 * ----------------------------
 *   provides a dialog to update data of selected judoka
 *
 *   widget: the button that has called the callback, 
 *    
 *   data: renshu_cb_provider, provides database and output window
 *
 */
void search_judoka_dialog(GtkWidget *widget, gpointer data){
    
    GtkWidget* dialog = gtk_dialog_new_with_buttons("Judoka durchsuchen", NULL, GTK_DIALOG_MODAL,"Abbruch", GTK_RESPONSE_CANCEL, "OK", GTK_RESPONSE_OK, NULL);
    // create entry widgets
    GtkWidget* entry_field = gtk_entry_new();
    
    GtkWidget* label = gtk_label_new("Hier Suchbegriff eintragen:\n");
    GtkWidget* label_field = gtk_label_new("(Name,Vorname,Lizenz,Bemerkungen oder Passnummer):");
    
    //assemble widgets
    GtkWidget* contentbox = gtk_dialog_get_content_area(GTK_DIALOG(dialog));
    gtk_box_pack_start(GTK_BOX(contentbox), label, TRUE, TRUE, 0);
    gtk_box_pack_start(GTK_BOX(contentbox), label_field, TRUE, TRUE, 0);
    gtk_box_pack_start(GTK_BOX(contentbox), entry_field, TRUE, TRUE, 0);
    
    gtk_widget_show_all(dialog); //important: needed for reasons
    gint result = gtk_dialog_run(GTK_DIALOG(dialog));
    
    switch (result){
        case GTK_RESPONSE_OK:
            search_judoka(dialog, data);
            break;
        default:
            break;
    }
    gtk_widget_destroy(dialog);
};

/*
 * Function: about_dialog
 * ----------------------------
 *   provides about window
 *
 *   widget: the button that has called the callback, 
 *    
 *   data: renshu_cb_provider, provides database and output window
 *
 */
void about_dialog(GtkWidget* widget, gpointer data){
    GdkPixbuf *pixbuf = gdk_pixbuf_new_from_file("/usr/share/pixmaps/renshu.png", NULL);
    GtkWidget *dialog = gtk_about_dialog_new();
    gtk_about_dialog_set_program_name(GTK_ABOUT_DIALOG(dialog), "Renshu");
    gtk_about_dialog_set_version(GTK_ABOUT_DIALOG(dialog), "2.5"); 
    gtk_about_dialog_set_copyright(GTK_ABOUT_DIALOG(dialog),"(c) Wolfgang Knopki");
    gtk_about_dialog_set_comments(GTK_ABOUT_DIALOG(dialog), 
                                  "Renshu ist eine simple Software zur Verwaltung von Judotrainings");
    gtk_about_dialog_set_website(GTK_ABOUT_DIALOG(dialog), 
                                 "https://bitbucket.org/lobosco/renshu2.0/src");
    gtk_about_dialog_set_license_type(GTK_ABOUT_DIALOG(dialog), 
                                      GTK_LICENSE_GPL_3_0);
    gtk_about_dialog_set_logo(GTK_ABOUT_DIALOG(dialog), 
                              pixbuf);
    gtk_window_set_modal(GTK_WINDOW(dialog), TRUE);
    gtk_dialog_run(GTK_DIALOG (dialog));
    g_object_unref(pixbuf), pixbuf = NULL;
    gtk_widget_destroy(dialog);
};

/*
 * Function: file_chooser_dialog
 * ----------------------------
 *   provides a dialog set current database file
 *
 *   widget: the button that has called the callback, 
 *    
 *   data: renshu_cb_provider, provides database and output window
 *
 */
void file_chooser_dialog(GtkWidget* widget, gpointer data){
    renshu_cb_provider* incoming = (renshu_cb_provider*)data;
    
    GtkFileFilter* filter_ren = gtk_file_filter_new();
    gtk_file_filter_add_pattern(filter_ren, "*.ren");
    GtkWidget* chooser = gtk_file_chooser_dialog_new("Öffne Datenbankdatei", GTK_WINDOW(widget), GTK_FILE_CHOOSER_ACTION_OPEN, "Abbruch", GTK_RESPONSE_CANCEL, "OK", GTK_RESPONSE_OK, NULL);
    gtk_file_chooser_add_filter(GTK_FILE_CHOOSER(chooser), filter_ren);
    gint res = gtk_dialog_run(GTK_DIALOG(chooser));
    if(res == GTK_RESPONSE_OK){
        incoming->usemysql = 0;
        strcpy(incoming->dbname, gtk_file_chooser_get_filename(GTK_FILE_CHOOSER(chooser))); 
    }
    gtk_widget_destroy(chooser);
}

/*
 * Function: salary_chooser_dialog
 * ----------------------------
 *   provides a dialog to enter month, year and qualification for current salary print
 *
 *   widget: the button that has called the callback, 
 *    
 *   data: renshu_cb_provider, provides database and output window
 *
 */
void salary_chooser_dialog(GtkWidget* widget, gpointer data){
    renshu_cb_provider* incoming = (renshu_cb_provider*)data;
    GtkWidget* dialog = gtk_dialog_new_with_buttons("Abrechnung", NULL, GTK_DIALOG_MODAL,"Abbruch", GTK_RESPONSE_CANCEL, "HTML", GTK_RESPONSE_OK, "PDF", GTK_RESPONSE_YES, NULL);
    // create entry widgets
    GtkWidget* button_month = gtk_combo_box_text_new();
    gtk_combo_box_text_append(GTK_COMBO_BOX_TEXT(button_month), "01", "Januar");
    gtk_combo_box_text_append(GTK_COMBO_BOX_TEXT(button_month), "02", "Februar");
    gtk_combo_box_text_append(GTK_COMBO_BOX_TEXT(button_month), "03", "März");
    gtk_combo_box_text_append(GTK_COMBO_BOX_TEXT(button_month), "04", "April");
    gtk_combo_box_text_append(GTK_COMBO_BOX_TEXT(button_month), "05", "Mai");
    gtk_combo_box_text_append(GTK_COMBO_BOX_TEXT(button_month), "06", "Juni");
    gtk_combo_box_text_append(GTK_COMBO_BOX_TEXT(button_month), "07", "Juli");
    gtk_combo_box_text_append(GTK_COMBO_BOX_TEXT(button_month), "08", "August");
    gtk_combo_box_text_append(GTK_COMBO_BOX_TEXT(button_month), "09", "September");
    gtk_combo_box_text_append(GTK_COMBO_BOX_TEXT(button_month), "10", "Oktober");
    gtk_combo_box_text_append(GTK_COMBO_BOX_TEXT(button_month), "11", "November");
    gtk_combo_box_text_append(GTK_COMBO_BOX_TEXT(button_month), "12", "Dezember");
    
    
    GtkWidget* button_year = gtk_combo_box_text_new();
    gtk_combo_box_text_append(GTK_COMBO_BOX_TEXT(button_year), "2015", "2015");
    gtk_combo_box_text_append(GTK_COMBO_BOX_TEXT(button_year), "2016", "2016");
    gtk_combo_box_text_append(GTK_COMBO_BOX_TEXT(button_year), "2017", "2017");
    gtk_combo_box_text_append(GTK_COMBO_BOX_TEXT(button_year), "2018", "2018");
    gtk_combo_box_text_append(GTK_COMBO_BOX_TEXT(button_year), "2019", "2019");
    gtk_combo_box_text_append(GTK_COMBO_BOX_TEXT(button_year), "2020", "2020");
    gtk_combo_box_text_append(GTK_COMBO_BOX_TEXT(button_year), "2021", "2021");
    gtk_combo_box_text_append(GTK_COMBO_BOX_TEXT(button_year), "2022", "2022");
    gtk_combo_box_text_append(GTK_COMBO_BOX_TEXT(button_year), "2023", "2023");
    gtk_combo_box_text_append(GTK_COMBO_BOX_TEXT(button_year), "2024", "2024");
    gtk_combo_box_text_append(GTK_COMBO_BOX_TEXT(button_year), "2025", "2025");
    
    GtkWidget* button_qualification = gtk_combo_box_text_new();
    read_db(GTK_TREE_STORE(button_qualification), incoming, "select prename,name from settings;", 5);
    
    
    GtkWidget* label = gtk_label_new("Abrechnung auswählen:\n");
    GtkWidget* label_month = gtk_label_new("Monat:");
    GtkWidget* label_year = gtk_label_new("Jahr:");
    GtkWidget* label_qualification = gtk_label_new("Trainer:");
    
    //assemble widgets
    GtkWidget* contentbox = gtk_dialog_get_content_area(GTK_DIALOG(dialog));
    gtk_box_pack_start(GTK_BOX(contentbox), label, TRUE, TRUE, 0);
    gtk_box_pack_start(GTK_BOX(contentbox), label_month, TRUE, TRUE, 0);
    gtk_box_pack_start(GTK_BOX(contentbox), button_month, TRUE, TRUE, 0);
    gtk_box_pack_start(GTK_BOX(contentbox), label_year, TRUE, TRUE, 0);
    gtk_box_pack_start(GTK_BOX(contentbox), button_year, TRUE, TRUE, 0);
    gtk_box_pack_start(GTK_BOX(contentbox), label_qualification, TRUE, TRUE, 0);
    gtk_box_pack_start(GTK_BOX(contentbox), button_qualification, TRUE, TRUE, 0);
    
    gtk_widget_show_all(dialog); //important: needed for reasons
    gint result = gtk_dialog_run(GTK_DIALOG(dialog));
    
    switch (result){
        case GTK_RESPONSE_OK:
            print_salary(dialog, data);
            break;
        case GTK_RESPONSE_YES:
            print_salary_pdf(dialog, data);
            break;
        default:
            break;
    }
    gtk_widget_destroy(dialog);
    
};

/*
 * Function: save_dialog
 * ----------------------------
 *   provides a dialog to save data to a file
 *
 *   widget: the button that has called the callback, 
 *    
 *   data: string to write into the file
 *
 */
void save_dialog(GtkWidget* widget, gpointer data, int kind){
    char* text_to_write = (char*) data;
    GtkFileFilter* filter_htmltxt = gtk_file_filter_new();
    GtkWidget* dialog = gtk_file_chooser_dialog_new("Speichere Datei", GTK_WINDOW(widget), GTK_FILE_CHOOSER_ACTION_SAVE, "Abbruch", GTK_RESPONSE_CANCEL, "OK", GTK_RESPONSE_OK, NULL);
    if(kind == 1){
        gtk_file_filter_add_pattern(filter_htmltxt, "*.tex");
        gtk_file_chooser_set_current_name (GTK_FILE_CHOOSER(dialog), "untitled.tex");
    }else{
        gtk_file_filter_add_pattern(filter_htmltxt, "*.html");
        gtk_file_chooser_set_current_name (GTK_FILE_CHOOSER(dialog), "untitled.html");
    }
    
    gtk_file_chooser_add_filter(GTK_FILE_CHOOSER(dialog), filter_htmltxt);
    gtk_file_chooser_set_do_overwrite_confirmation(GTK_FILE_CHOOSER(dialog), TRUE);
    gint res = gtk_dialog_run(GTK_DIALOG(dialog));
    if(res == GTK_RESPONSE_OK){
        char* filename = gtk_file_chooser_get_filename(GTK_FILE_CHOOSER(dialog));
        FILE* f = fopen(filename, "w");
        fprintf(f, "%s", text_to_write);
        fclose(f);
        if(kind == 1){
            char* compileandclean = NULL;
            asprintf(&compileandclean, "latexmk -cd -pdf %s && latexmk -cd -c %s", filename,filename);
            system(compileandclean);
        }
        g_free(filename);
    }
    gtk_widget_destroy(dialog);
};

/*
 * Function: update_config_dialog
 * ----------------------------
 *   provides a dialog to update the configuration of the current database
 *
 *   widget: the button that has called the callback, 
 *    
 *   data: renshu_cb_provider, provides database and output window
 *
 */
void update_config_dialog(GtkWidget* widget, gpointer data){ 
    renshu_cb_provider* incoming = (renshu_cb_provider *) data;
    GtkFileFilter* filter_image = gtk_file_filter_new();
    gtk_file_filter_add_pixbuf_formats(filter_image);
    
    GtkWidget* dialog = gtk_dialog_new_with_buttons("Datenbankeinstellungen aktualisieren", NULL, GTK_DIALOG_MODAL,"Abbruch", GTK_RESPONSE_CANCEL, "OK", GTK_RESPONSE_OK, NULL);
    // create entry widgets
    GtkWidget* button_chose = gtk_combo_box_text_new();
    read_db(GTK_TREE_STORE(button_chose), incoming, "select prename,name from settings;", 5);
   
    GtkWidget* entry_name = gtk_entry_new();
    GtkWidget* entry_prename = gtk_entry_new();
    GtkWidget* entry_bank = gtk_entry_new();
    GtkWidget* entry_iban = gtk_entry_new();
    GtkWidget* entry_bic = gtk_entry_new();
    GtkWidget* entry_postal = gtk_entry_new();
    GtkWidget* entry_adress = gtk_entry_new();
    GtkWidget* button_header = gtk_file_chooser_button_new("Wähle Abrechnungsheader aus", GTK_FILE_CHOOSER_ACTION_OPEN);
    gtk_file_chooser_add_filter(GTK_FILE_CHOOSER(button_header), filter_image);
    GtkWidget* button_config = gtk_file_chooser_button_new("Wähle Konfigurationsdatei aus", GTK_FILE_CHOOSER_ACTION_OPEN);
    GtkWidget* label = gtk_label_new("Datenbankeinstellungen:\n");
    GtkWidget* label_name = gtk_label_new("Name:");
    GtkWidget* label_prename = gtk_label_new("Vorname:");
    GtkWidget* label_bank = gtk_label_new("Kreditinstitut:");
    GtkWidget* label_iban = gtk_label_new("IBAN:");
    GtkWidget* label_bic = gtk_label_new("BIC:");
    GtkWidget* label_postal = gtk_label_new("PLZ/Ort:");
    GtkWidget* label_adress = gtk_label_new("Straße, Hausnummer:");
    GtkWidget* label_header = gtk_label_new("Headerabbildung für Abrechnung:");
    GtkWidget* label_config = gtk_label_new("Konfigurationsdatei:");
    
    
    //assemble widgets
    GtkWidget* contentbox = gtk_dialog_get_content_area(GTK_DIALOG(dialog));
    gtk_box_pack_start(GTK_BOX(contentbox), label, TRUE, TRUE, 0);
    gtk_box_pack_start(GTK_BOX(contentbox), button_chose, TRUE, TRUE, 0);
    gtk_box_pack_start(GTK_BOX(contentbox), label_name, TRUE, TRUE, 0);
    gtk_box_pack_start(GTK_BOX(contentbox), entry_name, TRUE, TRUE, 0);
    gtk_box_pack_start(GTK_BOX(contentbox), label_prename, TRUE, TRUE, 0);
    gtk_box_pack_start(GTK_BOX(contentbox), entry_prename, TRUE, TRUE, 0);
    gtk_box_pack_start(GTK_BOX(contentbox), label_config, TRUE, TRUE, 0);
    gtk_box_pack_start(GTK_BOX(contentbox), button_config, TRUE, TRUE, 0);
    gtk_box_pack_start(GTK_BOX(contentbox), label_bank, TRUE, TRUE, 0);
    gtk_box_pack_start(GTK_BOX(contentbox), entry_bank, TRUE, TRUE, 0);
    gtk_box_pack_start(GTK_BOX(contentbox), label_iban, TRUE, TRUE, 0);
    gtk_box_pack_start(GTK_BOX(contentbox), entry_iban, TRUE, TRUE, 0);
    gtk_box_pack_start(GTK_BOX(contentbox), label_bic, TRUE, TRUE, 0);
    gtk_box_pack_start(GTK_BOX(contentbox), entry_bic, TRUE, TRUE, 0);
    gtk_box_pack_start(GTK_BOX(contentbox), label_postal, TRUE, TRUE, 0);
    gtk_box_pack_start(GTK_BOX(contentbox), entry_postal, TRUE, TRUE, 0);
    gtk_box_pack_start(GTK_BOX(contentbox), label_adress, TRUE, TRUE, 0);
    gtk_box_pack_start(GTK_BOX(contentbox), entry_adress, TRUE, TRUE, 0);
    gtk_box_pack_start(GTK_BOX(contentbox), label_header, TRUE, TRUE, 0);
    gtk_box_pack_start(GTK_BOX(contentbox), button_header, TRUE, TRUE, 0);
    
     g_signal_connect( G_OBJECT( button_chose ), "changed",
                      G_CALLBACK(settings_changed_callback ), incoming);
    
    
    gtk_widget_show_all(dialog); //important: needed for reasons
    gint result = gtk_dialog_run(GTK_DIALOG(dialog));
    switch (result){
        case GTK_RESPONSE_OK:
            update_settings(dialog, data);
            break;
        default:
            break;
    }
    gtk_widget_destroy(dialog);  
};

/*
 * Function: competition_chooser_dialog
 * ----------------------------
 *   provides a dialog to enter year for current competitor print
 *
 *   widget: the button that has called the callback, 
 *    
 *   data: renshu_cb_provider, provides database and output window
 *
 */
void competition_chooser_dialog(GtkWidget* widget, gpointer data){
    
    GtkWidget* dialog = gtk_dialog_new_with_buttons("Wettkämpfer Jahresübersicht", NULL, GTK_DIALOG_MODAL,"Abbruch", GTK_RESPONSE_CANCEL, "HTML", GTK_RESPONSE_OK, "PDF", GTK_RESPONSE_YES, NULL);
    
    GtkWidget* button_year = gtk_combo_box_text_new();
    gtk_combo_box_text_append(GTK_COMBO_BOX_TEXT(button_year), "2014", "2014");
    gtk_combo_box_text_append(GTK_COMBO_BOX_TEXT(button_year), "2015", "2015");
    gtk_combo_box_text_append(GTK_COMBO_BOX_TEXT(button_year), "2016", "2016");
    gtk_combo_box_text_append(GTK_COMBO_BOX_TEXT(button_year), "2017", "2017");
    gtk_combo_box_text_append(GTK_COMBO_BOX_TEXT(button_year), "2018", "2018");
    gtk_combo_box_text_append(GTK_COMBO_BOX_TEXT(button_year), "2019", "2019");
    gtk_combo_box_text_append(GTK_COMBO_BOX_TEXT(button_year), "2020", "2020");
    gtk_combo_box_text_append(GTK_COMBO_BOX_TEXT(button_year), "2021", "2021");
    gtk_combo_box_text_append(GTK_COMBO_BOX_TEXT(button_year), "2022", "2022");
    gtk_combo_box_text_append(GTK_COMBO_BOX_TEXT(button_year), "2023", "2023");
    gtk_combo_box_text_append(GTK_COMBO_BOX_TEXT(button_year), "2024", "2024");
    gtk_combo_box_text_append(GTK_COMBO_BOX_TEXT(button_year), "2025", "2025");
    
    GtkWidget* label_year = gtk_label_new("Jahr:");
   
    //assemble widgets
    GtkWidget* contentbox = gtk_dialog_get_content_area(GTK_DIALOG(dialog));
    
    gtk_box_pack_start(GTK_BOX(contentbox), label_year, TRUE, TRUE, 0);
    gtk_box_pack_start(GTK_BOX(contentbox), button_year, TRUE, TRUE, 0);
    
    gtk_widget_show_all(dialog); //important: needed for reasons
    gint result = gtk_dialog_run(GTK_DIALOG(dialog));
    
    switch (result){
        case GTK_RESPONSE_OK:
            print_competitors(dialog, data);
            break;
        case GTK_RESPONSE_YES:
            print_competitors_pdf(dialog, data);
            break;
        default:
            break;
    }
    gtk_widget_destroy(dialog);
    
};


/*
 * Function: list_chooser_dialog
 * ----------------------------
 *   provides a dialog to enter month and year for current training list print
 *
 *   widget: the button that has called the callback, 
 *    
 *   data: renshu_cb_provider, provides database and output window
 *
 */
void list_chooser_dialog(GtkWidget* widget, gpointer data){
    renshu_cb_provider* incoming = (renshu_cb_provider *) data;
    GtkWidget* dialog = gtk_dialog_new_with_buttons("Trainingslisten seit", NULL, GTK_DIALOG_MODAL,"Abbruch", GTK_RESPONSE_CANCEL, "HTML", GTK_RESPONSE_OK,"PDF", GTK_RESPONSE_YES, NULL);
    
    GtkWidget* button_slot = gtk_combo_box_text_new();
    read_db(GTK_TREE_STORE(button_slot), incoming, "select day, nr from slots;", 5);
    
    GtkWidget* button_year = gtk_combo_box_text_new();
    
    GtkWidget* button_month = gtk_combo_box_text_new();
    gtk_combo_box_text_append(GTK_COMBO_BOX_TEXT(button_month), "01", "Januar");
    gtk_combo_box_text_append(GTK_COMBO_BOX_TEXT(button_month), "02", "Februar");
    gtk_combo_box_text_append(GTK_COMBO_BOX_TEXT(button_month), "03", "März");
    gtk_combo_box_text_append(GTK_COMBO_BOX_TEXT(button_month), "04", "April");
    gtk_combo_box_text_append(GTK_COMBO_BOX_TEXT(button_month), "05", "Mai");
    gtk_combo_box_text_append(GTK_COMBO_BOX_TEXT(button_month), "06", "Juni");
    gtk_combo_box_text_append(GTK_COMBO_BOX_TEXT(button_month), "07", "Juli");
    gtk_combo_box_text_append(GTK_COMBO_BOX_TEXT(button_month), "08", "August");
    gtk_combo_box_text_append(GTK_COMBO_BOX_TEXT(button_month), "09", "September");
    gtk_combo_box_text_append(GTK_COMBO_BOX_TEXT(button_month), "10", "Oktober");
    gtk_combo_box_text_append(GTK_COMBO_BOX_TEXT(button_month), "11", "November");
    gtk_combo_box_text_append(GTK_COMBO_BOX_TEXT(button_month), "12", "Dezember");
    
    gtk_combo_box_text_append(GTK_COMBO_BOX_TEXT(button_year), "2013", "2013");
    gtk_combo_box_text_append(GTK_COMBO_BOX_TEXT(button_year), "2014", "2014");
    gtk_combo_box_text_append(GTK_COMBO_BOX_TEXT(button_year), "2015", "2015");
    gtk_combo_box_text_append(GTK_COMBO_BOX_TEXT(button_year), "2016", "2016");
    gtk_combo_box_text_append(GTK_COMBO_BOX_TEXT(button_year), "2017", "2017");
    gtk_combo_box_text_append(GTK_COMBO_BOX_TEXT(button_year), "2018", "2018");
    gtk_combo_box_text_append(GTK_COMBO_BOX_TEXT(button_year), "2019", "2019");
    gtk_combo_box_text_append(GTK_COMBO_BOX_TEXT(button_year), "2020", "2020");
    gtk_combo_box_text_append(GTK_COMBO_BOX_TEXT(button_year), "2021", "2021");
    gtk_combo_box_text_append(GTK_COMBO_BOX_TEXT(button_year), "2022", "2022");
    gtk_combo_box_text_append(GTK_COMBO_BOX_TEXT(button_year), "2023", "2023");
    gtk_combo_box_text_append(GTK_COMBO_BOX_TEXT(button_year), "2024", "2024");
    gtk_combo_box_text_append(GTK_COMBO_BOX_TEXT(button_year), "2025", "2025");
    
    GtkWidget* button_year2 = gtk_combo_box_text_new();
    
    GtkWidget* button_month2 = gtk_combo_box_text_new();
    gtk_combo_box_text_append(GTK_COMBO_BOX_TEXT(button_month2), "01", "Januar");
    gtk_combo_box_text_append(GTK_COMBO_BOX_TEXT(button_month2), "02", "Februar");
    gtk_combo_box_text_append(GTK_COMBO_BOX_TEXT(button_month2), "03", "März");
    gtk_combo_box_text_append(GTK_COMBO_BOX_TEXT(button_month2), "04", "April");
    gtk_combo_box_text_append(GTK_COMBO_BOX_TEXT(button_month2), "05", "Mai");
    gtk_combo_box_text_append(GTK_COMBO_BOX_TEXT(button_month2), "06", "Juni");
    gtk_combo_box_text_append(GTK_COMBO_BOX_TEXT(button_month2), "07", "Juli");
    gtk_combo_box_text_append(GTK_COMBO_BOX_TEXT(button_month2), "08", "August");
    gtk_combo_box_text_append(GTK_COMBO_BOX_TEXT(button_month2), "09", "September");
    gtk_combo_box_text_append(GTK_COMBO_BOX_TEXT(button_month2), "10", "Oktober");
    gtk_combo_box_text_append(GTK_COMBO_BOX_TEXT(button_month2), "11", "November");
    gtk_combo_box_text_append(GTK_COMBO_BOX_TEXT(button_month2), "12", "Dezember");
    
    gtk_combo_box_text_append(GTK_COMBO_BOX_TEXT(button_year2), "2013", "2013");
    gtk_combo_box_text_append(GTK_COMBO_BOX_TEXT(button_year2), "2014", "2014");
    gtk_combo_box_text_append(GTK_COMBO_BOX_TEXT(button_year2), "2015", "2015");
    gtk_combo_box_text_append(GTK_COMBO_BOX_TEXT(button_year2), "2016", "2016");
    gtk_combo_box_text_append(GTK_COMBO_BOX_TEXT(button_year2), "2017", "2017");
    gtk_combo_box_text_append(GTK_COMBO_BOX_TEXT(button_year2), "2018", "2018");
    gtk_combo_box_text_append(GTK_COMBO_BOX_TEXT(button_year2), "2019", "2019");
    gtk_combo_box_text_append(GTK_COMBO_BOX_TEXT(button_year2), "2020", "2020");
    gtk_combo_box_text_append(GTK_COMBO_BOX_TEXT(button_year2), "2021", "2021");
    gtk_combo_box_text_append(GTK_COMBO_BOX_TEXT(button_year2), "2022", "2022");
    gtk_combo_box_text_append(GTK_COMBO_BOX_TEXT(button_year2), "2023", "2023");
    gtk_combo_box_text_append(GTK_COMBO_BOX_TEXT(button_year2), "2024", "2024");
    gtk_combo_box_text_append(GTK_COMBO_BOX_TEXT(button_year2), "2025", "2025");
    
    GtkWidget* label_from = gtk_label_new("Trainingslisten VON:");
    GtkWidget* label_year = gtk_label_new("Jahr:");
    GtkWidget* label_month = gtk_label_new("Monat:");
    GtkWidget* label_to = gtk_label_new("\nBIS:");
    GtkWidget* label_year2 = gtk_label_new("Jahr:");
    GtkWidget* label_month2 = gtk_label_new("Monat:");
    GtkWidget* label_slot = gtk_label_new("Slot:");
    
    //assemble widgets
    GtkWidget* contentbox = gtk_dialog_get_content_area(GTK_DIALOG(dialog));
    gtk_box_pack_start(GTK_BOX(contentbox), label_from, TRUE, TRUE, 0);
    gtk_box_pack_start(GTK_BOX(contentbox), label_month, TRUE, TRUE, 0);
    gtk_box_pack_start(GTK_BOX(contentbox), button_month, TRUE, TRUE, 0);
    gtk_box_pack_start(GTK_BOX(contentbox), label_year, TRUE, TRUE, 0);
    gtk_box_pack_start(GTK_BOX(contentbox), button_year, TRUE, TRUE, 0);
    gtk_box_pack_start(GTK_BOX(contentbox), label_to, TRUE, TRUE, 0);
    gtk_box_pack_start(GTK_BOX(contentbox), label_month2, TRUE, TRUE, 0);
    gtk_box_pack_start(GTK_BOX(contentbox), button_month2, TRUE, TRUE, 0);
    gtk_box_pack_start(GTK_BOX(contentbox), label_year2, TRUE, TRUE, 0);
    gtk_box_pack_start(GTK_BOX(contentbox), button_year2, TRUE, TRUE, 0);
    gtk_box_pack_start(GTK_BOX(contentbox), label_slot, TRUE, TRUE, 0);
    gtk_box_pack_start(GTK_BOX(contentbox), button_slot, TRUE, TRUE, 0);
    
    gtk_widget_show_all(dialog); //important: needed for reasons
    gint result = gtk_dialog_run(GTK_DIALOG(dialog));
    
    switch (result){
        case GTK_RESPONSE_OK:
            print_trainings(dialog, data);
            break;
        case GTK_RESPONSE_YES:
            print_trainings_pdf(dialog, data);
            break;
        
        default:
            break;
    }
    gtk_widget_destroy(dialog);
};

/*
 * Function: remote_db_dialog
 * ----------------------------
 *   provides a dialog to update the configuration of the current database
 *
 *   widget: the button that has called the callback, 
 *    
 *   data: renshu_cb_provider, provides database and output window
 *
 */
int callback_remote_db(GtkWidget *widget, gpointer data){
    renshu_cb_provider* incoming = (renshu_cb_provider *) data;
    int i = 0;
    GList* children = gtk_container_get_children(GTK_CONTAINER(widget));
    incoming->usemysql = 1;
    for(children = g_list_first(children); children != NULL; children = children->next){
        if(GTK_IS_ENTRY(children->data)){
            if(i == 0){
                sprintf(incoming->dbname,"%s",gtk_entry_get_text(GTK_ENTRY(children->data)));
            }
            if(i == 1){
                sprintf(incoming->dbaddr,"%s",gtk_entry_get_text(GTK_ENTRY(children->data)));
            }
            if(i == 2){
                sprintf(incoming->dbuser,"%s",gtk_entry_get_text(GTK_ENTRY(children->data)));
            }
            if(i == 3){
                sprintf(incoming->dbpasswd,"%s",gtk_entry_get_text(GTK_ENTRY(children->data)));
            }
            i++;
        }
    }
    return 0;
};

void remote_db_dialog(GtkWidget* widget, gpointer data){ 
    renshu_cb_provider* incoming = (renshu_cb_provider *) data;
    
    GtkWidget* dialog = gtk_dialog_new_with_buttons("Verbindung zum Datenbankserver", NULL, GTK_DIALOG_MODAL,"Abbruch", GTK_RESPONSE_CANCEL, "OK", GTK_RESPONSE_OK, NULL);
    
    GtkWidget* label = gtk_label_new("Hier bitte die Verbindungsdaten zur Datenbank eintragen:\n");
    GtkWidget* label_name = gtk_label_new("Name:");
    GtkWidget* label_adress = gtk_label_new("Adresse:");
    GtkWidget* label_user = gtk_label_new("Benutzername:");
    GtkWidget* label_passwd = gtk_label_new("Passwort:");
    
    GtkWidget* entry_name = gtk_entry_new();
    GtkWidget* entry_adress = gtk_entry_new();
    GtkWidget* entry_user = gtk_entry_new();
    GtkWidget* entry_passwd = gtk_entry_new();
    
    gtk_entry_set_text(GTK_ENTRY(entry_name), incoming->dbname);
    gtk_entry_set_text(GTK_ENTRY(entry_adress),incoming->dbaddr);
    gtk_entry_set_text(GTK_ENTRY(entry_user),incoming->dbuser);
    gtk_entry_set_text(GTK_ENTRY(entry_passwd),incoming->dbpasswd);
    gtk_entry_set_visibility(GTK_ENTRY(entry_passwd),FALSE);
    
    GtkWidget* contentbox = gtk_dialog_get_content_area(GTK_DIALOG(dialog));
    gtk_box_pack_start(GTK_BOX(contentbox), label, TRUE, TRUE, 0);
    gtk_box_pack_start(GTK_BOX(contentbox), label_name, TRUE, TRUE, 0);
    gtk_box_pack_start(GTK_BOX(contentbox), entry_name, TRUE, TRUE, 0);
    gtk_box_pack_start(GTK_BOX(contentbox), label_adress, TRUE, TRUE, 0);
    gtk_box_pack_start(GTK_BOX(contentbox), entry_adress, TRUE, TRUE, 0);
    gtk_box_pack_start(GTK_BOX(contentbox), label_user, TRUE, TRUE, 0);
    gtk_box_pack_start(GTK_BOX(contentbox), entry_user, TRUE, TRUE, 0);
    gtk_box_pack_start(GTK_BOX(contentbox), label_passwd, TRUE, TRUE, 0);
    gtk_box_pack_start(GTK_BOX(contentbox), entry_passwd, TRUE, TRUE, 0);
    
    gtk_widget_show_all(dialog); //important: needed for reasons
    gint result = gtk_dialog_run(GTK_DIALOG(dialog));
    switch (result){
        case GTK_RESPONSE_OK:
            callback_remote_db(contentbox, incoming);
            break;
        default:
            break;
    }
    gtk_widget_destroy(dialog); 
};

/*
 * Function: add_config_dialog
 * ----------------------------
 *   provides a dialog to add a configuration to the current database
 *
 *   widget: the button that has called the callback, 
 *    
 *   data: renshu_cb_provider, provides database and output window
 *
 */
void add_config_dialog(GtkWidget* widget, gpointer data){ 
    GtkFileFilter* filter_image = gtk_file_filter_new();
    gtk_file_filter_add_pixbuf_formats(filter_image);
    
    GtkWidget* dialog = gtk_dialog_new_with_buttons("Datenbankeinstellungen hinzufügen", NULL, GTK_DIALOG_MODAL,"Abbruch", GTK_RESPONSE_CANCEL, "OK", GTK_RESPONSE_OK, NULL);
   
    GtkWidget* entry_name = gtk_entry_new();
    GtkWidget* entry_prename = gtk_entry_new();
    GtkWidget* entry_bank = gtk_entry_new();
    GtkWidget* entry_iban = gtk_entry_new();
    GtkWidget* entry_bic = gtk_entry_new();
    GtkWidget* entry_postal = gtk_entry_new();
    GtkWidget* entry_adress = gtk_entry_new();
    GtkWidget* button_header = gtk_file_chooser_button_new("Wähle Abrechnungsheader aus", GTK_FILE_CHOOSER_ACTION_OPEN);
    gtk_file_chooser_add_filter(GTK_FILE_CHOOSER(button_header), filter_image);
    GtkWidget* button_config = gtk_file_chooser_button_new("Wähle Konfigurationsdatei aus", GTK_FILE_CHOOSER_ACTION_OPEN);
    GtkWidget* label = gtk_label_new("Datenbankeinstellungen:\n");
    GtkWidget* label_name = gtk_label_new("Name:");
    GtkWidget* label_prename = gtk_label_new("Vorname:");
    GtkWidget* label_bank = gtk_label_new("Kreditinstitut:");
    GtkWidget* label_iban = gtk_label_new("IBAN:");
    GtkWidget* label_bic = gtk_label_new("BIC:");
    GtkWidget* label_postal = gtk_label_new("PLZ/Ort:");
    GtkWidget* label_adress = gtk_label_new("Straße, Hausnummer:");
    GtkWidget* label_header = gtk_label_new("Headerabbildung für Abrechnung:");
    GtkWidget* label_config = gtk_label_new("Konfigurationsdatei:");
    
   
    gtk_entry_set_placeholder_text(GTK_ENTRY(entry_name), "Name");
    gtk_entry_set_placeholder_text(GTK_ENTRY(entry_prename),"Vorname");
    gtk_entry_set_placeholder_text(GTK_ENTRY(entry_iban),"IBAN");
    gtk_entry_set_placeholder_text(GTK_ENTRY(entry_bic),"BIC");
    gtk_entry_set_placeholder_text(GTK_ENTRY(entry_bank),"Kreditinstitut");
    gtk_entry_set_placeholder_text(GTK_ENTRY(entry_postal),"PLZ/Ort");
    gtk_entry_set_placeholder_text(GTK_ENTRY(entry_adress),"Straße/Hausnummer");
    
    //assemble widgets
    GtkWidget* contentbox = gtk_dialog_get_content_area(GTK_DIALOG(dialog));
    gtk_box_pack_start(GTK_BOX(contentbox), label, TRUE, TRUE, 0);
    gtk_box_pack_start(GTK_BOX(contentbox), label_name, TRUE, TRUE, 0);
    gtk_box_pack_start(GTK_BOX(contentbox), entry_name, TRUE, TRUE, 0);
    gtk_box_pack_start(GTK_BOX(contentbox), label_prename, TRUE, TRUE, 0);
    gtk_box_pack_start(GTK_BOX(contentbox), entry_prename, TRUE, TRUE, 0);
    gtk_box_pack_start(GTK_BOX(contentbox), label_config, TRUE, TRUE, 0);
    gtk_box_pack_start(GTK_BOX(contentbox), button_config, TRUE, TRUE, 0);
    gtk_box_pack_start(GTK_BOX(contentbox), label_bank, TRUE, TRUE, 0);
    gtk_box_pack_start(GTK_BOX(contentbox), entry_bank, TRUE, TRUE, 0);
    gtk_box_pack_start(GTK_BOX(contentbox), label_iban, TRUE, TRUE, 0);
    gtk_box_pack_start(GTK_BOX(contentbox), entry_iban, TRUE, TRUE, 0);
    gtk_box_pack_start(GTK_BOX(contentbox), label_bic, TRUE, TRUE, 0);
    gtk_box_pack_start(GTK_BOX(contentbox), entry_bic, TRUE, TRUE, 0);
    gtk_box_pack_start(GTK_BOX(contentbox), label_postal, TRUE, TRUE, 0);
    gtk_box_pack_start(GTK_BOX(contentbox), entry_postal, TRUE, TRUE, 0);
    gtk_box_pack_start(GTK_BOX(contentbox), label_adress, TRUE, TRUE, 0);
    gtk_box_pack_start(GTK_BOX(contentbox), entry_adress, TRUE, TRUE, 0);
    gtk_box_pack_start(GTK_BOX(contentbox), label_header, TRUE, TRUE, 0);
    gtk_box_pack_start(GTK_BOX(contentbox), button_header, TRUE, TRUE, 0);
    
    gtk_widget_show_all(dialog); //important: needed for reasons
    gint result = gtk_dialog_run(GTK_DIALOG(dialog));
    switch (result){
        case GTK_RESPONSE_OK:
            add_new_settings(dialog, data);
            break;
        default:
            break;
    }
    gtk_widget_destroy(dialog);
};

/*
 * Function: update_training_dialog
 * ----------------------------
 *   provides a dialog to update the configuration of the current database
 *
 *   widget: the button that has called the callback, 
 *    
 *   data: renshu_cb_provider, provides database and output window
 *
 */
void update_training_dialog(GtkWidget* widget, gpointer data){ 
    renshu_cb_provider* incoming = (renshu_cb_provider*)data;
    
    GtkWidget* judoka_view = incoming->output;
    GtkTreeSelection* outputselection = gtk_tree_view_get_selection(GTK_TREE_VIEW(judoka_view));
    GtkTreeModel* thismodel;
    GtkTreeIter iter;
    GtkTreeIter iter_parent;
    gchar* date;
    gchar* day;
    gint nr = 0;
    gchar* trainer0;
    gchar* trainer1;
    if(gtk_tree_selection_get_selected(outputselection, &thismodel, &iter)){
        gtk_tree_model_get(thismodel, &iter, DATE_COLUMN, &date, TRAINER0_COLUMN, &trainer0,TRAINER1_COLUMN, &trainer1, -1);
        gtk_tree_model_iter_parent(thismodel,&iter_parent,&iter);
        gtk_tree_model_get(thismodel, &iter_parent, DAY_COLUMN, &day, NR_COLUMN, &nr, -1);
    }else{
        error_dialog("no row selected\n"); 
        return;
    }
    gchar* slot;
    asprintf(&slot,"%s %d",day,nr);
    GtkWidget* dialog = gtk_dialog_new_with_buttons("Aktualisiere Training", NULL, GTK_DIALOG_MODAL,"Abbruch", GTK_RESPONSE_CANCEL, "OK", GTK_RESPONSE_OK, NULL);
    // create entry widgets
    GtkWidget* button_slot = gtk_combo_box_text_new();
    read_db(GTK_TREE_STORE(button_slot), incoming, "select day, nr from slots;", 5);
  
    GtkWidget* label = gtk_label_new("Hier bitte Training aktualisieren:\n");
    GtkWidget* label_date = gtk_label_new("Datum:");
    GtkWidget* label_trainer = gtk_label_new("Trainer:");
    GtkWidget* label_pupils = gtk_label_new("Schüler:");
    GtkWidget* label_slot = gtk_label_new("Slot auswählen:");
    GtkWidget* entry_date = gtk_entry_new();

    GtkWidget* button_trainer0 = gtk_combo_box_text_new();
    read_db(GTK_TREE_STORE(button_trainer0), incoming, "select prename, name from judoka where license > 1  or pupils_id=0;", 5);
    GtkWidget* button_trainer1 = gtk_combo_box_text_new();
    read_db(GTK_TREE_STORE(button_trainer1), incoming, "select prename, name from judoka where license > 1  or pupils_id=0;", 5);
    
    gtk_entry_set_text(GTK_ENTRY(entry_date),date);
    
    gtk_combo_box_set_active_id(GTK_COMBO_BOX(button_trainer0), trainer0);
    gtk_combo_box_set_active_id(GTK_COMBO_BOX(button_trainer1), trainer1);
    gtk_combo_box_set_active_id(GTK_COMBO_BOX(button_slot), slot);
    
    GtkWidget* treeview_pupils = gtk_tree_view_new();
    gtk_tree_view_set_reorderable(GTK_TREE_VIEW(treeview_pupils), FALSE);
    GtkTreeSelection* selection = gtk_tree_view_get_selection(GTK_TREE_VIEW(treeview_pupils));
    gtk_tree_selection_set_mode(selection, GTK_SELECTION_MULTIPLE);
    GtkWidget* scrollpupils = gtk_scrolled_window_new(NULL, NULL);
    gtk_scrolled_window_set_min_content_height(GTK_SCROLLED_WINDOW(scrollpupils),250);
    
    gtk_container_add(GTK_CONTAINER(scrollpupils), treeview_pupils);
        
     //build the treeview
    GtkTreeStore* store = gtk_tree_store_new(N_COLUMNS, G_TYPE_INT, G_TYPE_STRING, G_TYPE_STRING, G_TYPE_STRING, G_TYPE_INT, G_TYPE_INT, G_TYPE_STRING, G_TYPE_STRING, G_TYPE_STRING);
    /*remove any already set columns/ stores*/
    gtk_tree_view_set_model(GTK_TREE_VIEW(treeview_pupils), NULL);
    GList* columns = gtk_tree_view_get_columns(GTK_TREE_VIEW(treeview_pupils));
    for(columns = g_list_first(columns); columns != NULL; columns = columns->next){
        gtk_tree_view_remove_column(GTK_TREE_VIEW(treeview_pupils), GTK_TREE_VIEW_COLUMN(columns->data));   
    }
    //list no longer needed
    g_list_free(columns);
    GtkCellRenderer *renderer = gtk_cell_renderer_text_new();
    GtkTreeViewColumn* column; 
    column = gtk_tree_view_column_new_with_attributes("ID", renderer, "text", ID_COLUMN, NULL);
    gtk_tree_view_column_set_sort_column_id(column,ID_COLUMN);
    gtk_tree_view_append_column(GTK_TREE_VIEW(treeview_pupils), column);
    column = gtk_tree_view_column_new_with_attributes("Name", renderer, "text", NAME_COLUMN, NULL);
    gtk_tree_view_column_set_sort_column_id(column,NAME_COLUMN);
    gtk_tree_view_append_column(GTK_TREE_VIEW(treeview_pupils), column);
    column = gtk_tree_view_column_new_with_attributes("Vorname", renderer, "text", PRENAME_COLUMN, NULL);
    gtk_tree_view_column_set_sort_column_id(column,PRENAME_COLUMN);
    gtk_tree_view_append_column(GTK_TREE_VIEW(treeview_pupils), column);
    column = gtk_tree_view_column_new_with_attributes("Geburtsdatum", renderer, "text", YOB_COLUMN, NULL);
    gtk_tree_view_column_set_sort_column_id(column,YOB_COLUMN);
    gtk_tree_view_append_column(GTK_TREE_VIEW(treeview_pupils), column);
    column = gtk_tree_view_column_new_with_attributes("Graduierung", renderer, "text", GRADUATION_COLUMN, NULL);
    gtk_tree_view_column_set_sort_column_id(column,GRADUATION_COLUMN);
    gtk_tree_view_append_column(GTK_TREE_VIEW(treeview_pupils), column);
    column = gtk_tree_view_column_new_with_attributes("Lizenz", renderer, "text", LICENSE_COLUMN, NULL);
    gtk_tree_view_column_set_sort_column_id(column,LICENSE_COLUMN);
    gtk_tree_view_append_column(GTK_TREE_VIEW(treeview_pupils), column);
    column = gtk_tree_view_column_new_with_attributes("Kontakt", renderer, "text", CONTACT_COLUMN, NULL);
    gtk_tree_view_column_set_sort_column_id(column,CONTACT_COLUMN);
    gtk_tree_view_append_column(GTK_TREE_VIEW(treeview_pupils), column);
    column = gtk_tree_view_column_new_with_attributes("Passnummer", renderer, "text", PASSPORT_COLUMN, NULL);
    gtk_tree_view_column_set_sort_column_id(column,PASSPORT_COLUMN);
    gtk_tree_view_append_column(GTK_TREE_VIEW(treeview_pupils), column);
    column = gtk_tree_view_column_new_with_attributes("Bemerkungen", renderer, "text", NOTES_COLUMN, NULL);
    gtk_tree_view_column_set_sort_column_id(column,NOTES_COLUMN);
    gtk_tree_view_append_column(GTK_TREE_VIEW(treeview_pupils), column);
    gtk_tree_view_set_model(GTK_TREE_VIEW(treeview_pupils), GTK_TREE_MODEL(store));
    char* query;
    asprintf(&query, "SELECT judoka.pupils_id,judoka.name, judoka.prename, judoka.date_of_birth,judoka.graduation,judoka.license,judoka.contact,judoka.passport,judoka.notes from judoka where judoka.display='1';");
    read_db(store, incoming,query,0);
    GtkTreeSelection* input_selection = gtk_tree_view_get_selection(GTK_TREE_VIEW(treeview_pupils));
    //get through any row present, check if entry at iter matches a selected ID in trainings.pupils
    GtkTreeStore* store_exist = gtk_tree_store_new(N_COLUMNS, G_TYPE_INT, G_TYPE_STRING, G_TYPE_STRING, G_TYPE_STRING, G_TYPE_INT, G_TYPE_INT, G_TYPE_STRING, G_TYPE_STRING, G_TYPE_STRING);
    char* query_exist;
    asprintf(&query_exist, "SELECT judoka.pupils_id,judoka.name, judoka.prename, judoka.date_of_birth,judoka.graduation,judoka.license,judoka.contact,judoka.passport,judoka.notes from judoka inner join training on training.pupil = judoka.pupils_id where training.date = '%s' and training.training_nr='%d';",date,nr);
    read_db(store_exist, incoming,query_exist,0);
    gint pupils_id;
    gint pupils_id_exist;
    GtkTreeIter iter_all;
    GtkTreeIter iter_exist;
    gboolean valid0;
    gboolean valid1;
    valid0 = gtk_tree_model_get_iter_first(GTK_TREE_MODEL(store), &iter_all);    
    while(valid0){
        gtk_tree_model_get(GTK_TREE_MODEL(store),&iter_all,ID_COLUMN,&pupils_id,-1);
        valid1 = gtk_tree_model_get_iter_first(GTK_TREE_MODEL(store_exist), &iter_exist);
        while(valid1){
            gtk_tree_model_get(GTK_TREE_MODEL(store_exist),&iter_exist,ID_COLUMN,&pupils_id_exist,-1);
            if(pupils_id == pupils_id_exist){
                printf("found id: %d\n", pupils_id);
                gtk_tree_selection_select_iter(input_selection, &iter_all);
            }
            valid1 = gtk_tree_model_iter_next(GTK_TREE_MODEL(store_exist), &iter_exist);
        }
        valid0 = gtk_tree_model_iter_next(GTK_TREE_MODEL(store), &iter_all);
    }
    free(query);
    g_object_unref(store); /* destroy model automatically with view */
    g_object_unref(store_exist); /* destroy model automatically with view */
    
    //assemble widgets
    GtkWidget* contentbox = gtk_dialog_get_content_area(GTK_DIALOG(dialog));
    gtk_box_pack_start(GTK_BOX(contentbox), label, TRUE, TRUE, 0);
    gtk_box_pack_start(GTK_BOX(contentbox), label_date, TRUE, TRUE, 0);
    gtk_box_pack_start(GTK_BOX(contentbox), entry_date, TRUE, TRUE, 0);
    gtk_box_pack_start(GTK_BOX(contentbox), label_trainer, TRUE, TRUE, 0);
    gtk_box_pack_start(GTK_BOX(contentbox), button_trainer0, TRUE, TRUE, 0);
    gtk_box_pack_start(GTK_BOX(contentbox), button_trainer1, TRUE, TRUE, 0);
    gtk_box_pack_start(GTK_BOX(contentbox), label_slot, TRUE, TRUE, 0);
    gtk_box_pack_start(GTK_BOX(contentbox), button_slot, TRUE, TRUE, 0);
    gtk_box_pack_start(GTK_BOX(contentbox), label_pupils, TRUE, TRUE, 0);
    gtk_box_pack_start(GTK_BOX(contentbox), scrollpupils, TRUE, TRUE, 0);
    
    gtk_widget_show_all(dialog); //important: needed for reasons
    gint result = gtk_dialog_run(GTK_DIALOG(dialog));
    
    char* query_delete;
    asprintf(&query_delete, "delete training.* from training where training.date = '%s' and training.training_nr='%d';",date,nr);
    
    switch (result){
        case GTK_RESPONSE_OK:
            //delete old trainings
            write_db(query_delete,incoming);
            write_training_exist(dialog, data);
            break;
        default:
            break;
    }
    free(query_delete);
    gtk_widget_destroy(dialog); 
};

/*
 * Function: update_exam_dialog
 * ----------------------------
 *   provides a dialog to update the configuration of the current database
 *
 *   widget: the button that has called the callback, 
 *    
 *   data: renshu_cb_provider, provides database and output window
 *
 */
void update_exam_dialog(GtkWidget* widget, gpointer data){ 
    renshu_cb_provider* incoming = (renshu_cb_provider*)data;
    
    GtkWidget* judoka_view = incoming->output;
    GtkTreeSelection* selection = gtk_tree_view_get_selection(GTK_TREE_VIEW(judoka_view));
    GtkTreeModel* thismodel;
    GtkTreeIter iter;
    gchar* date;
    gchar* examiner0;
    gchar* examiner1;
    gchar* examiner2;
    if(gtk_tree_selection_get_selected(selection, &thismodel, &iter)){
        gtk_tree_model_get(thismodel, &iter, DATE_COLUMN_EXAM, &date, EXAMINER0_COLUMN, &examiner0, EXAMINER1_COLUMN, &examiner1, EXAMINER2_COLUMN, &examiner2, -1);
    }else{
        error_dialog("no row selected\n"); 
        return;
    }
    
    GtkWidget* dialog = gtk_dialog_new_with_buttons("Aktualisiere Prüfung", NULL, GTK_DIALOG_MODAL, "Abbruch", GTK_RESPONSE_CANCEL, "OK", GTK_RESPONSE_OK, NULL);
    // create entry widgets
    GtkWidget* entry_date = gtk_entry_new();
    GtkWidget* entry_examined = gtk_text_view_new();
    
    GtkWidget* label = gtk_label_new("Hier bitte neue Prüfungsdaten eintragen:\n");
    GtkWidget* label_date = gtk_label_new("Datum:");
    GtkWidget* label_examiner = gtk_label_new("Prüfer:");
    GtkWidget* button_examiner0 = gtk_combo_box_text_new();
    read_db(GTK_TREE_STORE(button_examiner0), incoming, "select prename, name from judoka where graduation < 0 or pupils_id = 0;", 5);
    GtkWidget* button_examiner1 = gtk_combo_box_text_new();
    read_db(GTK_TREE_STORE(button_examiner1), incoming, "select prename, name from judoka where graduation < 0  or pupils_id = 0;", 5);
    GtkWidget* button_examiner2 = gtk_combo_box_text_new();
    read_db(GTK_TREE_STORE(button_examiner2), incoming, "select prename, name from judoka where graduation < 0  or pupils_id = 0;", 5);
    
    GtkWidget* label_examined = gtk_label_new("Prüflinge:");
    
    gtk_entry_set_text(GTK_ENTRY(entry_date),date);
    
    gtk_combo_box_set_active_id(GTK_COMBO_BOX(button_examiner0), examiner0);
    gtk_combo_box_set_active_id(GTK_COMBO_BOX(button_examiner1), examiner1);
    gtk_combo_box_set_active_id(GTK_COMBO_BOX(button_examiner2), examiner2);
    
    //get treestore and populate it with necessary data
    GtkTreeStore* store = gtk_tree_store_new(N_EXAM_COLUMNS, G_TYPE_STRING, G_TYPE_STRING, G_TYPE_STRING, G_TYPE_STRING, G_TYPE_STRING, G_TYPE_INT, G_TYPE_STRING);
    
    char* getstring;
    if(incoming->usemysql){
        asprintf(&getstring, "select exam_id, date,pruefer.prename, pruefer.name, pruefer1.prename, pruefer1.name,pruefer2.prename, pruefer2.name, pruefling.prename, pruefling.name, received_grade, pruefling.pupils_id from exam inner join judoka pruefling on exam.examined=pruefling.pupils_id inner join judoka pruefer on exam.examiner0=pruefer.pupils_id inner join judoka pruefer1 on exam.examiner1=pruefer1.pupils_id inner join judoka pruefer2 on exam.examiner2=pruefer2.pupils_id where exam.date='%s' and concat(pruefer.prename,' ',pruefer.name) = '%s' and concat(pruefer1.prename,' ',pruefer1.name) = '%s' and concat(pruefer2.prename,' ',pruefer2.name) = '%s';", date,examiner0,examiner1,examiner2);
    }else{
        asprintf(&getstring, "select exam_id, date,pruefer.prename, pruefer.name, pruefer1.prename, pruefer1.name,pruefer2.prename, pruefer2.name, pruefling.prename, pruefling.name, received_grade, pruefling.pupils_id from exam inner join judoka pruefling on exam.examined=pruefling.pupils_id inner join judoka pruefer on exam.examiner0=pruefer.pupils_id inner join judoka pruefer1 on exam.examiner1=pruefer1.pupils_id inner join judoka pruefer2 on exam.examiner2=pruefer2.pupils_id where exam.date='%s' and (pruefer.prename || ' ' || pruefer.name) = '%s' and (pruefer1.prename||' '||pruefer1.name) = '%s' and (pruefer2.prename|| ' ' ||pruefer2.name) = '%s';", date,examiner0,examiner1,examiner2);

    }
    read_db(store, incoming, getstring,9);
    
    char* deletestring;
    if(incoming->usemysql){
        asprintf(&deletestring, "delete exam.* from exam inner join judoka pruefling on exam.examined=pruefling.pupils_id inner join judoka pruefer on exam.examiner0=pruefer.pupils_id inner join judoka pruefer1 on exam.examiner1=pruefer1.pupils_id inner join judoka pruefer2 on exam.examiner2=pruefer2.pupils_id where exam.date='%s' and concat(pruefer.prename,' ',pruefer.name) = '%s' and concat(pruefer1.prename,' ',pruefer1.name) = '%s' and concat(pruefer2.prename,' ',pruefer2.name) = '%s';", date,examiner0,examiner1,examiner2);
    }else{
        asprintf(&deletestring, "delete exam.* from exam inner join judoka pruefling on exam.examined=pruefling.pupils_id inner join judoka pruefer on exam.examiner0=pruefer.pupils_id inner join judoka pruefer1 on exam.examiner1=pruefer1.pupils_id inner join judoka pruefer2 on exam.examiner2=pruefer2.pupils_id where exam.date='%s' and (pruefer.prename||' '||pruefer.name) = '%s' and (pruefer1.prename||' '||pruefer1.name) = '%s' and (pruefer2.prename||' '||pruefer2.name) = '%s';", date,examiner0,examiner1,examiner2);
    }    
    GtkTreeIter iter0;
    GtkTextIter iter1;
    GtkTextBuffer* buffer;
    GtkTextMark* mark;
    gchar* examined;
    gint received_grade;
    gchar* examined_text = "";
    gboolean valid;
    
    valid = gtk_tree_model_get_iter_first(GTK_TREE_MODEL(store), &iter0);
    
    buffer = gtk_text_view_get_buffer (GTK_TEXT_VIEW (entry_examined));
    mark = gtk_text_buffer_get_insert (buffer);
    gtk_text_buffer_get_iter_at_mark (buffer, &iter1, mark);
    
    while(valid){
        gtk_tree_model_get(GTK_TREE_MODEL(store), &iter0, EXAMINED_COLUMN, &examined, RECEIVED_COLUMN, &received_grade, -1);
        asprintf(&examined_text,"%s %s %d\n", examined_text,examined,received_grade);
        valid = gtk_tree_model_iter_next(GTK_TREE_MODEL(store), &iter0);
    }
    
    gtk_text_buffer_insert(buffer, &iter1, examined_text, -1);
    
    //assemble widgets
    GtkWidget* contentbox = gtk_dialog_get_content_area(GTK_DIALOG(dialog));
    gtk_box_pack_start(GTK_BOX(contentbox), label, TRUE, TRUE, 0);
    gtk_box_pack_start(GTK_BOX(contentbox), label_date, TRUE, TRUE, 0);
    gtk_box_pack_start(GTK_BOX(contentbox), entry_date, TRUE, TRUE, 0);
    gtk_box_pack_start(GTK_BOX(contentbox), label_examiner, TRUE, TRUE, 0);
    gtk_box_pack_start(GTK_BOX(contentbox), button_examiner0, TRUE, TRUE, 0);
    gtk_box_pack_start(GTK_BOX(contentbox), button_examiner1, TRUE, TRUE, 0);
    gtk_box_pack_start(GTK_BOX(contentbox), button_examiner2, TRUE, TRUE, 0);
    gtk_box_pack_start(GTK_BOX(contentbox), label_examined, TRUE, TRUE, 0);
    gtk_box_pack_start(GTK_BOX(contentbox), entry_examined, TRUE, TRUE, 0);
    
    gtk_widget_show_all(dialog); //important: needed for reasons
    gint result = gtk_dialog_run(GTK_DIALOG(dialog));
    
    switch (result){
        case GTK_RESPONSE_OK:
            //delete old existing exams on sate with ols examiners
            write_db(deletestring,incoming);
            //rewrite exam
            write_exam(dialog, data);
            break;
        default:
            break;
    }
    gtk_widget_destroy(dialog);
};


/*
 * Function: update_exam_dialog
 * ----------------------------
 *   provides a dialog to update the configuration of the current database
 *
 *   widget: the button that has called the callback, 
 *    
 *   data: renshu_cb_provider, provides database and output window
 *
 */
void update_competition_dialog(GtkWidget* widget, gpointer data){ 
    renshu_cb_provider* incoming = (renshu_cb_provider*)data;
    
    GtkWidget* judoka_view = incoming->output;
    GtkTreeSelection* selection = gtk_tree_view_get_selection(GTK_TREE_VIEW(judoka_view));
    GtkTreeModel* thismodel;
    GtkTreeIter iter;
    gchar* date;
    gchar* competition;
    gchar* location;
    if(gtk_tree_selection_get_selected(selection, &thismodel, &iter)){
        gtk_tree_model_get(thismodel, &iter, DATE_COLUMN_COMP, &date, LOCATION_COLUMN, &location, EVENT_COLUMN, &competition, -1);
    }else{
        error_dialog("no row selected\n"); 
        return;
    }
    
    GtkWidget* dialog = gtk_dialog_new_with_buttons("Aktualisiere Wettkampf", NULL, GTK_DIALOG_MODAL, "Abbruch", GTK_RESPONSE_CANCEL, "OK", GTK_RESPONSE_OK, NULL);
    GtkWidget* entry_date = gtk_entry_new();
    GtkWidget* entry_event = gtk_entry_new();
    GtkWidget* entry_location = gtk_entry_new();
    GtkWidget* entry_athletes = gtk_text_view_new();
    
    GtkWidget* label = gtk_label_new("Hier bitte neue Wettkampfdaten eintragen:\n");
    GtkWidget* label_date = gtk_label_new("Datum:");
    GtkWidget* label_event = gtk_label_new("Wettkampf:");
    GtkWidget* label_location = gtk_label_new("Ort:");
    GtkWidget* label_athletes = gtk_label_new("Platzierungen:");
    
    gtk_entry_set_text(GTK_ENTRY(entry_date),date);
    gtk_entry_set_text(GTK_ENTRY(entry_event),competition);
    gtk_entry_set_text(GTK_ENTRY(entry_location),location);
    
    //get treestore and populate it with necessary data
    GtkTreeStore* store = gtk_tree_store_new(N_COMP_COLUMNS, G_TYPE_STRING, G_TYPE_STRING, G_TYPE_STRING, G_TYPE_STRING, G_TYPE_STRING, G_TYPE_INT, G_TYPE_STRING);
    
    char* getstring;
    asprintf(&getstring, "select competition_id, competition.name, competition.date,competition.location, judoka.prename, judoka.name, weight, result, judoka.pupils_id from competition inner join judoka on competition.competitor=judoka.pupils_id where competition.date='%s' and competition.name='%s';", date,competition);
    read_db(store, incoming, getstring,8);
    
    char* deletestring;
    asprintf(&deletestring, "delete competition.* from competition where date='%s' and name='%s';", date,competition);
    
    GtkTreeIter iter0;
    GtkTextIter iter1;
    GtkTextBuffer* buffer;
    GtkTextMark* mark;
    gchar* competitor;
    gchar* class;
    gint result;
    gchar* competitor_text = "";
    gboolean valid;
    
    valid = gtk_tree_model_get_iter_first(GTK_TREE_MODEL(store), &iter0);
    
    buffer = gtk_text_view_get_buffer (GTK_TEXT_VIEW (entry_athletes));
    mark = gtk_text_buffer_get_insert (buffer);
    gtk_text_buffer_get_iter_at_mark (buffer, &iter1, mark);
    
    while(valid){
        gtk_tree_model_get(GTK_TREE_MODEL(store), &iter0, COMPETITOR_COLUMN, &competitor, CLASS_COLUMN, &class, RESULT_COLUMN, &result, -1);
        asprintf(&competitor_text,"%s %s %s %d\n", competitor_text,competitor, class, result);
        valid = gtk_tree_model_iter_next(GTK_TREE_MODEL(store), &iter0);
    }
    
    gtk_text_buffer_insert(buffer, &iter1, competitor_text, -1);
    
    //assemble widgets
    GtkWidget* contentbox = gtk_dialog_get_content_area(GTK_DIALOG(dialog));
    gtk_box_pack_start(GTK_BOX(contentbox), label, TRUE, TRUE, 0);
    gtk_box_pack_start(GTK_BOX(contentbox), label_date, TRUE, TRUE, 0);
    gtk_box_pack_start(GTK_BOX(contentbox), entry_date, TRUE, TRUE, 0);
    gtk_box_pack_start(GTK_BOX(contentbox), label_event, TRUE, TRUE, 0);
    gtk_box_pack_start(GTK_BOX(contentbox), entry_event, TRUE, TRUE, 0);
    gtk_box_pack_start(GTK_BOX(contentbox), label_location, TRUE, TRUE, 0);
    gtk_box_pack_start(GTK_BOX(contentbox), entry_location, TRUE, TRUE, 0);
    gtk_box_pack_start(GTK_BOX(contentbox), label_athletes, TRUE, TRUE, 0);
    gtk_box_pack_start(GTK_BOX(contentbox), entry_athletes, TRUE, TRUE, 0);
    
    gtk_widget_show_all(dialog); //important: needed for reasons
    gint opcode = gtk_dialog_run(GTK_DIALOG(dialog));
    
    switch (opcode){
        case GTK_RESPONSE_OK:
            //delete old existing entries
            write_db(deletestring,incoming);
            //rewrite entry
            write_competition(dialog, data);
            break;
        default:
            break;
    }
    gtk_widget_destroy(dialog);
};

