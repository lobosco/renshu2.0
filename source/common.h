#ifndef COMMON_H 
#define COMMON_H

/*enum for displaying judoka in the tree*/

enum{
    ID_COLUMN,
    NAME_COLUMN,
    PRENAME_COLUMN,
    YOB_COLUMN,
    GRADUATION_COLUMN,
    LICENSE_COLUMN,
    CONTACT_COLUMN,
    NOTES_COLUMN,
    PASSPORT_COLUMN,
    N_COLUMNS
};

enum{
    DAY_COLUMN,
    NR_COLUMN,
    DATE_COLUMN,
    COUNT_COLUMN,
    TRAINER0_COLUMN,
    TRAINER1_COLUMN,
    N_TRAININGS_COLUMNS
};

enum{
    DATE_COLUMN_EXAM,
    EXAMINER0_COLUMN,
    EXAMINER1_COLUMN,
    EXAMINER2_COLUMN,
    EXAMINED_COLUMN,
    RECEIVED_COLUMN,
    EXAMINED_ID_COLUMN,
    N_EXAM_COLUMNS
};

enum{
    DAY_COLUMN_PRO,
    NR_COLUMN_PRO,
    DATE_COLUMN_PRO,
    TRAINER0_COLUMN_PRO,
    TRAINER1_COLUMN_PRO,
    N_TRAININGS_COLUMNS_PRO
};

enum{
    EVENT_COLUMN,
    DATE_COLUMN_COMP,
    LOCATION_COLUMN,
    COMPETITOR_COLUMN,
    CLASS_COLUMN,
    RESULT_COLUMN,
    COMPETITOR_ID_COLUMN,
    N_COMP_COLUMNS
};




/* build a struct, that holds any item necessary to be passed onto a callback
 *  output: Pointer to a widget that provides graphical output
 *  dbname: Filename of the provided database (or name of the mysql database if supplied
 * 
 * 
 */


typedef struct renshu_cb_provider{
    GtkWidget* output;
    int usemysql;
    char* dbname;
    char* dbuser;
    char* dbpasswd;
    char* dbaddr;
}renshu_cb_provider;


#endif