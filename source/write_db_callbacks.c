#define _GNU_SOURCE
#include <gtk/gtk.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "common.h"
#include "read_db_callbacks.h"
#include "dialog_callbacks.h"


#ifdef WIN32

#if 0
#include <windef.h>
#include <ansidecl.h>
#include <winbase.h>
#include <winnt.h>
#endif
#include "sqlite3.h"
#include "my_global.h"
#include "mysql.h"
#else
#include <sqlite3.h>
#include <my_global.h>
#include <mysql.h>
#endif

/*
 * Function: write_db
 * ----------------------------
 *   callback to write to database
 *
 *   query: the query to be executed
 *    
 *   dbname: pointer to the name of the database
 *
 */
void write_db(const char* query, const renshu_cb_provider* cb_provider){
    const char* dbname = cb_provider->dbname;
    int usemysql = cb_provider->usemysql;
    gchar* error = 0;
    char* err_msg = 0;
    
    if(!usemysql){
        sqlite3* db;
        int rc = sqlite3_open(dbname, &db);
        if(rc != SQLITE_OK){
            fprintf(stderr, "Cannot open database: %s\n", sqlite3_errmsg(db));
            sprintf(error, "Cannot open database: %s\n", sqlite3_errmsg(db));
            error_dialog(error);
            g_free(error);
            goto end; //ugly af, but not to avoid
        }
        //execute query
        rc = sqlite3_exec(db, query, NULL, NULL, &err_msg);
        if(rc!=SQLITE_OK){
            fprintf(stderr, "Failed to write data\n");
            fprintf(stderr, "SQL error: %s\n", err_msg);
            asprintf(&error, "Failed to write data\n SQL error: %s\n", err_msg);
            error_dialog(error);
            g_free(error);
            sqlite3_free(err_msg);
            sqlite3_close(db);
        }
        end:
        sqlite3_close(db); 
    }else if(usemysql){
//#ifndef WIN32        
        MYSQL* con = mysql_init(NULL);
        if (con == NULL){
            fprintf(stderr, "mysql_init() failed\n");
            error_dialog("mysql_init() failed\n");
            exit(1);
        }  
        if (mysql_real_connect(con, cb_provider->dbaddr, cb_provider->dbuser, cb_provider->dbpasswd, 
          cb_provider->dbname, 0, NULL, 0) == NULL) {
            finish_with_error(con);
        }
        if (mysql_query(con, "set names 'utf8';")){  
            finish_with_error(con);
        }
        if (mysql_query(con, "set character set 'utf8';")){  
            finish_with_error(con);
        }
        if (mysql_query(con, "set sql_mode='';")){  
            finish_with_error(con);
        }
        if (mysql_query(con, query)){  
            finish_with_error(con);
        }
        mysql_close(con);
//#endif
    };
};
/*
 * Function: write_judoka
 * ----------------------------
 *   callback to write judoka
 *
 *   widget: the dialog that has called the callback containing data
 *    
 *   data: renshu_cb_provider, provides database and output window
 *
 */
void write_judoka(GtkWidget *widget, gpointer data){
    renshu_cb_provider* incoming = (renshu_cb_provider*)data;
    GtkWidget* dialog = widget;
    GtkWidget* content = gtk_dialog_get_content_area(GTK_DIALOG(dialog));
    GList* children = gtk_container_get_children(GTK_CONTAINER(content)); 
    char query[500];
    char arguments[8][100];
    int i = 0;
    for(children = g_list_first(children); children != NULL; children = children->next){
        if(GTK_IS_ENTRY(children->data)){
            const gchar* text = gtk_entry_get_text(GTK_ENTRY(children->data));
            sprintf(arguments[i],"%s", text);
            i++;
        }
        if(GTK_IS_COMBO_BOX(children->data)){
            const gchar* text2 = gtk_combo_box_text_get_active_text(GTK_COMBO_BOX_TEXT(children->data));
            
            sprintf(arguments[i], "%s", text2);
            i++;
        }
    }
    
    //deassemble license name:
    char* box_text = strtok(arguments[4], " \n");
    char* box_id;
    int k = 0;
    while(box_text != NULL){
        if(k%3==0){
            asprintf(&box_id, "%s", box_text);
        }
        box_text = strtok(NULL, " \n");
        k++;
    }
    
    sprintf(query, "insert into judoka (name, prename, date_of_birth, graduation, license, display, contact,passport, notes) values ('%s', '%s','%s','%s','%s', '1', '%s', '%s', '%s');", arguments[0], arguments[1],arguments[2],arguments[3],box_id, arguments[5],arguments[6],arguments[7]);
    write_db(query, incoming);
    show_judoka_age(widget, incoming);
};
/*
 * Function: update_judoka
 * ----------------------------
 *   callback to update judoka
 *
 *   widget: the dialog that has called the callback containing data
 *    
 *   data: renshu_cb_provider, provides database and output window
 *
 */
void update_judoka(GtkWidget *widget, gpointer data){
    GtkWidget* dialog = widget;
    GtkWidget* content = gtk_dialog_get_content_area(GTK_DIALOG(dialog));
    GList* children = gtk_container_get_children(GTK_CONTAINER(content)); 
    char query[500];
    char arguments[8][100];
    int i = 0;
    renshu_cb_provider* incoming = (renshu_cb_provider*) data;
    GtkWidget* labeltowrite = GTK_WIDGET(incoming->output);
    /* get chosen id*/
    GtkTreeSelection* selected_row = gtk_tree_view_get_selection(GTK_TREE_VIEW(labeltowrite));
    GtkTreeModel* model;
    GtkTreeIter iter;
    gint judoka_id_int = 0;
    gchar* judoka_id;
    if(gtk_tree_selection_get_selected(selected_row, &model, &iter)){
        gtk_tree_model_get(model, &iter, ID_COLUMN, &judoka_id_int,-1);
        asprintf(&judoka_id,"%d",judoka_id_int);
    }else{
        error_dialog("no row selected\n");  
        return;
    }
    
    for(children = g_list_first(children); children != NULL; children = children->next){
        if(GTK_IS_ENTRY(children->data)){
            const gchar* text = gtk_entry_get_text(GTK_ENTRY(children->data));
            sprintf(arguments[i],"%s", text);
            i++;
        }
        if(GTK_IS_COMBO_BOX(children->data)){
            const gchar* text2 = gtk_combo_box_text_get_active_text(GTK_COMBO_BOX_TEXT(children->data));
            
            sprintf(arguments[i], "%s", text2);
            i++;
        }
    }
    
     //deassemble license name:
    char* box_text = strtok(arguments[4], " \n");
    char* box_id;
    int k = 0;
    while(box_text != NULL){
        if(k%3==0){
            asprintf(&box_id, "%s", box_text);
        }
        box_text = strtok(NULL, " \n");
        k++;
    }
    
    
    sprintf(query, "update judoka set name='%s', prename='%s',date_of_birth='%s',graduation='%s',license='%s',contact='%s',passport='%s',notes='%s', display='1' where pupils_id='%s';", arguments[0], arguments[1],arguments[2],arguments[3],box_id,arguments[5],arguments[6],arguments[7], judoka_id);
    write_db(query, incoming);
    g_free(judoka_id);
    show_judoka_age(widget, incoming);
};

void foreach_append (GtkTreeModel *model, GtkTreePath *path, GtkTreeIter *iter, gpointer userdata){
    int* list = (int*) userdata;  
    gint id;
    gtk_tree_model_get(GTK_TREE_MODEL(model), iter, ID_COLUMN , &id, -1);
    list[list[0]] = id;
    list[0]++;
  }

/*
 * Function: write_training_exist
 * ----------------------------
 *   callback to write judoka
 *
 *   widget: the dialog that has called the callback containing data
 *    
 *   data: renshu_cb_provider, provides database and output window
 *
 */
void write_training_exist(GtkWidget *widget, gpointer data){
    renshu_cb_provider* incoming = (renshu_cb_provider*)data;
    GtkWidget* dialog = widget;
    GtkWidget* content = gtk_dialog_get_content_area(GTK_DIALOG(dialog));
    GList* children = gtk_container_get_children(GTK_CONTAINER(content)); 
    int pupils[101];
    pupils[0] = 1;
    char* arguments[4];
    int i = 0;
    char* query;
    for(children = g_list_first(children); children != NULL; children = children->next){
        if(GTK_IS_ENTRY(children->data)){
            const gchar* text = gtk_entry_get_text(GTK_ENTRY(children->data));
            asprintf(&arguments[i],"%s", text);
            i++;
        }else if(GTK_IS_SCROLLED_WINDOW(children->data)){
            GtkTreeView* treeview = GTK_TREE_VIEW((g_list_first(gtk_container_get_children(GTK_CONTAINER(children->data))))->data);
            GtkTreeSelection* selection = gtk_tree_view_get_selection(GTK_TREE_VIEW(treeview));
            gtk_tree_selection_selected_foreach(selection, foreach_append, pupils);
        }else if(GTK_IS_COMBO_BOX(children->data)){
            const gchar* text2 = gtk_combo_box_text_get_active_text(GTK_COMBO_BOX_TEXT(children->data));
            asprintf(&arguments[i], "%s", text2);
            i++;
        }
    }
    //deassemble trainer name:
    char* trainer_text = strtok(arguments[1], " \n");
    char* trainer_name;
    char* trainer_prename;
    int k = 0;
    while(trainer_text != NULL){
        if(k%2==0){
            asprintf(&trainer_prename, "%s", trainer_text);
        }else{
            asprintf(&trainer_name, "%s", trainer_text);
        }
        trainer_text = strtok(NULL, " \n");
        k++;
    }
    //deassemble trainer name:
    char* trainer1_text = strtok(arguments[2], " \n");
    char* trainer1_name;
    char* trainer1_prename;
    k = 0;
    while(trainer1_text != NULL){
        if(k%2==0){
            asprintf(&trainer1_prename, "%s", trainer1_text);
        }else{
            asprintf(&trainer1_name, "%s", trainer1_text);
        }
        trainer1_text = strtok(NULL, " \n");
        k++;
    }
    //deassemble training text    
    char* training_text = strtok(arguments[3], " \n");
    char* training_weekday;
    char* training_nr;
    int j = 0;
    while(training_text != NULL){
        if(j%2==0){
            asprintf(&training_weekday, "%s", training_text);
        }else{
            asprintf(&training_nr, "%s", training_text);
        }
        training_text = strtok(NULL, " \n");
        j++;
    }
    int l = 0;
    for(l = 1; l<pupils[0]; l++){
        asprintf(&query, "insert into training (date,trainer0, trainer1,pupil,weekday,training_nr) values ('%s',  ( select pupils_id from judoka where name='%s' and prename ='%s'), (select pupils_id from judoka where name='%s' and prename ='%s'), '%d', '%s', '%s');", arguments[0], trainer_name, trainer_prename,trainer1_name,trainer1_prename, pupils[l], training_weekday, training_nr);
        write_db(query,incoming);
    }
    
    show_trainings_all(widget, incoming);
    free(query);
    free(trainer_name);
    free(trainer_prename);
    free(trainer_text);
    free(trainer1_name);
    free(trainer1_prename);
    free(trainer1_text);
    free(training_text);
    free(training_weekday);
    free(training_nr);
    for(int i=0; i<4; i++){
     free(arguments[i]);   
    }
};


/*
 * Function: write_competition
 * ----------------------------
 *   callback to write competition
 *
 *   widget: the dialog that has called the callback containing data
 *    
 *   data: renshu_cb_provider, provides database and output window
 *
 */
void write_competition(GtkWidget *widget, gpointer data){
    renshu_cb_provider* incoming = (renshu_cb_provider*)data;
    GtkWidget* dialog = widget;
    GtkWidget* content = gtk_dialog_get_content_area(GTK_DIALOG(dialog));
    GList* children = gtk_container_get_children(GTK_CONTAINER(content)); 
    gchar* buffertext;
    char* arguments[3];
    int i = 0;
    for(children = g_list_first(children); children != NULL; children = children->next){
        if(GTK_IS_ENTRY(children->data)){
            const gchar* text = gtk_entry_get_text(GTK_ENTRY(children->data));
            asprintf(&arguments[i],"%s", text);
            i++;
        }else if(GTK_IS_TEXT_VIEW(children->data)){
            GtkTextBuffer* textbuffer = gtk_text_view_get_buffer(GTK_TEXT_VIEW(children->data));
            GtkTextIter iter_start;
            GtkTextIter iter_end;
            gtk_text_buffer_get_start_iter(textbuffer, &iter_start);
            gtk_text_buffer_get_end_iter(textbuffer, &iter_end);
            buffertext = gtk_text_buffer_get_text(textbuffer, &iter_start, &iter_end, FALSE);
        }
    }
    
    char* text = strtok(buffertext, " \n");
    int j = 0;
    char* name;
    char* prename;
    char* class;
    char* query;
    while (text){   
        if(j%4==0){
            asprintf(&prename, "%s", text);
        }else if(j%4==1){
            asprintf(&name, "%s", text);
        }else if(j%4==2){
            asprintf(&class, "%s", text);
        }else{
            asprintf(&query, "insert into competition (date, competitor, location, name, result, weight) values ('%s', (select pupils_id from judoka where name='%s' and prename='%s' ) , '%s', '%s' , '%s', '%s' );", arguments[0], name, prename, arguments[2], arguments[1], text, class);
            write_db(query, incoming);
        }
        text = strtok(NULL, " \n");
        j++;
    }
    show_competitions_all(widget, incoming);
    g_free(buffertext);
    free(name);
    free(prename);
    free(class);
    free(query);
    free(text);
    for(int i=0; i<3; i++){
     free(arguments[i]);   
    }
};
/*
 * Function: write_exam
 * ----------------------------
 *   callback to write exam
 *
 *   widget: the dialog that has called the callback containing data
 *    
 *   data: renshu_cb_provider, provides database and output window
 *
 */
void write_exam(GtkWidget *widget, gpointer data){
    renshu_cb_provider* incoming = (renshu_cb_provider*)data;
    GtkWidget* dialog = widget;
    GtkWidget* content = gtk_dialog_get_content_area(GTK_DIALOG(dialog));
    GList* children = gtk_container_get_children(GTK_CONTAINER(content)); 
    gchar* buffertext;
    char* arguments[4];
    int i = 0;
    for(children = g_list_first(children); children != NULL; children = children->next){
        if(GTK_IS_ENTRY(children->data)){
            const gchar* text = gtk_entry_get_text(GTK_ENTRY(children->data));
            asprintf(&arguments[i],"%s", text);
            i++;
        }else if(GTK_IS_TEXT_VIEW(children->data)){
            GtkTextBuffer* textbuffer = gtk_text_view_get_buffer(GTK_TEXT_VIEW(children->data));
            GtkTextIter iter_start;
            GtkTextIter iter_end;
            gtk_text_buffer_get_start_iter(textbuffer, &iter_start);
            gtk_text_buffer_get_end_iter(textbuffer, &iter_end);
            buffertext = gtk_text_buffer_get_text(textbuffer, &iter_start, &iter_end, FALSE);
        }else if(GTK_IS_COMBO_BOX(children->data)){
            const gchar* text2 = gtk_combo_box_text_get_active_text(GTK_COMBO_BOX_TEXT(children->data));
            asprintf(&arguments[i], "%s", text2);
            i++;
        }
    }
    
    char* examiner_text = strtok(arguments[1], " \n");
    char* examiner_name;
    char* examiner_prename;
    int k = 0;
    while(examiner_text != NULL){
        if(k%2==0){
            asprintf(&examiner_prename, "%s", examiner_text);
        }else{    //list no longer needed
            asprintf(&examiner_name, "%s", examiner_text);
        }
        examiner_text = strtok(NULL, " \n");
        k++;
    }
    char* examiner1_text = strtok(arguments[2], " \n");
    char* examiner1_name;
    char* examiner1_prename;
    k = 0;
    while(examiner1_text != NULL){
        if(k%2==0){
            asprintf(&examiner1_prename, "%s", examiner1_text);
        }else{    //list no longer needed
            asprintf(&examiner1_name, "%s", examiner1_text);
        }
        examiner1_text = strtok(NULL, " \n");
        k++;
    }
    char* examiner2_text = strtok(arguments[3], " \n");
    char* examiner2_name;
    char* examiner2_prename;
    k = 0;
    while(examiner2_text != NULL){
        if(k%2==0){
            asprintf(&examiner2_prename, "%s", examiner2_text);
        }else{    //list no longer needed
            asprintf(&examiner2_name, "%s", examiner2_text);
        }
        examiner2_text = strtok(NULL, " \n");
        k++;
    }
    
    char* text = strtok(buffertext, " \n");
    int j = 0;
    char* name;
    char* prename;
    char* query;
    while (text){
        if(j%3==0){
            asprintf(&prename, "%s", text);
        }else if(j%3==1){
            asprintf(&name, "%s", text);
        }else{
            asprintf(&query, "insert into exam (date, examiner0, examiner1,examiner2, examined, received_grade) values ('%s', (select pupils_id from judoka where name='%s' and prename='%s' ) , (select pupils_id from judoka where name='%s' and prename='%s' ) , (select pupils_id from judoka where name='%s' and prename='%s' ) , (select pupils_id from judoka where name='%s' and prename='%s' ), '%s');", arguments[0], examiner_name, examiner_prename, examiner1_name, examiner1_prename, examiner2_name, examiner2_prename, name, prename, text);
            write_db(query, incoming);
            asprintf(&query, "update judoka set graduation='%s' where name='%s' and prename='%s';", text, name, prename);
            write_db(query, incoming);
        }
        text = strtok(NULL, " \n");
        j++;
    }
    show_exams_all(widget, incoming);
    g_free(buffertext);
    free(name);
    free(prename);
    free(query);
    free(examiner_name);
    free(examiner_prename);
    free(examiner_text);
    free(examiner1_name);
    free(examiner1_prename);
    free(examiner1_text);
    free(examiner2_name);
    free(examiner2_prename);
    free(examiner2_text);
    for(int i=0; i<4; i++){
     free(arguments[i]);   
    }
};
/*
 * Function: write_initial
 * ----------------------------
 *   callback to write initial data, does not yet overwrite existing data
 *
 *   widget: the dialog that has called the callback containing data
 *    
 *   data: renshu_cb_provider, provides database and output window
 *
 */
void write_initial(GtkWidget *widget, gpointer data){
    renshu_cb_provider* incoming = (renshu_cb_provider*)data;
    GtkWidget* dialog = widget;
    GtkWidget* content = gtk_dialog_get_content_area(GTK_DIALOG(dialog));
    GList* children = gtk_container_get_children(GTK_CONTAINER(content)); 
    char* query="";
    char* query2="";
    char* query3="";
    char* query4="";
    gchar* filechooser_output[2];
    gchar* textview_output[2];
    char* arguments[7];
    int i = 0;
    int j = 0;
    int k = 0;
    int first = 0;
    for(children = g_list_first(children); children != NULL; children = children->next){
        if(GTK_IS_ENTRY(children->data)){
            if((incoming->usemysql) && (first==0)){
                const char* text = gtk_entry_get_text(GTK_ENTRY(children->data));
                printf("i: %d, j: %d, %s\n", i,j,text);
                asprintf(&filechooser_output[i],"%s", text);
                j++;
                first++;
            }else{
                const char* text = gtk_entry_get_text(GTK_ENTRY(children->data));
                printf("i: %d, j: %d, %s\n", i,j,text);
                asprintf(&arguments[i],"%s", text);
                i++;
            }
        }else if(GTK_IS_FILE_CHOOSER_BUTTON(children->data)){
            const gchar* file = gtk_file_chooser_get_filename(GTK_FILE_CHOOSER(children->data));
            if(file == NULL){
                error_dialog("Bitte Header- und Konfigurationsdatei auswählen");
                return;
            }else{
               printf("i: %d, j: %d, %s\n", i,j,file);
                asprintf(&filechooser_output[j], "%s", file);
                j++;
            }
        }else if(GTK_IS_BUTTON(children->data)){
            const gchar* file = gtk_button_get_label(GTK_BUTTON(children->data));
            printf("i: %d, j: %d, %s\n", i,j,file);
                asprintf(&filechooser_output[j], "%s", file);
            j++;
        }else if(GTK_IS_TEXT_VIEW(children->data)){
            GtkTextBuffer* textbuffer = gtk_text_view_get_buffer(GTK_TEXT_VIEW(children->data));
            GtkTextIter iter_start;
            GtkTextIter iter_end;
            gtk_text_buffer_get_start_iter(textbuffer, &iter_start);
            gtk_text_buffer_get_end_iter(textbuffer, &iter_end);
            textview_output[k] = gtk_text_buffer_get_text(textbuffer, &iter_start, &iter_end, FALSE);
            k++;
        }
    }
   
    //set new database
    asprintf(&incoming->dbname, "%s", filechooser_output[0]);
    //declare database layout (and drop any tables before... user has been warned!):
    if(incoming->usemysql){
        const char* queries[] = {"create table `judoka` (`pupils_id` integer primary key auto_increment, `name` text, `prename` text , `date_of_birth` date, `graduation` integer, `license` integer, `display` text, `contact` text,`passport` text, `notes` text) ;", "create table `training` (`training_id` integer primary key auto_increment, `date` date, `trainer0` integer, `trainer1` integer , `pupil` integer , `training_nr` integer , `weekday` text); ", "create table `competition` (`competition_id` integer primary key auto_increment, `date` date, `competitor` integer ,`location` text, `name` text , `result` integer , `weight` text); ", "create table `exam` (`exam_id` integer primary key auto_increment, `date` date, `examiner0` integer ,`examiner1` integer ,`examiner2` integer ,`examined` integer, `received_grade` integer); ", "create table `settings` (`rowid` integer primary key auto_increment, `name` text, `prename` text, `configpath` text, `headerpath` text);","create table `rates` (`rateid` integer primary key auto_increment, `identifier` text, `rate` float);","create table `slots` (`slotid` integer primary key auto_increment, `day` text, `nr` integer, `begin` integer, `end` integer);","insert into judoka (pupils_id,name, prename, date_of_birth, graduation, license, display, `contact`,passport, `notes`) values ('0','-', '-', '0000-01-01', '0', '0', '1', '-', '0', '-');"
        };
        for(int k=0; k<8; k++){
            write_db(queries[k], incoming);
        }
    }else{
         const char* queries[] = {"create table `judoka` (`pupils_id` integer primary key, `name` text, `prename` text , `date_of_birth` date, `graduation` integer, `license` integer, `display` text, `contact` text,`passport` text, `notes` text) ;", "create table `training` (`training_id` integer primary key, `date` date, `trainer0` integer ,`trainer1` integer , `pupil` integer , `training_nr` integer , `weekday` text); ", "create table `competition` (`competition_id` integer primary key, `date` date, `competitor` integer ,`location` text, `name` text , `result` integer , `weight` text); ", "create table `exam` (`exam_id` integer primary key , `date` date, `examiner0` integer, `examiner1` integer, `examiner2` integer ,`examined` integer, `received_grade` integer); ", "create table `settings` (`rowid` integer primary key, `name` text, `prename` text, `configpath` text, `headerpath` text);""create table `rates` (`rateid` integer primary key, `identifier` text, `rate` float);","create table `slots` (`slotid` integer primary key, `day` text, `nr` integer, `begin` integer, `end` integer);","insert into judoka (pupils_id,name, prename, date_of_birth, graduation, license, display, `contact`,passport, `notes`) values ('0','-', '-', '0000-01-01', '0', '0', '1', '-', '0', '-');"};
        for(int k=0; k<8; k++){
            write_db(queries[k], incoming);
        }
    };
    //set settings of database: //
    asprintf(&query,"insert into settings (name, prename, configpath, headerpath) values ('%s','%s','%s','%s');", arguments[0],arguments[1],filechooser_output[1],filechooser_output[2]);
    write_db(query, incoming);
    asprintf(&query2, "insert into judoka (name, prename, date_of_birth, graduation, license, display, `contact`,passport, `notes`) values ('%s', '%s', '0000-01-01', '9', '-', '1', '-', '0', '-');", arguments[0], arguments[1]);
    write_db(query2, incoming);
    
    
    char* text = strtok(textview_output[1], ",\n");
    j = 0;
    char* identifier;
    char* rate;
    while (text){
        if(j%2==0){
            asprintf(&identifier, "%s", text);
        }else if(j%2==1){
            asprintf(&rate, "%s", text);
            asprintf(&query3, "insert into rates (identifier,rate) values ('%s', '%s');", identifier, rate);
            write_db(query3, incoming);
        }
        text = strtok(NULL, ",\n");
        j++;
    }
    
    text = strtok(textview_output[0], ",\n");
    j = 0;
    char* day;
    char* nr;
    char* begin;
    char* end;
    while (text){
        if(j%4==0){
            asprintf(&day, "%s", text);
        }else if(j%4==1){
            asprintf(&nr, "%s", text);
        }else if(j%4==2){
            asprintf(&begin, "%s", text);
        }else if(j%4==3){
            asprintf(&end, "%s", text);
            asprintf(&query4, "insert into slots (day,nr,begin,end) values ('%s', '%s','%s','%s');", day,nr,begin,end);
            write_db(query4, incoming);
        }
        text = strtok(NULL, ",\n");
        j++;
    }
    
    //write to configfile:
    FILE *f = fopen(filechooser_output[1], "w");
    if (f == NULL)
    {
        error_dialog("Error opening file!\n");
        return;
    }
    for(int i=2;i<7;i++){
        fprintf(f,"%s\n",arguments[i]);
    }
    fclose(f);
    
    free(day);
    free(nr);
    free(begin);
    free(end);
    free(identifier);
    free(rate);
    free(query2);
    free(query3);
    free(query4);
    free(query);
    for(int i=0; i<7; i++){
     free(arguments[i]);   
    }
    for(int i=0; i<2; i++){
     free(filechooser_output[i]);   
    }
    for(int i=0; i<2; i++){
     free(textview_output[i]);   
    }
};
/*
 * Function: update_settings
 * ----------------------------
 *   callback to update settings in the database
 *
 *   widget: the dialog that has called the callback containing data
 *    
 *   data: renshu_cb_provider, provides database and output window
 *
 */
void update_settings(GtkWidget *widget, gpointer data){
    renshu_cb_provider* incoming = (renshu_cb_provider*)data;
    GtkWidget* dialog = widget;
    GtkWidget* content = gtk_dialog_get_content_area(GTK_DIALOG(dialog));
    GList* children = gtk_container_get_children(GTK_CONTAINER(content)); 
    char* query="";
    gchar* filechooser_output[1];
    char* combo_output;
    char* arguments[7];
    int i = 0;
    int j = 0;
    for(children = g_list_first(children); children != NULL; children = children->next){
        if(GTK_IS_ENTRY(children->data)){
            const gchar* text = gtk_entry_get_text(GTK_ENTRY(children->data));
            asprintf(&arguments[i],"%s", text);
            i++;
        }else if(GTK_IS_FILE_CHOOSER_BUTTON(children->data)){
            const gchar* file = gtk_file_chooser_get_filename(GTK_FILE_CHOOSER(children->data));
            asprintf(&filechooser_output[j], "%s", file);
            j++;
        }else if(GTK_IS_COMBO_BOX(children->data)){
            const gchar* text2 = gtk_combo_box_text_get_active_text(GTK_COMBO_BOX_TEXT(children->data));
            asprintf(&combo_output, "%s", text2);
        }
    }
    //split combo_output to obtain rowid:
    
    char* trainer_text = strtok(combo_output, " \n");
    char* trainer_name;
    char* trainer_prename;
    int k = 0;
    while(trainer_text != NULL){
        if(k%2==0){
            asprintf(&trainer_prename, "%s", trainer_text);
        }else{
            asprintf(&trainer_name, "%s", trainer_text);
        }
        trainer_text = strtok(NULL, " \n");
        k++;
    }
    
    
    //set settings of database:
    asprintf(&query,"update settings set name='%s',prename='%s',configpath='%s', headerpath='%s' where name='%s' and prename='%s';", arguments[0],arguments[1],filechooser_output[0],filechooser_output[1],trainer_name,trainer_prename);
    write_db(query, incoming);
   
    //write to configfile:
    FILE *f = fopen(filechooser_output[0], "w");
    if (f == NULL)
    {
        error_dialog("Error opening file!\n");
        return;
    }
    for(int i=2;i<7;i++){
        fprintf(f,"%s\n",arguments[i]);
    }
    fclose(f);
    
    free(query);
    for(int i=0; i<7; i++){
     free(arguments[i]);   
    }
    for(int i=0; i<1; i++){
     free(filechooser_output[i]);   
    }
};

/*
 * Function: add_new_settings
 * ----------------------------
 *   callback to update settings in the database
 *
 *   widget: the dialog that has called the callback containing data
 *    
 *   data: renshu_cb_provider, provides database and output window
 *
 */
void add_new_settings(GtkWidget *widget, gpointer data){
    renshu_cb_provider* incoming = (renshu_cb_provider*)data;
    GtkWidget* dialog = widget;
    GtkWidget* content = gtk_dialog_get_content_area(GTK_DIALOG(dialog));
    GList* children = gtk_container_get_children(GTK_CONTAINER(content)); 
    char* query="";
    gchar* filechooser_output[2];
    char* arguments[7];
    int i = 0;
    int j = 0;
    for(children = g_list_first(children); children != NULL; children = children->next){
        if(GTK_IS_ENTRY(children->data)){
            const gchar* text = gtk_entry_get_text(GTK_ENTRY(children->data));
            asprintf(&arguments[i],"%s", text);
            i++;
        }else if(GTK_IS_FILE_CHOOSER_BUTTON(children->data)){
            const gchar* file = gtk_file_chooser_get_filename(GTK_FILE_CHOOSER(children->data));
            asprintf(&filechooser_output[j], "%s", file);
            j++;
        }
    }
    //set settings of database:
    asprintf(&query,"insert into settings (name,prename,configpath, headerpath) values ('%s','%s','%s','%s');", arguments[0],arguments[1],filechooser_output[0],filechooser_output[1]);
    write_db(query, incoming);
   
     //write to configfile:
    FILE *f = fopen(filechooser_output[0], "w");
    if (f == NULL)
    {
        error_dialog("Error opening file!\n");
        return;
    }
    for(int i=2;i<7;i++){
        fprintf(f,"%s\n",arguments[i]);
    }
    fclose(f);
   
    
    free(query);
    for(int i=0; i<7; i++){
     free(arguments[i]);   
    }
    for(int i=0; i<2; i++){
     free(filechooser_output[i]);   
    }
};


/*
 * Function: delete_judoka
 * ----------------------------
 *   callback to "delete" judoka so that he no longer appears on judoka_list
 *
 *   widget: the dialog that has called the callback containing data
 *    
 *   data: renshu_cb_provider, provides database and output window
 *
 */
void delete_judoka(GtkWidget *widget, gpointer data){
    renshu_cb_provider* incoming = (renshu_cb_provider*)data;
    GtkWidget* labeltowrite = GTK_WIDGET(incoming->output);
    /* get chosen id*/
    GtkTreeSelection* selected_row = gtk_tree_view_get_selection(GTK_TREE_VIEW(labeltowrite));
    GtkTreeModel* model;
    GtkTreeIter iter;
    gint judoka_id;
    
    if(gtk_tree_selection_get_selected(selected_row, &model, &iter)){
        gtk_tree_model_get(model, &iter, ID_COLUMN, &judoka_id,-1);
    }else{
        error_dialog("no row selected\n");  
        return;
    }
    char* query="";
    asprintf(&query,"update judoka set display='0',name='deleted',prename='deleted',graduation='0',date_of_birth='0000-01-01',license='0',contact='-', passport='0',notes='-' where pupils_id='%d';", judoka_id);
    write_db(query, incoming);
    show_judoka_age(widget, incoming);
    free(query);
};